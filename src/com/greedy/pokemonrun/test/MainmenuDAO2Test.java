package com.greedy.pokemonrun.test;

import static com.greedy.pokemonrun.common.JDBCTemplate.getConnection;
import static org.junit.Assert.assertNotNull;

import java.sql.Connection;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.greedy.pokemonrun.model.dao.MainmenuDAO2;

public class MainmenuDAO2Test {

	private MainmenuDAO2 mainmenuDAO2;
	private Connection con;
	
	@Before
	public void setup() {
	
		mainmenuDAO2 = new MainmenuDAO2();
		
		con = getConnection(); 
		
	}
	
	@Test
	public void TestRankInfo() {
		
		Map<String, Integer> rankList = mainmenuDAO2.rankInfo(con);
		
		assertNotNull(rankList);
		
		for(String key : rankList.keySet()) {
			Integer value = rankList.get(key);
			System.out.println(key + " : " + value);
		}
	}
}
