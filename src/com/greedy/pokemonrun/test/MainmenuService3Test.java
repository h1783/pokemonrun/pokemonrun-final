package com.greedy.pokemonrun.test;

import org.junit.Before;
import org.junit.Test;

import com.greedy.pokemonrun.model.service.MainmenuService3;

public class MainmenuService3Test {

	private MainmenuService3 mainmenuService3;
	private int acntNum;
	private String message;
	private String acntPwd;
	private String newPwd = "abd";
	private String checkNewPwd = "abc";
	
	@Before
	public void setup() {
	
		mainmenuService3 = new MainmenuService3();
		
	}
	
	@Test
	public void testDeleteAcct() {
		
		mainmenuService3.deleteAcct(acntNum, message);
	}
	
	@Test
	public void testChangePwd() {
		
		mainmenuService3.changPwd(acntNum, acntPwd, newPwd, checkNewPwd);
	}
}
