package com.greedy.pokemonrun.test;

import static com.greedy.pokemonrun.common.JDBCTemplate.getConnection;

import java.sql.Connection;

import org.junit.Before;
import org.junit.Test;

import com.greedy.pokemonrun.model.dao.LoginDAO;
import com.greedy.pokemonrun.model.dto.AccountDTO;

public class LoginDAOTest {

	private Connection con;
	private LoginDAO loginDAO;
	private AccountDTO accountDTO;
	private String memEmail;
	
	@Before
	public void setup() {
		
		con = getConnection();
		loginDAO = new LoginDAO();
		accountDTO = new AccountDTO();
				
	}
	
	@Test
	public void testInsertAccount() {
		
		loginDAO.insertAccount(con, accountDTO);
		loginDAO.insertPlayer(con);
		loginDAO.insertDrawPokemonHistory(con);
	}
	
	@Test
	public void testRegistAccountCheckInput() {
		
		AccountDTO acntDTO = new AccountDTO();

		loginDAO.insertAccount(con, acntDTO);
	}
	
	@Test
	public void testRegistAccountCheckInput2() {
		
        AccountDTO acntDTO = new AccountDTO();

        loginDAO.insertAccount(con, acntDTO);
	}
	
	@Test
	public void testFindAccountId() {
		
		 loginDAO.findAccountDTOByEmail(con, memEmail);
	}
	
}