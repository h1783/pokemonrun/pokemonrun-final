package com.greedy.pokemonrun.test;

import static com.greedy.pokemonrun.common.JDBCTemplate.getConnection;

import java.sql.Connection;

import org.junit.Before;
import org.junit.Test;

import com.greedy.pokemonrun.model.dao.MainmenuDAO3;

public class MainmenuDAO3Test {

	private MainmenuDAO3 mainmenuDAO3;
	private Connection con;
	private int acntNum = 1;
	private String newPwd;
	
	@Before
	public void setup() {
		
		con = getConnection();

		mainmenuDAO3 = new MainmenuDAO3();
	}
	
	@Test
	public void testDeleteAcct() {
		
		mainmenuDAO3.deleteAcct(con, acntNum);
		
	}
	
	@Test
	public void testCheckNameByPwd() {
		
		mainmenuDAO3.checkNameByPwd(con, acntNum);
	}
	
	@Test
	public void testUpdatePwd() {
		
		mainmenuDAO3.updatePwd(con, newPwd, acntNum);
	}
}
