package com.greedy.pokemonrun.model.dao;

import static com.greedy.pokemonrun.common.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

/**
 * <pre>
 * Class : MainmenuDAO
 * Comment : 서비스에서 받아온 값들을 이용해 CRUD의 결과를 리턴해주는 DAO
 * History
 * 2021/12/12 (구훈모) 처음 작성
 * 2021/12/16 (구훈모) 완성
 * </pre>
 * @author 구훈모
 * */
public class MainmenuDAO3 {
	
	/* 필드에 Properties를 생성하여 공유 */
	private Properties prop = new Properties();
	
	/**
	 * <pre>
	 * Comment : DAO를 호출할때 기본생성자가 호출이 되므로 여기서 쿼리문 가져오는 행위를 공통적으로 함.
	 * </pre>
	 * @author 구훈모
	 * */
	public MainmenuDAO3() {
		
		try {
			prop.loadFromXML(new FileInputStream("mapper/hunmo-query.xml"));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * <pre>
	 * Comment : 회원번호를 이용해 계정삭제여부를 update 해주는 DAO
	 * </pre>
	 * @author 구훈모
	 * @param con : DB와 커넥션키셔주는 녀석
	 * @param acntNum : update할 회원의 번호 
	 * @return result : update 성공여부를 알려주는 정수 값
	 * */
	public int deleteAcct(Connection con, int acntNum) {
		
		int result = 0;

		PreparedStatement pstmt = null;
		
		String query = prop.getProperty("deleteAcct");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, acntNum);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	/**
	 * <pre>
	 * Comment : 회원번호를 이용해 그 회원의 비밀번호를 반환해주는 메소드
	 * </pre>
	 * @author 구훈모
	 * @param con : DB와 커넥션 시켜주는 녀석
	 * @param acntNum : 조회할 회원 번호
	 * @return nameByPwd : 회원번호에 해당하는 사용자의 비밀번호 반환
	 * */
	public String checkNameByPwd(Connection con, int acntNum) {
		
		String nameByPwd = "";

		/* 아이디에 해당하는 pwd를 가져오는 쿼리   */
		String query = prop.getProperty("checkIdByPwd");
		
		PreparedStatement pstmt = null;
		
		ResultSet rset = null;
		
		try {
			/* Id을 받아 pwd 조회 */
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, acntNum);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				nameByPwd = rset.getString("ACNT_PW");
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return nameByPwd;
	}

	/**
	 * <pre>
	 * Comment : 회원의 비밀번호를 Update해주는 메소드
	 * </pre>
	 * @author  구훈모
	 * @param con : DB와 커넥션 시켜주는 녀석
	 * @param newPwd : 사용자가 새롭게 변경하길 원하는 비밀번호
	 * @param acntNum : 비밀번호 변경을 할 회원의 회원번호 
	 * @return result : 비밀번호 변경 성공여부를 알려주는 정수 값
	 * */
	public int updatePwd(Connection con, String newPwd, int acntNum) {

		int result = 0;

		PreparedStatement pstmt = null;
		
		String query = prop.getProperty("updatePwd");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, newPwd);
			pstmt.setInt(2, acntNum);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
}
