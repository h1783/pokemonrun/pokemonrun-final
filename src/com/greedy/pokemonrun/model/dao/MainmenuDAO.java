package com.greedy.pokemonrun.model.dao;

import static com.greedy.pokemonrun.common.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.greedy.pokemonrun.model.dto.AccountDTO;
import com.greedy.pokemonrun.model.dto.PokemonDTO;

/** 
 * <pre>
 * Class : MainmenuDAO
 * Comment : 메인 메뉴 페이지의 상점 기능에 사용되는 쿼리문을 실행하는 메소드들의 모음 DAO
 * History
 * 2021/12/13 (박상범) 작성
 * </pre>
 * @version 1
 * @author 박상범
 * */
public class MainmenuDAO {
	/* 쿼리문이 담겨있는 Property를 담을 변수를 필드에 선언*/
	private Properties prop;

	/* 기본생성자 호출 시 mapper폴더 안에있는 쿼리문과 연결을 해준다. */
	public MainmenuDAO() {

		prop = new Properties();
		
		try {
			prop.loadFromXML(new FileInputStream("mapper/sangbum-query.xml"));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * <pre>
	 * (현재 유저의 몬스터볼 갯수를 가져오는 메소드)
	 *  selectCurBall() : prop에 연결된 Properties안에 있는 key값이 selectCurBall인 쿼리문에
	 *  				  acntNum(회원 번호)을 넣어 
	 *                    유저의 현재 몬스터볼 갯수를 리턴한다.
	 * </pre>
	 * @param Connection con : DB에 연결하기위한 Connection 타입의 주소값을 저장하는 참조변수
	 * @param int acntNum : 회원 번호
	 * @return int curBall : 현재 몬스터볼의 갯수
	 * @author 박상범
	 */
	public int selectCurBall(Connection con, int acntNum) {

		PreparedStatement pstmt = null;

		ResultSet rset = null;

		int curBall = 0;

		String query = prop.getProperty("selectCurBall");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, acntNum);
			rset = pstmt.executeQuery();

			while (rset.next()) {
				curBall = rset.getInt("CUR_BALL");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return curBall;
	}

	// ------------------------------------------------포켓몬 뽑기----------------------------------------------

	/**
	 * <pre>
	 * (현재 유저의 몬스터볼 갯수를 가져오는 메소드)
	 *  selectCurBall() : prop에 연결된 Properties안에 있는 key값이 selectAllPokemon인 쿼리문을 실행해
	 *                    모든 포켓몬의 번호를 담은 List를 리턴한다.
	 * </pre>
	 * @param Connection con : DB에 연결하기위한 Connection 타입의 주소값을 저장하는 참조변수
	 * @return List<Integer> pokemonList : 모든 포켓몬의 번호를 담고 있는 List
	 * @author 박상범
	 */
	public List<Integer> selectAllPokemon(Connection con) {

		PreparedStatement pstmt = null;

		ResultSet rset = null;

		List<Integer> pokemonList = new ArrayList<>();

		String query = prop.getProperty("selectAllPokemon");

		try {
			pstmt = con.prepareStatement(query);
			rset = pstmt.executeQuery();

			while (rset.next()) {

				pokemonList.add(rset.getInt("POKEMON_NUM"));

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}

		return pokemonList;
	}

	/**
	 * <pre>
	 * (현재 유저가 보유한 포켓몬 번호 List를 가져오는 메소드)
	 *  selectAccountGetPokemon() : prop에 연결된 Properties안에 있는 key값이 selectAccountGetPokemon인 쿼리문에
	 *  				  			acntNum(회원 번호)을 넣어 
	 *                   			유저가 보유한 포켓몬 번호 List를 리턴한다.
	 * </pre>
	 * @param Connection con : DB에 연결하기위한 Connection 타입의 주소값을 저장하는 참조변수
	 * @param int acntNum : 회원 번호
	 * @return List<Integer> getPokemonList : 현재 몬스터볼의 갯수
	 * @author 박상범
	 */
	public List<Integer> selectAccountGetPokemon(Connection con, int acntNum) { // 유저가 보유한 포켓몬 리스트 불러오기

		PreparedStatement pstmt = null;

		ResultSet rset = null;

		List<Integer> getPokemonList = null;

		String query = prop.getProperty("selectAccountGetPokemon");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, acntNum);
			rset = pstmt.executeQuery();

			getPokemonList = new ArrayList<>();

			while (rset.next()) {
				getPokemonList.add(rset.getInt("POKEMON_NUM"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return getPokemonList;
	}

	/**
	 * <pre>
	 * (유저의 현재 몬스터 볼 갯수에서 -100을 하는 메소드)
	 *  minus100CurBall() : prop에 연결된 Properties안에 있는 key값이 minus100Ball인 쿼리문에
	 *  				  	acntNum(회원 번호)을 넣어 쿼리문을 실행후 
	 *                   	-100 성공 여부를 리턴한다.
	 * </pre>
	 * @param Connection con : DB에 연결하기위한 Connection 타입의 주소값을 저장하는 참조변수
	 * @param int acntNum : 회원 번호
	 * @return int result : -100 성공 여부
	 * @author 박상범
	 */
	public int minus100CurBall(Connection con, int acntNum) {

		PreparedStatement pstmt = null;

		int result = 0;

		String query = prop.getProperty("minus100Ball");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, acntNum);

			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		if (result > 0) {
			System.out.println("-100성공");
		} else {
			System.out.println("-100실패");
		}
		return result;
	}

	/**
	 * <pre>
	 * (유저의 현재 몬스터 볼 갯수에서 +30을 하는 메소드)
	 *  minus100CurBall() : prop에 연결된 Properties안에 있는 key값이 plus30Ball인 쿼리문에
	 *  				  	acntNum(회원 번호)을 넣어 쿼리문을 실행후 
	 *                   	+30 성공 여부를 리턴한다.
	 * </pre>
	 * @param Connection con : DB에 연결하기위한 Connection 타입의 주소값을 저장하는 참조변수
	 * @param int acntNum : 회원 번호
	 * @return int result : +30 성공 여부
	 * @author 박상범
	 */
	public int plus30CurBall(Connection con, int acntNum) { // 몬스터볼 +30

		PreparedStatement pstmt = null;

		int result = 0;

		String query = prop.getProperty("plus30Ball");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, acntNum);

			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		if (result > 0) {
			System.out.println("+30성공");
		} else {
			System.out.println("+30실패");
		}

		return result;
	}

	/**
	 * <pre>
	 * (유저가 뽑은 포켓몬 번호를 포켓몬 뽑기 내역에 추가하는 메소드)
	 *  minus100CurBall() : prop에 연결된 Properties안에 있는 key값이 insertGetPokemon인 쿼리문에
	 *  				  	acntNum(회원 번호)와, pokemonNum(포켓몬 번호)를 넣어 쿼리문을 실행후 
	 *                   	포켓몬 뽑기 내역 추가 성공 여부를 리턴한다.
	 * </pre>
	 * @param Connection con : DB에 연결하기위한 Connection 타입의 주소값을 저장하는 참조변수
	 * @param int pokemonNum : 포켓몬 번호
	 * @param int acntNum : 회원 번호
	 * @return int result : 포켓몬 뽑기 내역 추가 성공 여부
	 * @author 박상범
	 */
	public int insertGetPokemon(Connection con, int pokemonNum, int acntNum) { // 포켓몬획득내역추가

		PreparedStatement pstmt = null;

		int result = 0;

		String query = prop.getProperty("insertGetPokemon");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, pokemonNum);
			pstmt.setInt(2, acntNum);

			result = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return result;
	}

//----------------------------------------------------코스튬----------------------------------------------

	/**
	 * <pre>
	 * (선택한 코스튬의 가격을 불러오는 메소드)
	 *  selectCostumePrice() : prop에 연결된 Properties안에 있는 key값이 selectCostumePrice인 쿼리문에
	 *  				  	costumeNum(코스튬 번호)를 넣어 쿼리문을 실행후 
	 *                   	선택한 코스튬의 가격을 리턴한다.
	 * </pre>
	 * @param Connection con : DB에 연결하기위한 Connection 타입의 주소값을 저장하는 참조변수
	 * @param int costumeNum : 코스튬 번호
	 * @return int costumePrice : 포켓몬 뽑기 내역 추가 성공 여부
	 * @author 박상범
	 */
	public int selectCostumePrice(Connection con, int costumeNum) {

		PreparedStatement pstmt = null;

		ResultSet rset = null;

		int costumePrice = 0;

		String query = prop.getProperty("selectCostumePrice");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, costumeNum);

			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				costumePrice = rset.getInt("COSTUME_PRICE");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		return costumePrice;
	}

	/**
	 * <pre>
	 * ((유저의 현재 몬스터볼 갯수 - 코스튬 가격)을 하는 메소드)
	 *  minusPriceCurBall() : prop에 연결된 Properties안에 있는 key값이 minusPirceBall인 쿼리문에
	 *  				  	  costumePrice(코스튬 가격)과
	 *  					  acntNum(회원 번호)를 넣어 쿼리문을 실행후 
	 *                   	    쿼리문 성공여부를 리턴한다.
	 * </pre>
	 * @param Connection con : DB에 연결하기위한 Connection 타입의 주소값을 저장하는 참조변수
	 * @param int costumePrice : 코스튬 가격
	 * @param int acntNum : 회원 번호
	 * @return int result : -코스튬 가격 성공 여부
	 * @author 박상범
	 */
	public int minusPriceCurBall(Connection con, int costumePrice, int acntNum) {

		PreparedStatement pstmt = null;

		int result = 0;

		String query = prop.getProperty("minusPirceBall");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, costumePrice);
			pstmt.setInt(2, acntNum);

			result = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
	}

	/**
	 * <pre>
	 * (유저의 는 메소드)
	 *  selectGetCostume() : prop에 연결된 Properties안에 있는 key값이 minusPirceBall인 쿼리문에
	 *  				  	  costumePrice(코스튬 가격)과
	 *  					  acntNum(회원 번호)를 넣어 쿼리문을 실행후 
	 *                   	    쿼리문 성공여부를 리턴한다.
	 * </pre>
	 * @param Connection con : DB에 연결하기위한 Connection 타입의 주소값을 저장하는 참조변수
	 * @param int acntNum : 회원 번호
	 * @return List<Integer> getCostumeList : 유저가 보유한 코스튬 번호 List
	 * @author 박상범
	 */
	public List<Integer> selectGetCostume(Connection con, int acntNum) { // 유저가 구매한 코스튬 리스트 불러오기

		PreparedStatement pstmt = null;

		ResultSet rset = null;

		List<Integer> getCostumeList = null;

		String query = prop.getProperty("selectGetCostume");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, acntNum);

			rset = pstmt.executeQuery();

			getCostumeList = new ArrayList<>();

			while (rset.next()) {
				getCostumeList.add(rset.getInt("COSTUME_NUM"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		return getCostumeList;
	}

	public int insertBuyCostume(Connection con, int costumNum, int acntNum) { // 코스튬 구매내역 추가

		PreparedStatement pstmt = null;

		int result = 0;

		String query = prop.getProperty("insertBuyCostume");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, costumNum);
			pstmt.setInt(2, acntNum);

			result = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return result;
	}

}
