package com.greedy.pokemonrun.model.dao;

import static com.greedy.pokemonrun.common.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.Properties;

import com.greedy.pokemonrun.model.dto.AccountDTO;

/**
 * <pre>
 * Class : AdminDAO
 * Comment : 관리자 페이지  
 * History
 * 2021/12/13 (박휘림)
 * </pre>
 * @version 1
 * @author 박휘림
 * */

public class AdminDAO {
   
   /* 프로퍼티스 파일을 읽어오기 위해 객체 생성 */
   private Properties prop = new Properties();
   
   public AdminDAO() {
      
      try {
    	  
         prop.loadFromXML(new FileInputStream("mapper/hwirim-query.xml"));
         
      } catch (InvalidPropertiesFormatException e) {
         e.printStackTrace();
      } catch (FileNotFoundException e) {
         e.printStackTrace();
      } catch (IOException e) {
         e.printStackTrace();
      }
      
   }
   
   /**
    * selectAllAccounts : account테이블에 있는 회원 계정 정보를 전부 조회한다.
    * @return 모든 회원정보를 담은 accountList
    * @author 박휘림
    * */
   public List<AccountDTO> selectAllAccounts(Connection con) {
      
	  PreparedStatement pstmt = null;
	  
      ResultSet rset = null;
      
      List<AccountDTO> accountList = null;
    
      String query = prop.getProperty("selectAllAccounts");
      
      try {
    	
         pstmt = con.prepareStatement(query);
        
         rset = pstmt.executeQuery();
         
         accountList = new ArrayList<>();
        
         while(rset.next()) {
            AccountDTO account = new AccountDTO();
           
            account.setAcntNum(rset.getInt("ACNT_NUM"));
            account.setAcntKind(rset.getString("ACNT_KIND"));
            account.setAcntId(rset.getString("ACNT_ID"));
            account.setAcntPwd(rset.getString("ACNT_PW"));
            account.setDelYn(rset.getString("DEL_YN"));
            account.setDelDate(rset.getDate("DEL_DATE"));
            account.setMemName(rset.getString("MEM_NAME"));
            account.setMemEmail(rset.getString("MEM_EMAIL"));
            account.setEntDate(rset.getDate("ENT_DATE"));
            
            accountList.add(account);
         }
         
      } catch (SQLException e) {
         e.printStackTrace();
      } finally {
         close(pstmt);
         close(rset);
      }
 
      return accountList;
      
   }
   
   /**
    * updatePassword : 회원 계정 아이디를 입력해 해당 계정의 비밀번호를 수정한다.
    * @param first 비밀번호를 수정할 회원 계정 아이디
    * @param second 수정할 비밀번호 
    * @return 비밀번호 변경에 성공했다면 1을 리턴하고 실패하면 0을 리턴
    * @author 박휘림
    * */
   public int updatePassword(Connection con, String acntId, String acntPwd) {
	  
      PreparedStatement pstmt = null;
      
      int result = 0;
      
      String query = prop.getProperty("updatePassword");
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, acntPwd);         
         pstmt.setString(2, acntId);
         
         result = pstmt.executeUpdate();
         
      } catch (SQLException e) {
         e.printStackTrace();
      } finally {
         close(pstmt);
      }
      
      return result;
   }
   
   /**
    * deleteAccount : 회원 계정 아이디를 입력해 존재하는 계정일 경우 계정 삭제한다.
    * @param 삭제할 계정 아이디
    * @return 계정 삭제에 성공하면 1을 리턴, 실패하면 0을 리턴
    * @author 박휘림
    * */
   public int deleteAccount(Connection con, String acntId) {
      
      PreparedStatement pstmt = null;
      
      int result = 0;
      
      String query = prop.getProperty("deleteAccount");
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, acntId);
         
         result = pstmt.executeUpdate();
         
      } catch (SQLException e) {
         e.printStackTrace();
      } finally {
         close(pstmt);
      }
      
      return result;
      
   }
}