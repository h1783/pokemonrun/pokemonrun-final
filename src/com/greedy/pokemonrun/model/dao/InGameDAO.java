package com.greedy.pokemonrun.model.dao;

import static com.greedy.pokemonrun.common.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.InvalidPropertiesFormatException;
import java.util.Map;
import java.util.Properties;

import com.greedy.pokemonrun.model.dto.GameHistoryDTO;

public class InGameDAO {
	
	private Properties prop = new Properties();
	
	public InGameDAO() {
		
		try {
			prop.loadFromXML(new FileInputStream("mapper/hwirim-query.xml"));
		
		} catch (InvalidPropertiesFormatException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public int insertGameHistory(Connection con, GameHistoryDTO gamehistoryDTO) {
		
		PreparedStatement pstmt = null;
		
		int result = 0;
		
		String query = prop.getProperty("insertGameHistory");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, gamehistoryDTO.getGetScore());
			pstmt.setInt(2, gamehistoryDTO.getGetBall());
			pstmt.setInt(3, gamehistoryDTO.getUsePokemonNum());
			pstmt.setInt(4, gamehistoryDTO.getMapNum());
			pstmt.setInt(5, gamehistoryDTO.getAcntNum());
			pstmt.setInt(6, gamehistoryDTO.getUseCostume());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int updateCurBall(Connection con, int acntNum, int curBall) {
		
		PreparedStatement pstmt = null;
		
		int result = 0;
		
		String query = prop.getProperty("updateCurBall");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, curBall);
			pstmt.setInt(2, acntNum);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
		
	}
	
	public int updateHighScore(Connection con, int acntNum, int highScore) {
		
		PreparedStatement pstmt = null;
		
		int result = 0;
		
		String query = prop.getProperty("updateHighScore");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, highScore);
			pstmt.setInt(2, highScore);
			pstmt.setInt(3, acntNum);
			pstmt.setInt(4, highScore);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
}








