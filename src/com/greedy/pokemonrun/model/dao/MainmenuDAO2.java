package com.greedy.pokemonrun.model.dao;

import static com.greedy.pokemonrun.common.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

/*
 * <pre>
 * Class : MainmenuDAO2
 * Comment : 랭킹 DB 쿼리문 로딩 클래스
 * History
 * 2021/12/13 (차화응) 처음 작성
 * 2021/12/14 (차화응) 구문 수정
 * </pre>
 * @version 1
 * @author 차화응
 * */
public class MainmenuDAO2 {
	
	private Properties prop;
		
		public MainmenuDAO2() {
		
			prop = new Properties();
			try {
				prop.loadFromXML(new FileInputStream("mapper/hwaoung-query.xml"));
			
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		public Map<String, Integer> rankInfo(Connection con) {
			
			PreparedStatement pstmt = null;
			
			ResultSet rset = null;
			
			Map<String, Integer> rankList = null;
			
			String query = prop.getProperty("rankInfo");
			
			try {
				rankList = new LinkedHashMap<>();
				pstmt = con.prepareStatement(query);
				rset = pstmt.executeQuery();
				
				int cnt = 0;
				while(rset.next()) {
					
					rankList.put(rset.getString("ACNT_ID"), rset.getInt("GET_SCORE"));
					cnt++;
					if(cnt > 4) {
						break;
					}
					
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(rset);
				close(pstmt);
			}
			
			return rankList;
		}
	
}
