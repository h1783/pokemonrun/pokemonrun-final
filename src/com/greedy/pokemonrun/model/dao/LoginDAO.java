package com.greedy.pokemonrun.model.dao;

import static com.greedy.pokemonrun.common.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;

import com.greedy.pokemonrun.model.dto.AccountDTO;

/** 
 * <pre>
 * Class : LoginDAO
 * Comment : 로그인 페이지의 로그인, 회원가입, ID/PWD찾기에 사용되는 쿼리문을 실행하는 메소드들의 모음. DAO
 * History
 * 2021/12/13 (홍성원) 작성
 * </pre>
 * @version 1
 * @author 홍성원
 * */
public class LoginDAO {

   /*
    * 초기화면에서 사용할 쿼리문들을 모아서 관리하는 DAO 메소드들이 공용으로 쓰는 Properties 변수를 필드로 선언해주고 기본생성자 호출
    * 시 mapper폴더 안에있는 쿼리문과 연결을 해준다.
    */
   private Properties prop = new Properties();

   public LoginDAO() {
      try {
         prop.loadFromXML(new FileInputStream("mapper/sungwon-query.xml"));
      } catch (IOException e) {
         e.printStackTrace();
      }
   }

   /* 회원정보로 Account테이블에 새로운 행을 삽입하는 쿼리문 호출부분 */
   public int insertAccount(Connection con, AccountDTO accountDTO) {
      PreparedStatement pstmt = null;
      int result = 0;

      String query = prop.getProperty("insertAccount");

      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, accountDTO.getAcntId());
         pstmt.setString(2, accountDTO.getAcntPwd());
         pstmt.setString(3, accountDTO.getMemName());
         pstmt.setString(4, accountDTO.getMemEmail());

         result = pstmt.executeUpdate();

      } catch (SQLException e) {
         e.printStackTrace();
      } finally {
         close(pstmt);
      }
      return result;
   }

   /** 
    * getAccountNumById : 회원의 아이디로 계정번호를 조회하는 메소드
    * @param con : DB에 연결하기위한 Connection 타입의 주소값을 저장하는 참조변수
    * @param acntId : 계정번호를 조회하려는 회원의 아이디
    * @return acntNum : 조회한 회원번호
    * @author 홍성원
    * */
    public int getAccountNumById(Connection con, String accountId) {
    
       PreparedStatement pstmt = null; 
       int acntNum = 0; 
       ResultSet rset = null;
    
       String query = prop.getProperty("getAccountNumById");
       try {
          pstmt = con.prepareStatement(query);
          pstmt.setString(1, accountId);
          rset = pstmt.executeQuery(); 
          if(rset.next()) {
        	  acntNum = rset.getInt("ACNT_NUM"); 
          }
      } catch (SQLException e) { 
         e.printStackTrace(); 
      } finally {
         close(rset);
         close(pstmt);
      }
       return acntNum; 
    }
    
   
    /** 
     * insertPlayer : 회원가입시 같이 삽입해줘야하는 플레이어를 삽입하는 메소드
     * @param con : DB에 연결하기위한 Connection 타입의 주소값을 저장하는 참조변수
     * @return result : 삽입결과. 성공이면1, 실패하면 0.
     * @author 홍성원
     * */
   public int insertPlayer(Connection con) {
	  
	   PreparedStatement pstmt = null;
	   int result = 0;
	   String query = prop.getProperty("insertPlayer");

	   try {
		   pstmt = con.prepareStatement(query);
		   result = pstmt.executeUpdate();

	   } catch (SQLException e) {
		   e.printStackTrace();
	   }
	   return result;
	}

    /** 
    * insertPlayer : 회원가입시 같이 삽입해줘야하는 포켓몬뽑기내역을 삽입하는 메소드
    * 					회원가입시 초기에 피카츄를 갖고 시작하기떄문에 해당 회원번호로 피카츄 구입내역을 같이 삽입해준다
    * @param con : DB에 연결하기위한 Connection 타입의 주소값을 저장하는 참조변수
    * @return result : 삽입결과. 성공이면1, 실패하면 0.
    * @author 홍성원
    * */
	public int insertDrawPokemonHistory(Connection con) {

		PreparedStatement pstmt = null;
		int result = 0;
		String query = prop.getProperty("insertDrawPokemonHistory");

		try {	
			pstmt = con.prepareStatement(query);
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	   return result;
   	}

   

	/** 
	    * insertPlayer : 회원가입시 같이 삽입해줘야하는 포켓몬뽑기내역을 삽입하는 메소드
	    * 					회원가입시 초기에 피카츄를 갖고 시작하기떄문에 해당 회원번호로 피카츄 구입내역을 같이 삽입해준다
	    * @param con : DB에 연결하기위한 Connection 타입의 주소값을 저장하는 참조변수
	    * @return result : 삽입결과. 성공이면1, 실패하면 0.
	    * @author 홍성원
	    * */
	public String findIdByName(Connection con, String memName) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
     	String acntId = null;
     	String query = prop.getProperty("findNumByEmail"); 
      
     	try {
     		pstmt = con.prepareStatement(query);
     		pstmt.setString(1, memName);
     		rset = pstmt.executeQuery();
     		acntId = rset.getString("MEM_NAME");
     	} catch (SQLException e) {
     		e.printStackTrace();
     	} finally {
     		close(pstmt);
     		close(rset);
     	}

     	return acntId;
	}


   
   
   

   /* 로그인기능에 사용하는 DAO 모음
    * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
   

   /**
    * getAccountDTOById : 전달받은 아이디로 전체 회원을 조회한 후 , 만약 해당 아이디의 회원이 있다면 그 회원의 모든 정보를 반환하는 메소드.
    * 
    * @param con : DB에 연결하기위한 Connection 타입의 주소값을 저장하는 참조변수
    * @param acntId : 회원의 비밀번호
    * @param result : 회원정보 유무 결과를 저장한다. 회원정보가 없다면 0을, 회원이라면 정수형인 회원번호를 저장한다
    * @author 홍성원
    * */
   public AccountDTO getAccountDTOById(Connection con, String acntId) {
	   
	   /* DB와 연결하기 위한 변수들 */
       PreparedStatement pstmt = null;							//DB에 보낼 쿼리 구문을 담을 변수 
       ResultSet rset = null;									//쿼리문으로 DB에서 값을 조회해, 그 값을 저장할 변수
       String query = prop.getProperty("getAccountDTOById");	//Property형태로 저장된 쿼리문중 필요한 쿼리문을 key값으로 뺴와서 query문자열에 저장 
       
       AccountDTO acntDTO = new AccountDTO();					//조회된 결과를 담을 DTO변수 생성					
      
       try {
    	   pstmt = con.prepareStatement(query); 				//쿼리문을 PreparedStatement에 담은 후, 오라클과 연결을 해준다. 
    	   pstmt.setString(1, acntId);							//쿼리문에 비워둔 변수부분에 값을 대입해준다

    	   rset = pstmt.executeQuery();							//DB와 연결된 쿼리문을 실행한 후 그 값을 rset에 담는다.
    	   if(rset.next()) {									//rset에 값이 없을때 까지 DTO에 저장을 해준다
    	      acntDTO.setAcntNum(rset.getInt("ACNT_NUM"));
    	   	   acntDTO.setAcntPwd(rset.getString("ACNT_PW"));
    	   	   acntDTO.setAcntKind(rset.getString("ACNT_KIND"));
    	   	   acntDTO.setDelYn(rset.getString("DEL_YN"));
    	   }
       } catch(SQLException e) {
         e.printStackTrace();
       } finally {												//사용이 끝난 변수들은 할당을 해제해준다
    	   close(pstmt);
    	   close(rset);
       }	
      
       return acntDTO;											//회원정보를 담은 DTO를 반환한다
   }

   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   /* 아이디 찾기 DAO모음  ++비밀번호할때도 사용
    * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
   /* 유니크 제약조건이 있는 이메일을 이용해 이름을 찾아서 반환
    * 반환값은 없거나 하나만 있다 */
   
   /** 
    * findAccountDTOByEmail : 입력받은 이메일로 회원테이블에서 해당 회원의 정보를 저장 후 반환해주는 메소드
    * @param con : 오라클과의 연결을 위한 변수
    * @param memEmail : 유일성이 보장된 email을 사용해 해당 회원의 정보를 조회한다
    * @return acntDTO : 조회한 회원의 정보를 저장한다
    * 
    * @author 홍성원
    * */
   public AccountDTO findAccountDTOByEmail(Connection con, String memEmail) {
      AccountDTO acntDTO = null;
      PreparedStatement pstmt = null;
      ResultSet rset = null;
      String query = prop.getProperty("getAccountDTOByEmail");
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, memEmail);
         rset = pstmt.executeQuery();
         
         acntDTO = new AccountDTO();
         if(rset.next()) {
            acntDTO.setAcntId(rset.getString("ACNT_ID"));
            acntDTO.setMemName(rset.getString("MEM_NAME"));
            acntDTO.setDelYn(rset.getString("DEL_YN"));
            acntDTO.setMemEmail(rset.getString("MEM_EMAIL"));
            acntDTO.setAcntPwd(rset.getString("ACNT_PW"));
         }
      } catch (SQLException e) {
         e.printStackTrace();
      } finally {
         close(pstmt);
         close(rset);
      }
      return acntDTO;
   }

   /* 
    * 비밀번호 찾기에 사용하는 두번쨰 DAO 첫번째는 아이디찾기에 같이 쓴다. 
    * */
   public int updateAcntPwd(Connection con, String acntId, String pwd) {
      PreparedStatement pstmt = null;
      int result = 0;
      
      String query = prop.getProperty("updateAcntPwdById");
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setString(1, pwd);
         pstmt.setString(2, acntId);

         result = pstmt.executeUpdate();
      } catch (SQLException e) {
         e.printStackTrace();
      } finally {
         close(pstmt);
      }
      return result;
   }
   
   
   
   
   
   /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    * 
    * 플레이어 아이디로 현재 포켓몬 알아내는 쿼리문.. 
    * 
    * */
   public int getCurPokemonByNum(Connection con,  int acntNum) {
      PreparedStatement pstmt = null;
      ResultSet rset = null;
      int curPoke = 0;
      String query = prop.getProperty("getCurPokemonById"); 
      
      try {
         
         pstmt = con.prepareStatement(query);
         pstmt.setInt(1, acntNum);
         rset = pstmt.executeQuery();
         System.out.println("dddd");
         if(rset.next()) {
            curPoke = rset.getInt("CUR_POKEMON_NUM");
         }
      } catch (SQLException e) {
         e.printStackTrace();
      } finally {
         close(pstmt);
         close(rset);
      }

      return curPoke;
   }
   
   public int getCurCostumeByNum(Connection con,  int acntNum) {
        PreparedStatement pstmt = null;
        ResultSet rset = null;
        int curCos = 0;
        String query = prop.getProperty("selectCurCos"); 

        try {

            pstmt = con.prepareStatement(query);
            pstmt.setInt(1, acntNum);
            rset = pstmt.executeQuery();
            if(rset.next()) {
                curCos = rset.getInt("COSTUME_NUM");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(pstmt);
            close(rset);
        }

        return curCos;
    }
   /* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    * 플레이어넘버, 포켓몬번호를 전달받는다.
    * 해당 플레이어가 해당 포켓몬을 가지고있는지 알아내는 쿼리문..
    * */
   public int findOwnPoke(Connection con, int acntNum, int pokeNum) {
      int result = 0;
      PreparedStatement pstmt = null;
      ResultSet rset = null;
      String query = prop.getProperty("findOwnPoke");
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setInt(1, acntNum);
         pstmt.setInt(2, pokeNum);
         rset = pstmt.executeQuery();
         
         if(rset.next()) {
            result = rset.getInt("POKEMON_NUM");
         }
         
      } catch (SQLException e) {
         e.printStackTrace();
      } finally {
         close(rset);
         close(pstmt);
      }
      
      return result;
   }

   public int updateCurPoke(Connection con, int acntNum, int pokeNum) {

      int result = 0;
      PreparedStatement pstmt = null;
      String query = prop.getProperty("updateCurPoke");
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setInt(1, pokeNum);
         pstmt.setInt(2, acntNum);
         
         result = pstmt.executeUpdate();
         
      } catch (SQLException e) {
         e.printStackTrace();
      } finally{
         close(pstmt);
      }

      
      
      return result;
   }

   public int findOwnCostume(Connection con, int acntNum, int costumNum) {
   
      int result = 0;
      PreparedStatement pstmt = null;
      ResultSet rset = null;
      String query = prop.getProperty("findCurCos");
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setInt(1, acntNum);
         pstmt.setInt(2, costumNum);
         
         rset = pstmt.executeQuery();
         
         if(rset.next()) {
            result = rset.getInt("COSTUME_NUM");
         }
         
      } catch (SQLException e) {
         e.printStackTrace();
      } finally {
         close(rset);
         close(pstmt);
      }

      return result;
   }

   public int updateCostume(Connection con, int acntNum, int costumNum) {
      
      int result = 0;
      PreparedStatement pstmt = null;
      String query = prop.getProperty("updateCurCos");
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setInt(1, costumNum);
         pstmt.setInt(2, acntNum);
         
         result = pstmt.executeUpdate();
      } catch (SQLException e) {
         e.printStackTrace();
      }
      
      return result;
   }

   public int pokemonCheckByNum(Connection con, int acntNum, int i) {

      PreparedStatement pstmt = null;
      String query = prop.getProperty("pokemonCheckByNum");
      ResultSet rset = null;
      int result = 0;
      
      try {
         pstmt = con.prepareStatement(query);
         pstmt.setInt(1, acntNum);
         
         rset = pstmt.executeQuery();
         
         while(rset.next()) {
            if(rset.getInt("POKEMON_NUM") == i) {
            result = 1;
            }
         }
         
      } catch (SQLException e) {
         e.printStackTrace();
      } finally {
         close(pstmt);
         close(rset);
      }
      
      return result;
   }
}































