package com.greedy.pokemonrun.model.service;

import static com.greedy.pokemonrun.common.JDBCTemplate.close;
import static com.greedy.pokemonrun.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import java.util.Map;

import com.greedy.pokemonrun.model.dao.MainmenuDAO2;

/*
 * <pre>
 * Class : MainmenuService2
 * Comment : 랭킹 DB를 rankList에 대입하는 클래스
 * History
 * 2021/12/13 (차화응) 처음 작성
 * 2021/12/17 (구훈모) 구문 완료
 * </pre>
 * @version 1
 * @author 차화응
 * */
public class MainmenuService2 {

	public Map<String, Integer> rankInfo() {
		
		MainmenuDAO2 mainmenuDAO = new MainmenuDAO2();

		Connection con = getConnection();
		
		Map<String, Integer> rankList = mainmenuDAO.rankInfo(con);
		
		close(con);
		
		return rankList;
	}


}
