package com.greedy.pokemonrun.model.service;

import static com.greedy.pokemonrun.common.JDBCTemplate.close;
import static com.greedy.pokemonrun.common.JDBCTemplate.commit;
import static com.greedy.pokemonrun.common.JDBCTemplate.getConnection;
import static com.greedy.pokemonrun.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.Map;

import com.greedy.pokemonrun.model.dao.LoginDAO;
import com.greedy.pokemonrun.model.dto.AccountDTO;

/** 
 * <pre>
 * Class : LoginService
 * Comment : 로그인 페이지의 로그인, 회원가입, ID/PWD찾기를 담당하는 서비스 
 * History
 * 2021/12/13 (홍성원) 처음 작성
 * </pre>
 * @version 1
 * @author 홍성원
 * */
public class LoginService {

   private LoginDAO loginDAO; 

   /**
    * registAccountCheckInput : 회원가입시 사용자가 입력한 아이디와 이메일주소가 삽입이 되는지 확인하여, 중복여부를 판별하는 메소드
    * 							
    * @param regist : 사용자가 입력한 개인정보를 저장하고있는 Map
    * @return result : 성공여부를 반환.
    * 
    * @author 홍성원
    * */
   public int registAccountCheckInput(Map<String, String> regist) {

      Connection con = getConnection();
      loginDAO = new LoginDAO();
      AccountDTO acntDTO = new AccountDTO();
      acntDTO.setAcntId(regist.get("acntId"));
      acntDTO.setMemName(regist.get("memName"));
      acntDTO.setMemEmail(regist.get("memEmail"));
      /* 임의로 삽입할 1회성 비밀번호 */
      acntDTO.setAcntPwd("qlalfqjsgh1");

      /* 삽입이 될 경우, 회원이 입력한 아이디와 이메일이 중복되지 않는것 */
      int result = loginDAO.insertAccount(con, acntDTO);
      
      /* 판별이 끝난후 삽입한 정보들을 바로 롤백시켜 삭제한다. */
      rollback(con);
      close(con);

      return result;
   }
 
   
   /** 
    * registAccountCheckInput2 : 회원가입중, 아이디 이메일의 중복을 확인한 후 비회원이 입력한비밀번호를 저장하는 서비스 
    * @param regist : 회원의 회원가입시 필요한 이름, 아이디, 이메일, 비밀번호를 담고있는 Map
    * @return memId : 회원의 회원번호를 조회하여 반환한다.
    * 
    * @author 홍성원
    * */
   public int registAccountCheckInput2(Map<String, String> regist) {

        Connection con = getConnection();
        loginDAO = new LoginDAO();
    
        AccountDTO acntDTO = new AccountDTO();
        acntDTO.setAcntId(regist.get("acntId"));
        acntDTO.setMemName(regist.get("memName"));
        acntDTO.setMemEmail(regist.get("memEmail"));
        acntDTO.setAcntPwd(regist.get("acntPwd"));
        int acntNum = 0;
        
        /*  */
        int result1 = loginDAO.insertAccount(con, acntDTO);
        
        /* 플레이어테이블에 회원정보를 삽입 */
        int result2 = loginDAO.insertPlayer(con);
        
        /* 뽑기내역 삽입 */
        int result3 = loginDAO.insertDrawPokemonHistory(con);
        if(result1 * result2 * result3 > 0) {
        	
        	/* 만약 회원정보가 정상적으로 삽입이 됐다면 커밋을 하고, 회원번호를 조회하여 컨트롤러로 전달해준다 */
        	acntNum = loginDAO.getAccountNumById(con, acntDTO.getAcntId());
            commit(con);
        } else {
        	/* 만약 정상적으로 회원정보가 저장되지 않았다면 롤백을 한다. */
            rollback(con);
        }

        close(con);
        return acntNum;
    }
    
   /** 
    * findAccountId : 아이디 찾기기능의 흐름을 제어하는 Service
    * 					이름과 이메일을 입력받아 이메일에 해당하는 이름을 찾은 후 
    * 					그 이름과 입력받은 이름을 비교한 후 결과를 반환한다.
    * 					만약 입력한 이름과 이메일에 해당되는 이름이 다른경우 아이디찾기 실패 처리를 한다.
    * @param memInf : 회원의 이름과, 이메일을 전달인자로 받는다
    * @return memId : 회원의 아이디를 반환한다.
    * 
    * @author 홍성원
    * */
   	public String findAccountId(String memName, String memEmail) {
      
   		loginDAO = new LoginDAO();
   		Connection con = getConnection();
   		AccountDTO acntDTO = null;
   		String acntId = "";
      
   		/* 유니크제약조건이 걸린 이메일로, 해당회원의 모든정보를 DTO자료형으로 받는다. */
   		acntDTO = loginDAO.findAccountDTOByEmail(con, memEmail);
      
   		/* DTO가 Null이 아니고, ID가 존재할 때, 이메일로 조회한 회원의 이름이 입력받은 이름과 같다면 Id를 변수에 담아준다.
   		 * 이떄 만약 탈퇴한 회원이라면 빈 문자열을 대입해준다 */
   		if(acntDTO!= null && acntDTO.getAcntId()!=null && memName.equals(acntDTO.getMemName())) {
   			acntId = acntDTO.getAcntId();
   			System.out.println(acntId);
    	  
   			/* 선택한 회원이 혹시 탈퇴한 회원이라면 아이디를 반환해주지 않기 위해 acnId를 빈 문자열로 설정해준다. */
   			if(acntDTO.getDelYn().equals("Y")) {
   				acntId = "";
   			}
   		}
   		close(con);
     
   		return acntId;
   	}
   
   /** 
    * findMemberPwd : 비밀번호찾기의 흐름을 제어하는서비스. 입력한 이름과 아이디, 이메일을 갖고있는 회원이 있다면 그 회원의 비밀번호를 전달해준다.
    * @param memName : 회원의 이름을 전달한다.
    * @param acntId : 회원의 아이디를 전달한다.
    * @param memEmail : 회원의 이메일을 전달한다.
    * @return acntPwd : 회원의 비밀번호를 알려주기위해 반환한다.
    * @author 홍성원
    * */
   public String findMemberPwd(String memName, String acntId, String memEmail) {
      
	   loginDAO = new LoginDAO();
	   Connection con = getConnection();
	   String acntPwd = "";
	   AccountDTO acntDTO = null;
	   
	   /* 이메일을 전달해, 그 이메일을 갖고있는 회원의 모든 정보를 반환받는다 */
	   acntDTO = loginDAO.findAccountDTOByEmail(con, memEmail);
      
	   //회원정보가 존재하는 경우 실행할 부분 >> 아이디 이름 비밀번호 세개의 값을 비교해서, 전부다 같아야 비밀번호 전송하고, 업데이트
	   /* 이메일이 null이 아닌경우 아래 조건을 비교하여 비밀번호를 반환해준다 */
	   if(acntDTO.getMemEmail() != null) {
		   
		   /* 이메일로 조회한 아이디와 회원이름을 입력받은 이름,아이디와 비교한다 */
		   if(acntDTO.getAcntId().equals(acntId) && acntDTO.getMemName().equals(memName)) {
			   
			   /* 만약 이름,아이디가 같고 비밀번호가  저장되어있으면 반환해줄 acntPwd에 비밀번호를 담아준다. */
			   if(acntPwd != null) {
				   acntPwd = acntDTO.getAcntPwd();
				   commit(con);
			   } else {
				   rollback(con);
			   }
		   }
	   }
      close(con);

      return acntPwd;
   }
   
   /**
    * login : 아이디와 비밀번호를 입력받아서, 현재 회원목록중 해당 회원이 있는지 확인 후 , 그 회원의 회원번호를 반환한다. 
    * 		     만약 회원정보가 없다면 0을 반환한다.
    * 
    * @param acntId : 회원의 아이디
    * @param acntPwd : 회원의 비밀번호
    * @param result : 회원정보 유무 결과를 저장한다. 회원정보가 없다면 0을, 회원이라면 정수형인 회원번호를 저장한다
    * @author 홍성원
    * */
   	public int login(String acntId, String acntPwd) {
   		/* 로그인DAO를 사용하기위해 인스턴스를 생성한다 */
   		loginDAO = new LoginDAO();
   		/* SQL과 연결하기위해 Connection클래스의 변수를 생성한다. */
   		Connection con = getConnection();
   		/* 회원정보를 저장할 회원정보DTO를 생성해준다 */
   		AccountDTO acntDTO = new AccountDTO();
   		/* 회원정보 조회결과를 담을 변수를 선언 후 0으로초기화한다 */
   		int result = 0;
	   
   		/* SQL과 연결하기위한 커넥션과, 조회를 할 아이디를 DAO안의 메소드에 담아 호출한다 */
   		acntDTO = loginDAO.getAccountDTOById(con, acntId); 

   		/* 먼저 DTO에 null이 아닌 값이 저장됐는지 확인을 하고, 반환받은 회원정보중, 비밀번호가, 사용자에게 입력받은 비밀번호와 같는지 판별한다. */
   		if(acntDTO.getAcntPwd() != null && acntDTO.getAcntPwd().equals(acntPwd)) {
   			/* 반환받은 회원정보가 혹시 삭제된 계정이 아닌지 판별한다ㅓ. */
   			if(acntDTO.getDelYn().equals("N")) {
			  	/* 만약 회원목록에 있고, 탈퇴한회원이 아니라면 회원번호를 조회한 후, result에 담아 컨트롤러에 반환한다 */
   				result = acntDTO.getAcntNum();   
   			}
   		}
   		close(con);

   		return result;
   	}




   	/**
     * updatePwdById : 회원가입중, 아이디 이름 이메일을 중복없이 입력하고, 비밀번호도 양식에 맞게 잘 입력해서 , 비밀번호를 회원아이디를 통해 
     * 					특정 아이디로, 그 아이디를 갖고있는 회원의 비밀번호를 업데이트 하는 기능의 서비스 메소드.
     * 				
     * 
     * @param acntId : 회원의 아이디
     * @param acntPwd : 회원의 비밀번호
     * @param result : 회원정보 유무 결과를 저장한다. 회원정보가 없다면 0을, 회원이라면 정수형인 회원번호를 저장한다
     * @author 홍성원
     * */
   public int updatePwdById(String acntId, String pwd) {
      Connection con = getConnection();
      loginDAO = new LoginDAO();
      int result = loginDAO.updateAcntPwd(con, acntId, pwd);
      
      if(result > 0) {
         commit(con);
      } else {
         rollback(con);
      }
      close(con);
      return result;
   }
   
   

   /*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    * 
    * 아이디를 입력해주면 현재 포켓몬을 알려주는 DAO를 호출한다
    * 
    * 
    * @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

   public int getCurPokemonNumByAcntNum(int anctNum) {
      int curPoke = 0;
      loginDAO = new LoginDAO();
      Connection con = getConnection();
      curPoke = loginDAO.getCurPokemonByNum(con, anctNum);
   
      
      close(con);
      return curPoke;
   }








   /* 포켓몬변경트랜잭션을 담당하는 서비스
    * 플레이어가 바꾸겠다는 포켓몬을 현재포켓몬으로 바꿔주는데;
    * 보유중이라면 바꿔주고, 커밋하고, 1반환,,,, 보유중이 아니라면 업데이트 안하고 그냥  0 반환
    *  */
   public int changePokemon(int acntNum, int pokeNum) {

      int result = 0;
      loginDAO = new LoginDAO();
      
      /* 우선 보내준 아이디에 , 바꾸겠다는 포켓몬이 있는지 확인해야됨 
       * 포켓몬뽑기내역에서 해당 회원번호의 플레이어가 뽑은 내역을 쫙 조회하고
       * 그 안에 넘겨받은 포켓몬번호가 있는지 확인해서 
       * result변수에 담아준다*/

      Connection con = getConnection();
      result = loginDAO.findOwnPoke(con, acntNum, pokeNum);
      
      
      
      /* result가 1이면 >> 포켓몬이 있다는거니까 update & commit하고 성공했다는 의미의 result=1로 return하고서비스 종료
       * result가 0이면 없다는거니까 그냥 0 return하고 종료
       * */
      
      if(result > 0) {
      
         result = loginDAO.updateCurPoke(con, acntNum, pokeNum);
         
         if(result > 0) {
            commit(con);
         } else {
            rollback(con);
         }
      }
      
      close(con);
      return result;
   }




   public int changeCostume(int acntNum, int costumNum) {

	   int result = 0;
	   loginDAO = new LoginDAO();
	   Connection con = getConnection();
	   result = loginDAO.findOwnCostume(con, acntNum, costumNum);
      
	   if(result > 0) {
		   result = loginDAO.updateCostume(con, acntNum,costumNum);

		   if(result > 0) {
			   commit(con);
		   } else {
			   rollback(con);
		   }
	   }
	   close(con);

	   return result;
   }      

   
   public int selectCurCostume(int acntNum) {
      int result = 0;
      
      loginDAO = new LoginDAO();
      
      Connection con = getConnection();
      result = loginDAO.getCurCostumeByNum(con, acntNum);
      
      close(con);
      return result;
   }

   public int pokemonCheckByNum(int acntNum, int i) {
      
	   Connection con = getConnection();
	   loginDAO = new LoginDAO();
	   int result = 0;
      
	   result = loginDAO.pokemonCheckByNum(con, acntNum, i);
      
	   close(con);
      
	   return result;
   }      
}