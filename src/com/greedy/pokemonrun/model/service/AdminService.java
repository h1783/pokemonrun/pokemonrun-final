package com.greedy.pokemonrun.model.service;

import static com.greedy.pokemonrun.common.JDBCTemplate.close;
import static com.greedy.pokemonrun.common.JDBCTemplate.commit;
import static com.greedy.pokemonrun.common.JDBCTemplate.getConnection;
import static com.greedy.pokemonrun.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.List;

import com.greedy.pokemonrun.model.dao.AdminDAO;
import com.greedy.pokemonrun.model.dto.AccountDTO;

/**
 * <pre>
 * Class : AdminService
 * Comment : 관리자 페이지
 * History
 * 2021/12/13 (박휘림)
 * </pre>
 * @version 1
 * @author 박휘림
 * */
public class AdminService {
	
   /* 필드에서 인스턴스 생성으로 클래스 안의 메소드들이 공유 가능 */
   private AdminDAO adminDAO = new AdminDAO();
   
   /**
    * selectAllAccounts : 
    * @return 모든 회원정보를 담은 accountList
    * @author 박휘림
    * */
   public List<AccountDTO> selectAllAccounts() {
  
      Connection con = getConnection();
      /* loginDAO2의 selectAllAccounts메소드에서 accountList를 받아온다. */
      List<AccountDTO> accountList = adminDAO.selectAllAccounts(con);
    
      close(con);
      
      return accountList;
   }
   
   /**
    * updatePassword : 회원 아이디를 입력해 해당 회원의 비밀번호를 수정한다.
    * @param first 비밀번호를 수정할 회원 계정 아이디
    * @param second 수정할 비밀번호
    * @return 비밀번호 성공 여부에 따른 결과값
    * @author 박휘림
    * */
   public int updatePassword(String acntId, String acntPwd) {
      
      Connection con = getConnection();
      /* 결과를 담을 result변수를 0으로 초기화 */
      int result = 0;
      /* result에 비밀번호 수정 성공 여부에 관한 결과값을 담는다. */
      result = adminDAO.updatePassword(con, acntId, acntPwd);
      /* 결과가 1이면 비밀번호 수정에 성공을 했기 때문에 commit을 하고 실패 시 rollback진행 */
      if(result > 0) {
         commit(con);
      } else {
         rollback(con);
      }
      close(con);
      
      return result;
    }

   /**
    * deleteAccount : 삭제할 계정 아이디를 받아 해당 아이디를 가지는 계정을 삭제하고 성공여부를 반환한다. 
    * @param first 삭제할 계정 아이디
    * @return 계정 삭제 성공 여부에 따른 결과값
    * @author 박휘림
    * */
   public int deleteAccount(String acntId) {
      
      Connection con = getConnection();
      /* 결과를 담을 result변수를 0으로 초기화 */
      int result = 0;
      /* result에 계정 삭제 성공 여부에 관한 결과값을 담는다. */
      result = adminDAO.deleteAccount(con, acntId);
      /* 결과가 1이면 비밀번호 수정에 성공을 했기 때문에 commit을 하고 실패 시 rollback진행 */
      if(result > 0) {
         commit(con);
      } else {
         rollback(con);
      }
      close(con);
      
      return result;
   }
}