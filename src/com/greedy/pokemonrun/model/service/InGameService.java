package com.greedy.pokemonrun.model.service;

import static com.greedy.pokemonrun.common.JDBCTemplate.commit;
import static com.greedy.pokemonrun.common.JDBCTemplate.getConnection;
import static com.greedy.pokemonrun.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.Map;

import com.greedy.pokemonrun.model.dao.InGameDAO;
import com.greedy.pokemonrun.model.dto.GameHistoryDTO;

public class InGameService {
	
	private InGameDAO inGameDAO = new InGameDAO();
	
	public int insertGameHistory(Map<String, Integer> map) {
		
		Connection con = getConnection();
		
		GameHistoryDTO gameHistory = new GameHistoryDTO(map.get("ballPoint") * 100, map.get("ballPoint"), 7, 1, map.get("acntNum"), 1);
		
		int result = 0;
		
//		gameHistory.setGetScore(map.get("ballPoint"));
//		gameHistory.setAcntNum(map.get("acntNum"));
//		gameHistory.setGetBall(map.get("ballPoint"));
		/* 포켓몬 넘버, 맵넘버, 사용한 코스튬 */
		
		
		result = inGameDAO.insertGameHistory(con, gameHistory);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		return result;
	}
	
	public int updateCurBall(int acntNum, int curBall) {
		
		Connection con = getConnection();
		
		int result = 0;
		
		result = inGameDAO.updateCurBall(con, acntNum, curBall);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		return result;
	}
	
	public int updateHighScore(int acntNum, int highScore) {
		
		Connection con = getConnection();
		
		int result = 0;
		
		result = inGameDAO.updateHighScore(con, acntNum, highScore);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		return result;
	}
	

}
