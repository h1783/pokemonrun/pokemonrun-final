package com.greedy.pokemonrun.model.service;

import java.sql.Connection;
import java.util.List;

import com.greedy.pokemonrun.model.dao.MainmenuDAO;
import com.greedy.pokemonrun.model.dto.AccountDTO;
import com.greedy.pokemonrun.model.dto.PokemonDTO;

import static com.greedy.pokemonrun.common.JDBCTemplate.getConnection;
import static com.greedy.pokemonrun.common.JDBCTemplate.close;
import static com.greedy.pokemonrun.common.JDBCTemplate.commit;
import static com.greedy.pokemonrun.common.JDBCTemplate.rollback;

/**
 * <pre>
* Class : MainmenuService
* Comment : 메인메뉴의 로직을 담당하는 서비스
* History
* 2021/12/ (박상범) 처음 작성
* 2021/12/ (박상범)
 * </pre>
 * @version 1
 * @author 박상범
 *
 */
public class MainmenuService {
	/* MainmenuDAO를 담을 변수를 필드에 선언 */
	private MainmenuDAO mainmenuDAO;

	/* 로그인이 되었을때 받을 회원번호를 담을 변수 선언 */
	private int acntNum;

	/* MainmenuService의 기본 생성자 */
	public MainmenuService() {
		/* 필드 변수에 MainmenuDAO를 생성 */
		this.mainmenuDAO = new MainmenuDAO();
	}

	/* MainmenuService를 생성함과 동시에 acntNum을 담을 수 있는 생성자 */
	public MainmenuService(int acntNum) {
		/* 필드 변수에 MainmenuDAO를 생성 */
		this.mainmenuDAO = new MainmenuDAO();
		this.acntNum = acntNum;
	}

	/**
	 * <pre>
	 * (현재 유저의 몬스터볼 갯수를 가져오는 메소드)
	 *  selectCurBall() : acntNum(회원번호)를
	 * 			     	  mainmenuDAO.selectcurBall()메소드에 넘겨
	 *                    회원의 현재 몬스터볼 갯수를 리턴한다.
	 * </pre>
	 * @param int acntNum : 회원 번호
	 * @return int curBall : 현재 몬스터볼의 갯수
	 * @author 박상범
	 */
	public int selectcurBall(int acntNum) {
		/* DB에 연결할 Connection생성 */
		Connection con = getConnection();

		int curBall = mainmenuDAO.selectCurBall(con, acntNum);

		close(con);
		
		return curBall;
	}

	/**
	 * <pre>
	 * (모든 포켓몬 번호를 List에 담아 불러오는 메소드) 
	 * selectAllPokemon() :	mainmenuDAO.selectcurBall()메소드를 사용해 
	 * 						리턴되는 List를 모든 포켓몬 번호를 담은 List에 담아 리턴한다.
	 * </pre>
	 * @return List<Integer> pokemonList : 모든 포켓몬 번호를 담은 List
	 * @author 박상범
	 */
	public List<Integer> selectAllPokemon() {

		Connection con = getConnection();

		List<Integer> pokemonList = mainmenuDAO.selectAllPokemon(con);

		close(con);

		return pokemonList;
	}

	/**
	 * <pre>
	 * (모든 회원의 포켓몬 뽑기 내역에서 유저의 포켓몬 뽑기 내역을 List에 담아 불러오는 메소드)
	 * selectAccountGetPokemon() : acntNum(회원번호)를
	 * 							   mainmenuDAO.selectAccountGetPokemon()메소드에 넘겨 리턴되는
	 * 							       회원이 보유한 포켓몬 번호들을 담는 getPokemonList에 담아서 리턴한다.
	 * </pre>
	 * @param int acntNum : 회원 번호
	 * @return List<Integer> getPokemonList : 회원이 보유한 포켓몬 번호들을 담은 List
	 * @author 박상범
	 */
	public List<Integer> selectAccountGetPokemon(int acntNum) {

		Connection con = getConnection();

		List<Integer> getPokemonList = mainmenuDAO.selectAccountGetPokemon(con, acntNum);

		close(con);

		return getPokemonList;
	}

	/**
	 * <pre>
	 * (현재 몬스터 볼의 갯수를 체크한 후, 무작위 포켓몬 번호를 리턴하는 메소드) 
	 * drawPokemon() : acntNum(회원번호)를
	 * 				   mainmenuDAO.selectCurBall()메소드에 넘겨 리턴되는 값을
	 * 				   curBall에 담은 후
	 * 				   curBall이 뽑기 가격(100)보다 이상이면
	 * 				   mainmenuDAO.minus100CurBall()를 사용해 유저의 현재 몬스터 볼에 - 100(뽑기 가격)을 하고
	 * 				   2 ~ 10(뽑을 수 있는 포켓몬 번호)까지의 무작위 수를 result에 담아서
	 * 				   result를 리턴한다.
	 * </pre>
	 * @param int acntNum : 회원 번호
	 * @return int result : 랜덤 포켓몬 번호를 담는 변수 
	 * @author 박상범
	 */
	public int drawPokemon(int acntNum) {

		Connection con = getConnection();

		int result = 0;

		int curBall = mainmenuDAO.selectCurBall(con, acntNum); // 현재 유저의 몬스터볼 수

		if (curBall >= 100) { 								   // 몬스터볼의 갯수가 충분하면
			mainmenuDAO.minus100CurBall(con, acntNum);		   // 현재몬스터볼 -100
			result = (int) (Math.random() * 9) + 2;			   // 랜덤 포켓몬 넘버 생성 2번부터 10까지
			System.out.println(result);				
		}

		if (result > 1) {									   // 2 ~ 10 이 나오면 성공
			System.out.println("랜덤 포켓못 번호 뽑기 성공");
		} else {											   // 0이하로 나오면 실패
			System.out.println("뽑기 실패");
		}

		close(con);

		return result;
	}
	
	/**
	 * <pre>
	 * (유저가 매개변수로 받은 drawPokemon(포켓몬 번호)이 소유중인지를 체크하는 메소드 ) 
	 * checkGetPokemon() : 뽑은  drawPokemon(포켓몬 번호)를 유저의 뽑기 내역과 비교해
	 * 					       그 결과 값을 리턴한다.
	 * </pre>
	 * @param int acntNum : 회원 번호
	 * @param int drawPokemon : 뽑은 포켓몬 번호
	 * @return int result : 뽑은 포켓몬 번호가 이미 구매 내역에 등록 되어있는 포켓몬인지 아닌지를 리턴함
	 * 						0 = 내역에 없는 새로운 포켓몬
	 * 						1 = 이미 뽑은 포켓몬
	 * @author 박상범
	 */
	public int checkGetPokemon(int acntNum, int drawPokemon) {

		Connection con = getConnection();

		int result = 0;

		List<Integer> getPokemonList = null; 								// 뽑기 내역을 담을 변수

		int pokemonDupl = 0; 												// 포켓몬 보유 여부

		getPokemonList = mainmenuDAO.selectAccountGetPokemon(con, acntNum); // 뽑기 내역에서 뽑은 포켓몬 리스트를 변수에 담는다.

		for (int i = 0; i < getPokemonList.size(); i++) {
			if (drawPokemon == getPokemonList.get(i)) { 					// 이미 보유한 포켓몬이지 조회
				pokemonDupl++; 												// 1이상 이면 이미 보유중
			}
		}

		if (pokemonDupl == 0) { 											
			result = 1;
			commit(con);
		} else {
			rollback(con);
		}

		close(con);

		return result; 														// 1이면 이미 보유한 포켓몬, 0이면 보유하지 않은 포켓몬
	}
	/**
	 * <pre>
	 * (포켓몬 뽑기 내역에 회원 번호와 포켓몬 번호를 등록하는 메소드) 
	 * insertGetPokemon() : acntNum(회원 번호)와 drawPokemon(포켓몬 번호)를 
	 * 				 		mainmenuDAO.insertGetPokemon()메소드에 넘겨
	 * 				       	회원이 보유한 포켓몬 번호들을 List에 담아서 리턴한다.
	 * </pre>
	 * @param int acntNum : 회원 번호
	 * @param int drawPokemon : 뽑은 포켓몬 번호
	 * @return int result : 0 = 포켓몬 뽑기 내역에 등록 실패
	 * 						1 = 포켓몬 뽑기 내역에 등록 성공
	 * @author 박상범
	 */
	public int insertGetPokemon(int acntNum, int drawPokemon) {

		Connection con = getConnection();

		int result = mainmenuDAO.insertGetPokemon(con, drawPokemon, acntNum);

		close(con);

		return result;
	}
	
	/**
	 * <pre>
	 * (유저의 현재 몬스터 볼 갯수에 -30을 하는 메소드) 
	 * plus30CurBall() : acntNum(회원 번호)를 
	 * 				 	 mainmenuDAO.plus30CurBall()메소드에 넘겨
	 * 				           결과 값을 담을 result변수에 담아 리턴한다.
	 * </pre>
	 * @param int acntNum : 회원 번호
	 * @return int result : 0 = 유저의 현재 몬스터 볼에 -30 실패
	 * 						1 = 유저의 현재 몬스터 볼에 -30 성공
	 * @author 박상범
	 */
	public int plus30CurBall(int acntNum) {

		Connection con = getConnection();

		int result = 0;

		result = mainmenuDAO.plus30CurBall(con, acntNum);

		close(con);

		return result;
	}
//-------------------------------------------------------코스튬------------------------------------------------
	
	/**
	 * <pre>
	 * (선택한 코스튬의 가격을 불러오는 메소드) 
	 * selectCostumePrice() : costumeNum(코스튬 번호)를 
	 * 				 		  mainmenuDAO.selectCostumePrice()메소드에 넘겨
	 * 				                      코스튬의 가격을 담을 costumePrice변수에 담아 리턴한다.
	 * </pre>
	 * @param int costumeNum : 코스튬 번호
	 * @return int costumePrice : 0 = 선택한 코스튬의 가격을 불러오지 못함
	 * 							 !0 = 선택한 코스튬의 가격
	 * @author 박상범
	 */
	public int selectCostumePrice(int costumeNum) { // 선택한 코스튬 가격 불러오기

		Connection con = getConnection();

		int costumePrice = mainmenuDAO.selectCostumePrice(con, costumeNum);

		close(con);

		return costumePrice;
	}

	/**
	 * <pre>
	 * (유저가 매개변수로 받은 costumeNum(코스튬 번호)을 소유중인지를 체크하는 메소드 ) 
	 * selectGetCostume() : acntNum(회원 번호)를 
	 * 				 		mainmenuDAO.selectGetCostume()에 넘겨서 나온 결과 값을
	 * 						costumeList에 담아서 costumeNum(코스튬 번호)와 차례대로 비교를 한 다음
	 * 						if조건에 만족하면 alreadyBuy(구매 이력이 있는지 확인)을 +1 해준다.
	 * 						그 결과는 result에 담아 리턴한다.
	 * </pre>
	 * @param int acntNum : 회원 번호
	 * @param int costumeNum : 코스튬 번호
	 * @return int result : 0 = 선택한 코스튬 구매 가능(미보유)
	 * 						1 = 선택한 코스튬 구매 불가능(이미 보유중)
	 * @author 박상범
	 */
	public int selectGetCostume(int acntNum, int costumeNum) {

		Connection con = getConnection();

		int result = 0;

		List<Integer> costumeList = mainmenuDAO.selectGetCostume(con, acntNum);

		int alreadyBuy = 0; 													// 1이면 구매한적이 있다. 0이면 없다.

		for (int i = 0; i < costumeList.size(); i++) {
			if (costumeNum == costumeList.get(i)) { 							// 구매한적이 있는지 확인
				alreadyBuy++;													// 1이면 구매 내역 있음  0이면 미보유
			}
		}

		if (alreadyBuy > 0) {
			result = 1; 														// 1이면 구매 내역 있음 0이면 미보유
		}

		close(con);

		return result;
	}
	
	/**
	 * <pre>
	 * (유저가 코스튬을 구매한 후 구매 내역에 추가함) 
	 * buyCostume() : acntNum(회원 번호)를 
	 * 				  mainmenuDAO.selectGetCostume()에 넘겨서 나온 결과 값을
	 * 				  costumeList에 담아서 costumeNum(코스튬 번호)와 차례대로 비교를 한 다음
	 * 				  if조건에 만족하면 alreadyBuy(구매 이력이 있는지 확인)을 +1 해준다.
	 * 				     그 결과는 result에 담아 리턴한다.
	 * </pre>
	 * @param int acntNum : 회원 번호
	 * @param int costumeNum : 코스튬 번호
	 * @return int result : 0 = 선택한 코스튬 구매 가능(미보유)
	 * 						1 = 선택한 코스튬 구매 불가능(이미 보유중)
	 * @author 박상범
	 */
	public int buyCostume(int acntNum, int costumeNum) {

		Connection con = getConnection();

		int result = 0;

		int curBall = mainmenuDAO.selectCurBall(con, acntNum);						// 현재 몬스터 볼 갯수 불러오기

		int costumePrice = mainmenuDAO.selectCostumePrice(con, costumeNum); 		// 선택한 코스튬 가격 불러오기

		List<Integer> costumeList = mainmenuDAO.selectGetCostume(con, costumeNum);  // 코스튬 구매 내역 불러와서 List에 담기

		int alreadyBuy = 0; 														// 1이면 구매한적이 있다. 0이면 없다.

		if (curBall >= costumePrice) { 												// 구매가능여부
			
			for (int i = 0; i < costumeList.size(); i++) {
				if (costumeNum == costumeList.get(i)) { 							// 구매한적이 있는지 확인
					alreadyBuy++;
				}
			}
			
			if (alreadyBuy == 0) {
				mainmenuDAO.minusPriceCurBall(con, costumePrice, acntNum);			// 가격만큼 몬스터볼 소비
				mainmenuDAO.insertBuyCostume(con, costumeNum, acntNum); 			// 구매 내역에 추가
				result = 1;
			}
		}
		
		if (result > 0) { 															// 1이면 구매 성공
			commit(con);
		} else {
			rollback(con);
		}

		close(con);

		return result;
	}

}
