package com.greedy.pokemonrun.model.service;

import static com.greedy.pokemonrun.common.JDBCTemplate.close;
import static com.greedy.pokemonrun.common.JDBCTemplate.commit;
import static com.greedy.pokemonrun.common.JDBCTemplate.getConnection;
import static com.greedy.pokemonrun.common.JDBCTemplate.rollback;

import java.sql.Connection;

import com.greedy.pokemonrun.model.dao.MainmenuDAO3;

/**
 * <pre>
 * Class : MainmenuService
 * Comment : 컨트롤러에서 받아온 값들을 DAO에 보내 주고 결과 값들을 받는 service
 * 			 Connection 연결 해줌.
 * History
 * 2021/12/12 (구훈모) 처음 작성
 * 2021/12/16 (구훈모) 작성 완료
 * </pre>
 * @author 구훈모
 * */
public class MainmenuService3 {
	
	/* DAO를 필드에서 호출해 공유할 수 있도록 함. */
	MainmenuDAO3 mainmenuDAO3 = new MainmenuDAO3();

	/**
	 * <pre>
	 * Comment : 계정삭제에 관련하는 Service
	 * </pre>
	 * @author 구훈모
	 * @param acntNum : 로그인시 받아온 회원 번호
	 * @param message : 계정탈퇴시 사용자가 기입하는 message
	 * @return deleteResult : 계정탈퇴 성공여부를 정수로 리턴
	 * */
	public int deleteAcct(int acntNum, String message) {
		
		int result = 0;

		Connection con = getConnection();
		
		/* 화면에 입력한 단어와 일치 한다면 계정 삭제 진행 */
		if(!"계정탈퇴".equals(message)) {
			result = 0;
		} else {
			result = mainmenuDAO3.deleteAcct(con, acntNum);
		}
		
		/* 계정 삭제에 성공 했다면 양의 정수를 받아오기 떄문에 그떄는 커밋, 아닐경우 롤백. */
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	/**
	 * <pre>
	 * Comment : 설정에서 비밀번호 변경의 성공여부를 가져오는 메소드
	 * </pre>
	 * @author 구훈모
	 * @param acntNum : 로그인시 받아온 회원 번호
	 * @param acntPwd : 사용자가 입력하는 현재 비밀번호(회원번호를 이용해 현재 비밀번호와 비교 )
	 * @param newPwd : 사용자가 입력하는 새로운 비밀번호
	 * @param checkNewPwd : 사용자가 입력한 새로운 비밀번호와 일치하는지 비교하기 위한 비밀번호
	 * @return changeResult : 비밀변호 변경성공 여부를 정수로 리턴. 
	 * */
	public int changPwd(int acntNum, String acntPwd, String newPwd, String checkNewPwd) {
		
		int result = 0;
		String checkPwd = "";
		int checkNameByPwdResult = 0;
		int checkNewPwdResult = 0;
		int updateResult = 0;

		Connection con = getConnection();
		
		/* 회원번호의 비밀번호와 acntPwd가 일치하는지 확인 */
		checkPwd = mainmenuDAO3.checkNameByPwd(con, acntNum);
		
		if(checkPwd.equals(acntPwd)) {
			checkNameByPwdResult = 1;
		} else {
			checkNameByPwdResult = 0;
		}
		
		/* 화면에서 입력한 newPwd와 checkNewPwd 가 같은지 비교 */
		if(newPwd.equals(checkNewPwd)) {
			checkNewPwdResult = 1;
		} else {
			checkNewPwdResult = 0;
		}
		
		/* 비밀번호 변경에 성공한것인지 판별 */
		updateResult = mainmenuDAO3.updatePwd(con, newPwd, acntNum);
		
		/* 어떠한 값이라도 0이 나오면 실패한 것이기 때문에 곱하기로 한번에 통합함. */
		result = checkNewPwdResult * checkNameByPwdResult * updateResult;
		
		/* 비밀번호 일치여부 확인과 바꾸려는 비밀번호가 일치하게 입력됐다면 결과값은 양의 정수가 나올것이고
		 * 아니라면 0이 나올 것이기 때문에, 양의 정수가 나온다면 커밋, 아니라면 롤백을 해준다. 
		 * */
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		} 
		
		close(con);
		
		return result;
	}

}
