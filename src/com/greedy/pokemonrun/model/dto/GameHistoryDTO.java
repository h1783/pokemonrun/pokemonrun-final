package com.greedy.pokemonrun.model.dto;

import java.sql.Date;

public class GameHistoryDTO {

	private int playDateNum;
	private java.sql.Date playDate;
	private int getScore;
	private int getBall;
	private int usePokemonNum;
	private int mapNum;
	private int acntNum;
	private int useCostume;
	
	public GameHistoryDTO() {}

	public GameHistoryDTO(int playDateNum, Date playDate, int getScore, int getBall, int usePokemonNum, int mapNum,
			int acntNum, int useCostume) {
		super();
		this.playDateNum = playDateNum;
		this.playDate = playDate;
		this.getScore = getScore;
		this.getBall = getBall;
		this.usePokemonNum = usePokemonNum;
		this.mapNum = mapNum;
		this.acntNum = acntNum;
		this.useCostume = useCostume;
	}
	
	public GameHistoryDTO(int getScore, int getBall, int usePokemonNum, int mapNum,
			int acntNum, int useCostume) {
		super();
		this.getScore = getScore;
		this.getBall = getBall;
		this.usePokemonNum = usePokemonNum;
		this.mapNum = mapNum;
		this.acntNum = acntNum;
		this.useCostume = useCostume;
	}

	public int getPlayDateNum() {
		return playDateNum;
	}

	public void setPlayDateNum(int playDateNum) {
		this.playDateNum = playDateNum;
	}

	public java.sql.Date getPlayDate() {
		return playDate;
	}

	public void setPlayDate(java.sql.Date playDate) {
		this.playDate = playDate;
	}

	public int getGetScore() {
		return getScore;
	}

	public void setGetScore(int getScore) {
		this.getScore = getScore;
	}

	public int getGetBall() {
		return getBall;
	}

	public void setGetBall(int getBall) {
		this.getBall = getBall;
	}

	public int getUsePokemonNum() {
		return usePokemonNum;
	}

	public void setUsePokemonNum(int usePokemonNum) {
		this.usePokemonNum = usePokemonNum;
	}

	public int getMapNum() {
		return mapNum;
	}

	public void setMapNum(int mapNum) {
		this.mapNum = mapNum;
	}

	public int getAcntNum() {
		return acntNum;
	}

	public void setAcntNum(int acntNum) {
		this.acntNum = acntNum;
	}

	public int getUseCostume() {
		return useCostume;
	}

	public void setUseCostume(int useCostume) {
		this.useCostume = useCostume;
	}

	@Override
	public String toString() {
		return "GameHistoryDTO [playDateNum=" + playDateNum + ", playDate=" + playDate + ", getScore=" + getScore
				+ ", getBall=" + getBall + ", usePokemonNum=" + usePokemonNum + ", mapNum=" + mapNum + ", acntNum="
				+ acntNum + ", useCostume=" + useCostume + "]";
	}
	
	
}
