package com.greedy.pokemonrun.model.dto;

public class PokemonDTO {

	private int pokemonNum;
	private String pokemonName;
	private int increaseBallRate;
	private int pokemonPrice;
	
	public PokemonDTO() {}

	public PokemonDTO(int pokemonNum, String pokemonName, int increaseBallRate, int pokemonPrice) {
		super();
		this.pokemonNum = pokemonNum;
		this.pokemonName = pokemonName;
		this.increaseBallRate = increaseBallRate;
		this.pokemonPrice = pokemonPrice;
	}

	public int getPokemonNum() {
		return pokemonNum;
	}

	public void setPokemonNum(int pokemonNum) {
		this.pokemonNum = pokemonNum;
	}

	public String getPokemonName() {
		return pokemonName;
	}

	public void setPokemonName(String pokemonName) {
		this.pokemonName = pokemonName;
	}

	public int getIncreaseBallRate() {
		return increaseBallRate;
	}

	public void setIncreaseBallRate(int increaseBallRate) {
		this.increaseBallRate = increaseBallRate;
	}

	public int getPokemonPrice() {
		return pokemonPrice;
	}

	public void setPokemonPrice(int pokemonPrice) {
		this.pokemonPrice = pokemonPrice;
	}

	@Override
	public String toString() {
		return "PokemonDTO [pokemonNum=" + pokemonNum + ", pokemonName=" + pokemonName + ", increaseBallRate="
				+ increaseBallRate + ", pokemonPrice=" + pokemonPrice + "]";
	}
	
	
}
