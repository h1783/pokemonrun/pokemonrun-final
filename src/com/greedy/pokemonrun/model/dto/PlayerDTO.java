package com.greedy.pokemonrun.model.dto;

public class PlayerDTO {

	private int acntNum;
	private int curBall;
	private int highScore;
	private int curPokemonNum;
	private int costumeNum;
	
	public PlayerDTO() {}

	public PlayerDTO(int acntNum, int curBall, int highScore, int curPokemonNum, int costumeNum) {
		super();
		this.acntNum = acntNum;
		this.curBall = curBall;
		this.highScore = highScore;
		this.curPokemonNum = curPokemonNum;
		this.costumeNum = costumeNum;
	}

	public int getAcntNum() {
		return acntNum;
	}

	public int getCurBall() {
		return curBall;
	}

	public int getHighScore() {
		return highScore;
	}

	public int getCurPokemonNum() {
		return curPokemonNum;
	}

	public int getCostumeNum() {
		return costumeNum;
	}

	public void setAcntNum(int acntNum) {
		this.acntNum = acntNum;
	}

	public void setCurBall(int curBall) {
		this.curBall = curBall;
	}

	public void setHighScore(int highScore) {
		this.highScore = highScore;
	}

	public void setCurPokemonNum(int curPokemonNum) {
		this.curPokemonNum = curPokemonNum;
	}

	public void setCostumeNum(int costumeNum) {
		this.costumeNum = costumeNum;
	}

	@Override
	public String toString() {
		return "PlayerDTO [acntNum=" + acntNum + ", curBall=" + curBall + ", highScore=" + highScore
				+ ", curPokemonNum=" + curPokemonNum + ", costumeNum=" + costumeNum + "]";
	}
	
	
}
