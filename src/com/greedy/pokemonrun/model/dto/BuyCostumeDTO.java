package com.greedy.pokemonrun.model.dto;

import java.sql.Date;

public class BuyCostumeDTO {
	
	private int costumeNumber;
	private int acntNumber;
	private Date buyDate;
	
	public BuyCostumeDTO() {}

	public BuyCostumeDTO(int costumeNumber, int acntNumber, Date buyDate) {
		super();
		this.costumeNumber = costumeNumber;
		this.acntNumber = acntNumber;
		this.buyDate = buyDate;
	}

	public int getCostumeNumber() {
		return costumeNumber;
	}

	public int getAcntNumber() {
		return acntNumber;
	}

	public Date getBuyDate() {
		return buyDate;
	}

	public void setCostumeNumber(int costumeNumber) {
		this.costumeNumber = costumeNumber;
	}

	public void setAcntNumber(int acntNumber) {
		this.acntNumber = acntNumber;
	}

	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}

	@Override
	public String toString() {
		return "BuyCostumeDTO [costumeNumber=" + costumeNumber + ", acntNumber=" + acntNumber + ", buyDate=" + buyDate
				+ "]";
	}
	
	

}
