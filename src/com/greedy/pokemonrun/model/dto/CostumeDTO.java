package com.greedy.pokemonrun.model.dto;

public class CostumeDTO {

	private int costumeNum;
	private String costumeName;
	private int increaseLife;
	private int increaseScoreRate;
	private int costumePrice;
	
	public CostumeDTO() {}

	public CostumeDTO(int costumeNum, String costumeName, int increaseLife, int increaseScoreRate, int costumePrice) {
		super();
		this.costumeNum = costumeNum;
		this.costumeName = costumeName;
		this.increaseLife = increaseLife;
		this.increaseScoreRate = increaseScoreRate;
		this.costumePrice = costumePrice;
	}

	public int getCostumeNum() {
		return costumeNum;
	}

	public void setCostumeNum(int costumeNum) {
		this.costumeNum = costumeNum;
	}

	public String getCostumeName() {
		return costumeName;
	}

	public void setCostumeName(String costumeName) {
		this.costumeName = costumeName;
	}

	public int getIncreaseLife() {
		return increaseLife;
	}

	public void setIncreaseLife(int increaseLife) {
		this.increaseLife = increaseLife;
	}

	public int getIncreaseScoreRate() {
		return increaseScoreRate;
	}

	public void setIncreaseScoreRate(int increaseScoreRate) {
		this.increaseScoreRate = increaseScoreRate;
	}

	public int getCostumePrice() {
		return costumePrice;
	}

	public void setCostumePrice(int costumePrice) {
		this.costumePrice = costumePrice;
	}

	@Override
	public String toString() {
		return "CostumeDTO [costumeNum=" + costumeNum + ", costumeName=" + costumeName + ", increaseLife="
				+ increaseLife + ", increaseScoreRate=" + increaseScoreRate + ", costumePrice=" + costumePrice + "]";
	}
	
	
}
