package com.greedy.pokemonrun.model.dto;

import java.sql.Date;

public class GetPokemonDTO {

	private int pokemonNum;
	private int acntNum;
	private java.sql.Date getDate;
	
	
	public GetPokemonDTO() {
	}


	public GetPokemonDTO(int pokemonNum, int acntNum, Date getDate) {
		super();
		this.pokemonNum = pokemonNum;
		this.acntNum = acntNum;
		this.getDate = getDate;
	}


	public int getPokemonNum() {
		return pokemonNum;
	}


	public void setPokemonNum(int pokemonNum) {
		this.pokemonNum = pokemonNum;
	}


	public int getAcntNum() {
		return acntNum;
	}


	public void setAcntNum(int acntNum) {
		this.acntNum = acntNum;
	}


	public java.sql.Date getGetDate() {
		return getDate;
	}


	public void setGetDate(java.sql.Date getDate) {
		this.getDate = getDate;
	}


	@Override
	public String toString() {
		return "GetPokemonDTO [pokemonNum=" + pokemonNum + ", acntNum=" + acntNum + ", getDate=" + getDate + "]";
	}
	
	
}
