package com.greedy.pokemonrun.model.dto;

import java.sql.Date;

public class AccountDTO {
	private int acntNum;
	private String acntKind;
	private String acntId;
	private String acntPwd;
	private String delYn;
	private Date delDate;
	private String memName;
	private String memEmail;
	private Date entDate;
	
	public AccountDTO(){}

	public AccountDTO(int acntNum, String acntKind, String acntId, String acntPwd, String delYn, Date delDate,
			String memName, String memEmail, Date entDate) {
		super();
		this.acntNum = acntNum;
		this.acntKind = acntKind;
		this.acntId = acntId;
		this.acntPwd = acntPwd;
		this.delYn = delYn;
		this.delDate = delDate;
		this.memName = memName;
		this.memEmail = memEmail;
		this.entDate = entDate;
	}

	public int getAcntNum() {
		return acntNum;
	}

	public void setAcntNum(int acntNum) {
		this.acntNum = acntNum;
	}

	public String getAcntKind() {
		return acntKind;
	}

	public void setAcntKind(String acntKind) {
		this.acntKind = acntKind;
	}

	public String getAcntId() {
		return acntId;
	}

	public void setAcntId(String acntId) {
		this.acntId = acntId;
	}

	public String getAcntPwd() {
		return acntPwd;
	}

	public void setAcntPwd(String acntPwd) {
		this.acntPwd = acntPwd;
	}

	public String getDelYn() {
		return delYn;
	}

	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	public Date getDelDate() {
		return delDate;
	}

	public void setDelDate(Date delDate) {
		this.delDate = delDate;
	}

	public String getMemName() {
		return memName;
	}

	public void setMemName(String memName) {
		this.memName = memName;
	}

	public String getMemEmail() {
		return memEmail;
	}

	public void setMemEmail(String memEmail) {
		this.memEmail = memEmail;
	}

	public Date getEntDate() {
		return entDate;
	}

	public void setEntDate(Date entDate) {
		this.entDate = entDate;
	}

	@Override
	public String toString() {
		return "acntNum = " + acntNum + ", acntId = " + acntId + ", acntPwd = " + acntPwd
				+ ", delYn = " + delYn + ", memName = " + memName + ", memEmail = " + memEmail;
	}
	
	
	
	
	
	
}

