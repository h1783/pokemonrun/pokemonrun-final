package com.greedy.pokemonrun.controller;

import java.util.List;
import java.util.Map;

import javax.swing.JFrame;

import com.greedy.pokemonrun.model.dto.PokemonDTO;
import com.greedy.pokemonrun.model.service.MainmenuService;

/**
* <pre>
* Class : MainmenuController
* Comment : 메인메뉴의 기능을 담당하는 컨트롤러
* History
* 2021/12/ (박상범) 처음 작성
* 2021/12/ (박상범) 
* </pre>
* @version 1
* @author 박상범
*
*/
public class MainmenuController {
	/** MainmenuService를 담을 변수를 필드에 선언 */
	private MainmenuService mainmenuService;
	
	/** 회원 번호를 담을 변수를 필드에 선언 */
	private int acntNum;
	
	/** MainmenuController의 기본 생성자 */
	public MainmenuController() {
		/** 메인메뉴 서비스 호출 */
		mainmenuService = new MainmenuService();
	}
	
	/** MainmenuController를 생성함과 동시에 acntNum를 담을 수 있는 생성자*/
	public MainmenuController(int acntNum) {
		/** 메인메뉴 서비스 호출 */
		mainmenuService = new MainmenuService(acntNum);
		
		
		this.acntNum = acntNum;
	}

	/**
	* (현재 유저의 몬스터볼 갯수를 가져오는 메소드)
	* selectCurBall() : acntNum(회원번호)를  
	* 					mainmenuService.selectcurBall()메소드에 넘겨
	* 					회원의 현재 몬스터볼 갯수를 리턴한다.
	* @param int acntNum : 회원번호
	* @return int curBall : 현재 몬스터볼의 갯수
	* @author 박상범
	*/
	public int selectCurBall(int acntNum) {
		
		int curBall = 0;
		
		curBall = mainmenuService.selectcurBall(acntNum);
		
		return curBall;
	}
	
	/**
	* (선택한 포켓몬을 보유중인지 체크하는 메소드)
	* checkPokemon() : acntNum(회원번호)와 drawPokemon(뽑은 포켓몬의 번호)을
	* 				   mainmenuService.checkGetPokemon()에 넘겨
	* 				       메소드의 결과값을 리턴한다.
	* @param int acntNum : 회원 번호
	* @param int drawPokemon : 포켓몬 번호
	* @return int result : mainmenuService.checkGetPokemon()메소드의 실행 결과
	* 					   0 = 내역에 없는 포켓몬
	* 					   1 = 이미 보유한 포켓몬
	* @author 박상범
	*/
	public int checkPokemon(int acntNum, int pokemonNum) {  

		int result = 0;
		
		result = mainmenuService.checkGetPokemon(acntNum, pokemonNum);
		
		return result;
	}
	
	/**
	* (랜덤 포켓몬을 뽑는 메소드) 
	* randomDrawPokemon() : acntNum(회원번호)를
	* 						mainmenuService.drawPokemon()에 넘겨
	* 						2 ~ 10까지의 랜덤한 수(포켓몬 번호)를 리턴한다.
	* @param int acntNum : 회원 번호
	* @return int random : 2 ~ 10까지의 랜덤한 수(포켓몬 번호)
	* @author 박상범
	*/
	public int randomDrawPokemon(int acntNum) {

		int random = 0;
		
		random = mainmenuService.drawPokemon(acntNum);
		
		return random;
	}

	/**
	* (포켓몬 뽑기 내역을 추가하는 메소드)
	* insertGetPokemon() : acntNum(회원번호)와 pokemonNum(포켓몬 번호)를
	* 					   mainmenuService.insertGetPokemon()에 넘겨
	* 					      메소드의 결과값을 리턴한다.
	* @param int acntNum : 회원 번호
	* @param int pokemonNum : 포켓몬 번호
	* @return int result : mainmenuService.insertGetPokemon()의 실행 결과
	* 					   0 = 구매 내역에 추가 실패
	* 					   1 = 구매 내역에 추가 성공
	* @author 박상범
	*/
	public int insertGetPokemon(int acntNum, int pokemonNum) {
		
		int result = 0;
		
		result = mainmenuService.insertGetPokemon(acntNum, pokemonNum);
		
		return result;
	}
	
	/**
	* (현재 유저의 몬스터볼 개수를 -30하는 메소드) 
	* plus30CurBall() : acntNum(회원 번호)를
	* 					mainmenuService.plus30CurBall()메소드에 넘겨
	* 					메소드의 결과값을 리턴한다.
	* @param int acntNum : 회원 번호
	* @return int result : mainmenuService.plus30CurBall()의 실행 결과
	* 					   0 = 현재 몬스터볼 갯수 + 30 실패
	* 					   1 = 현재 몬스터볼 갯수 + 30 성공
	* @author 박상범
	*/
	public int plus30CurBall(int acntNum) {
		
		int result = 0;
		
		result = mainmenuService.plus30CurBall(acntNum);
		
		return result;
	}
	
	
	/**
	* (선택한 코스튬 가격 불러오기)
	* selectCostumePrice() : costumeNum(코스튬 번호)를
	* 						mainmenuService.selectCostumePrice()메소드에 넘겨
	* 						코스튬 번호를 가진 코스튬의 가격을 리턴한다.
	* @param int costumeNum : 코스튬 번호
	* @return int costumePrice : 코스튬 번호를 가진 코스튬의 가격
	* @author 박상범
	*/
	public int selectCostumePrice(int costumeNum) {
		
		int costumePrice;
		
		costumePrice = mainmenuService.selectCostumePrice(costumeNum);
		
		return costumePrice;
	}
	
	/**
	* (선택한 코스튬이 보유중인지 체크하는 메소드) 
	* selectGetCostume() : acntNum(회원 번호)를
	* 					   mainmenuService.selectGetCostume()메소드에 넘겨
	* 					       메소드의 결과값을 리턴한다.
	* @param int acntNum : 회원 번호
	* @param int costumeNum : 코스 번호
	* @return int result : mainmenuService.selectGetCostume()의 실행 결과
	* 					   0 = 구매내역 없음
	* 					   1 = 구매내역 있음
	* @author 박상범
	*/
	public int selectGetCostume(int acntNum, int costumeNum) {
		
		int result = 0;
		
		result = mainmenuService.selectGetCostume(acntNum, costumeNum);
		
		return result;
	}
	
	/**
	 * (선택한 코스튬을 코스튬 구매내역에 추가하는 메소드)
	 * buyCostume() : acntNum(회원 번호)와 costumeNum(코스튬 번호)를
	 * 				  mainmenuService.buyCostume()메소드에 넘겨
	 * 				    메소드의 결과값을 리턴한다.
	 * @param int acntNum : 회원 번호
	 * @param int costumeNum : 회원 번호
	 * @return int result : mainmenuService.buyCostume()의 실행 결과
	 * 					    0 = 코스튬 구매내역 추가 실패
	 * 						1 = 코스튬 구매내역 추가 완료
	 * @author 박상범
	 */
	public int buyCostume(int acntNum, int costumeNum) {
		
		int result = 0;
		
		result = mainmenuService.buyCostume(acntNum, costumeNum);
		
		return result;
	}
}
