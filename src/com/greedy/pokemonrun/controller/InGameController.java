package com.greedy.pokemonrun.controller;

import java.util.Map;

import com.greedy.pokemonrun.model.dto.GameHistoryDTO;
import com.greedy.pokemonrun.model.service.InGameService;
//import com.greedy.pokemonrun.view.IngameView;

public class InGameController {
	
	private InGameService ingameService = new InGameService();
//	private IngameView ingameView = new IngameView();

	
	public void insertGameHistory(Map<String, Integer> map) {
		
		
		int insertResult = ingameService.insertGameHistory(map);
		
		if(insertResult > 0) {
			System.out.println("게임내역등록");
		} else {
			System.out.println("실패");
		}
	}
	
	public void updateCurBall(int acntNum, int curBall ) {
		
		int updateResult = ingameService.updateCurBall(acntNum, curBall);
		
		if(updateResult > 0) {
			System.out.println("몬스터볼 증가");
		} else {
			System.out.println("몬스터볼 증가 x");
		}
	}
	
	public void updateHighScore(int acntNum, int highScore) {
		
		int updateResult = ingameService.updateHighScore(acntNum, highScore);
		
		if(updateResult > 0) {
			System.out.println("최고 점수 등록");
		} else {
			System.out.println("신기록 갱신 실패");
		}
		
	}

}
                                                                       