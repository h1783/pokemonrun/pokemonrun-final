package com.greedy.pokemonrun.controller;

import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.greedy.pokemonrun.model.service.LoginService;
import com.greedy.pokemonrun.swingview.AdminManagementPanel;
import com.greedy.pokemonrun.swingview.DisplayOpenningPanel;
import com.greedy.pokemonrun.swingview.IdPwdSearchPanel;
import com.greedy.pokemonrun.swingview.LoginViewPanel;
import com.greedy.pokemonrun.swingview.MainmenuPanel;
import com.greedy.pokemonrun.swingview.PanelSwitch;
import com.greedy.pokemonrun.swingview.SignUpPanel;

/** 
 * <pre>
 * Class : LoginController
 * Comment : 로그인 페이지의 로그인, 회원가입, ID/PWD찾기를 담당하는 컨트롤러 
 * History
 * 2021/12/13 (홍성원) 작성
 * </pre>
 * @version 1
 * @author 홍성원
 * */
public class LoginController {
   private LoginService loginService;
   
   /** 
    * findMemberId : 로그인페이지에서 아이디 찾기기능을 담당하는 controller
    * @param memInf : 회원의 정보를 저장하는 Map. "memName"의 키는 회원의 이름을 저장하고, "memEmail"의 키는 회원의 이메일을 저장하고있다.
    * @return memId : 회원의 아이디를 반환한다.
    * 
    * @author 홍성원
    * */
   public void findMemberId(JFrame mf, JPanel thisPanel, Map<String, String> memInfo) {

      /* 로그인 기능을 담당하는 LoginService 클래스에 있는 아이디 찾기 관련 메소드를 호춣하기위해 인스턴스를 생성하는 부분 */
      loginService = new LoginService();
      String memName = memInfo.get("memName");
      String memEmail = memInfo.get("memEmail");
      
      /* 아이디를 찾기위해 회원의 이름과 이메일주소를 매개변수로 넣어주는 부분 */
      String memId = loginService.findAccountId(memName, memEmail);
      
      /* 만약 회원정보가 존재해서 회원의 아이디의 길이가 0 이상이라면 메인프레임의 패널을 ID찾기 성공화면패널로, 0이라면 아이디찾기 실패화면을 출력하는 패널로 교체한다. */
      if(memId.length() > 0) {
			PanelSwitch.ChangePanel(mf, thisPanel, new IdPwdSearchPanel(mf).idSearchSuccess(mf, memId));
		} else {
			PanelSwitch.ChangePanel(mf, thisPanel, new IdPwdSearchPanel(mf).idSearchFailed(mf));
		}
      
      return ;   
   }

   /**
    * findMemberPwd : 로그인페이지에서 비밀번호 찾기 기능을 담당하는 controller
    * @param memInfo : 회원의 정보를 저장하는 Map. "memName", "memEmail", "acntId"가 각각 호원의 이름, 이메일, 아이디의 value를 갖고있다.
    * @return result : 비밀번호찾기의 결과를 반환한다. 변경된 비밀번호가 이메일로 전송이 됐다면 1, 아니라면 0을 반환한다. 
    * 
    * @author 홍성원
    * */
   public void findMemberPwd(JFrame mf, JPanel thisPanel, Map<String, String> memInfo) {
      /* 비밀번호찾기 트랜잭션 수행할 서비스 생성 */
      loginService = new LoginService();
      String memName = memInfo.get("memName");
      String acntId = memInfo.get("acntId");
      String memEmail = memInfo.get("memEmail");

      String result = loginService.findMemberPwd(memName, acntId, memEmail);
      
      /* 이름, 아이디 이메일을 가지고있는 해당 회원의 비밀번호의 길이가 0보다 크면 해당회원의 비밀번호를 성공적으로 출력해주는 뷰로 보낸다. */
      /* 만약 전달받은 비밀번호의 길이가 0이라면 해당 값을 갖는 회원이 없는 뜻이므로 비밀번호찾기 실패를 출력하는 패널로 전환한다. */
      if(result.length() > 0) {
          PanelSwitch.ChangePanel(mf, thisPanel, new IdPwdSearchPanel(mf).pwdSearchSuccess(mf, result));  
       } else {
          PanelSwitch.ChangePanel(mf, thisPanel, new IdPwdSearchPanel(mf).pwdSearchFailed(mf));
       }
      
      return;
   }

   

   /* 회원가입시 입력받은 아이디 이름 이메일의 중복여부를 확인한다
    * 중복이면 입력정보확인 뷰로 보내고, 중복이 아니면 비밀번호 입력 뷰로 보낸다 */
   
   /* 이메일 중복 아닐시 행 삽입이 되니까 중복되는 사람 없다는것,*/
   /**
    * registAccountCheckInput : 회원가입시, 입력한 정보가 기존의 회원과 중복되는지 판별해주는 기능중 controller
    * @param regist : 비회원이 입력한 아이디, 이름, 이메일주소를 각각 "acntId", "memName", "memEmail"의 key값으로 저장하고있는 Map
    * @return result : 
    * 
    * @author 홍성원
    * */
   public void registAccountCheckInput(JFrame mf, JPanel thisPanel, Map<String, String> regist) {
	   loginService = new LoginService();
	   int result = loginService.registAccountCheckInput(regist);
      /* 만약 result가 0보다 크면 회원가입 순서중 비밀번호를 입력하는 부분을 출력하는 패널로 이동한다.. */
		/* result가 0보다 작으면 회원가입 실패창으로 이동한다 */
      if(result > 0) {
    	  PanelSwitch.ChangePanel(mf, thisPanel, new SignUpPanel(mf).signUpPwd(mf, regist));
      }else {
    	  PanelSwitch.ChangePanel(mf, thisPanel, new SignUpPanel(mf).signUpFail(mf));
      }
   }
   
   /**
    * registAccountCheckInput2????? : 회원가입시, 입력한 정보가 기존의 회원과 중복되는지 판별해주는 기능중 controller
    * @param regist : 비회원이 입력한 아이디, 이름, 이메일주소를 각각 "acntId", "memName", "memEmail"의 key값으로 저장하고있는 Map
    * @return result : 
    * 
    * @author 홍성원
    * */
    public void registAccountCheckInput2(JFrame mf, JPanel thisPanel, Map<String, String> regist) {
        loginService = new LoginService();
        int acntNum = loginService.registAccountCheckInput2(regist);
        if(acntNum> 0) {
			PanelSwitch.ChangePanel(mf, thisPanel, new MainmenuPanel((JFrame)mf, acntNum));
		} else {
			PanelSwitch.ChangePanel(mf, thisPanel, new SignUpPanel(mf).signUpPwd(mf, regist));
		}
        
        return;
    }
    
    /**
     * login : 입력받은 아이디와 비밀번호로 회원인지를 확인하여, 맞는 출력부분으로 보내주는 메소드.  
     * @param mf : 메인프레임의 주소를 저장하는 변수
     * @param thisPanel : 컨트롤러의 결과에 따라 다른 패널로 이동할때, 지워줘야하는 현재 패널의 주소값.
     * @param regist : 비회원이 입력한 아이디, 이름, 이메일주소를 각각 "acntId", "memName", "memEmail"의 key값으로 저장하고있는 Map
     * @author 홍성원
     * */
   public void login(JFrame mf, JPanel thisPanel, Map<String, String> loginInfo) {
      loginService = new LoginService();
      
      /* Map 형태로 전달받은 아이디와 비밀번호를, Map에서 꺼내, id문자열과, pwd문자열에 저장하는 과정 */
      String acntId = loginInfo.get("acntId");
      String acntPwd = loginInfo.get("acntPwd");
      /* 저장한 id와 pwd를 로그인 기능을 수행하는 서비스로 보내준 후  , 해당 결과값인 회원번호를 리턴받는다. */
      int acntNum = loginService.login(acntId,acntPwd);			
      
      /* 로그인담당 서비스에서 전달받은 결과값으로, 해당결과에 맞는 View가 출력되게 해당 뷰를 호출한다. */
      /* 회원번호가 999면 관리자 화면으로 전환되고, 0보다 크면 일반회원 로그인, 0이라면 로그인 실패 화면으로 전달한다.*/
      if(acntNum == 999) {																			
    	  PanelSwitch.ChangePanel(mf, thisPanel, new AdminManagementPanel(mf).adminManagement(mf));		/* 회원번호가 999인경우 관리자 페이지로 이동한다 */
      }else if(acntNum> 0) {
    	  PanelSwitch.ChangePanel(mf, thisPanel, new DisplayOpenningPanel((JFrame)mf, acntNum));					/* 회원번호가 0보다 큰 경우 메인메뉴로 이동한다. */
      } else {
    	  PanelSwitch.ChangePanel(mf, thisPanel, new LoginViewPanel(mf).loginFailed(mf));					/* 존재하지 않는 회원일때 0을 리턴하고, 이때 로그인 실패화면으로 이동한다. */
      }
      
      return ;
   }

   /**
    * updatePwdById : 입력받은 아이디와 비밀번호로 회원인지를 확인하여, 맞는 출력부분으로 보내주는 메소드.  
    * @param regist : 비회원이 입력한 아이디, 이름, 이메일주소를 각각 "acntId", "memName", "memEmail"의 key값으로 저장하고있는 Map
    * @return result : 
    * 
    * @author 홍성원
    * */
   public void updatePwdById(String acntId, String pwd) {
      loginService = new LoginService();
      int result = loginService.updatePwdById(acntId, pwd);
      
   }
   
   
   
   
   
   
   /* 마이페이지 입장하면 현재 포켓몬을 출력해야되는데 
    * 회원번호를 보내주면 현재포켓몬을 반환하는 과정의 컨트롤러부분
    * */
   public int getCurPokemonNumByAcntNum(int acntNum) {
      LoginService loginService = new LoginService();
      int result = loginService.getCurPokemonNumByAcntNum(acntNum);
      
      return result;
   }
   
   /* 마이페이지에서 포켓몬변경을 선택하면, 회원번호와 변경할 포켓몬번호를 보내주는데
    * 포켓몬을 보유하고있으면 1 반환 아니라면 0 반환할 예정
    * */
   public int changePokemon(int acntNum, int pokeNum) {
      int result = 0;
      
      LoginService loginService = new LoginService();
      result = loginService.changePokemon(acntNum, pokeNum);
      
      return result;
   }
   
   public int changeCostume(int acntNum, int costumNum) {
      int result = 0;
      LoginService loginService = new LoginService();
      result = loginService.changeCostume(acntNum, costumNum);
            
      return result;
   }
   
   public int selectCurCos(int acntNum) {
      int result = 0;
      LoginService loginService = new LoginService();
      result = loginService.selectCurCostume(acntNum);
      
      return result;
   }

   public int pokemonCheckByNum(int acntNum, int i) {
      
      loginService = new LoginService();
      int result = 0;
      result = loginService.pokemonCheckByNum(acntNum, i);
      
      
      return result;
   }
   
   
}