package com.greedy.pokemonrun.controller;

import java.util.List;

import com.greedy.pokemonrun.model.dto.AccountDTO;
import com.greedy.pokemonrun.model.service.AdminService;

/**
 * <pre>
 * Class : AdminController
 * Comment : 관리자 페이지로 시작화면에서 관리자 계정으로 로그인 할 경우 서비스와 연결해주는 클래스 
 * History
 * 2021/12/13 (박휘림)
 * </pre>
 * @version 1
 * @author 박휘림
 * */
public class AdminController {
   
   private AdminService adminService = new AdminService();

   /**
    * selectAllAccounts : 관리자 계정으로 로그인 해서 모든 회원 계정의 정보를 조회
    * @return 모든 회원정보를 담은 accountList
    * @author 박휘림
    * */
   public List<AccountDTO> selectAllAccounts() {
	   /* loginService2의 selectAllAccounts메소드에서 accountList를 받아온다. */
      List<AccountDTO> accountList = adminService.selectAllAccounts();
      
      return accountList;
      
   }
   
   /**
    * updatePassword : 관리자 계정으로 로그인 해서 회원계정의 비밀번호를 수정
    * @param first 비밀번호를 수정할 회원 계정 아이디
    * @param second 수정할 비밀번호
    * @return 비밀번호 성공 여부에 따른 결과값
    * @author 박휘림
    * */
   public int updatePassword(String acntId, String acntPwd) {
       /* updateResult에 loginService2 updatePassword 결과값을 담아준다. */
      int updateResult = adminService.updatePassword(acntId, acntPwd);
      
      return updateResult;
      
   }
   
   /**
    * deleteAccount : 관리자 계정으로 로그인 해서 계정 삭제
    * @param first 삭제할 계정 아이디
    * @return 계정 삭제 성공 여부에 따른 결과값
    * @author 박휘림
    * */
   public int deleteAccount(String acntId) {
      /* deleteResult에 서비스의 deleteAccount메소드에서 리턴받은 결과값을 담아준다. */
      int deleteResult = adminService.deleteAccount(acntId);
      
      return deleteResult;
      
   }
}