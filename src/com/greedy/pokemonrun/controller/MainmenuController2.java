package com.greedy.pokemonrun.controller;

import java.util.List;
import java.util.Map;

import com.greedy.pokemonrun.model.dto.PokemonDTO;
import com.greedy.pokemonrun.model.service.MainmenuService2;

/**
 * <pre>
 * Class : MainmenuController2
 * Comment : rankInfo 메소드 생성 클래스
 * History
 * 2021/12/13 (차화응) 처음 작성
 * 2021/12/17 (구훈모) 구문 완료
 * </pre>
 * @version 1
 * @author 차화응
 * */
public class MainmenuController2 {

	MainmenuService2 mainmenuService2 = new MainmenuService2();
	
	public int changeCostume() {

		return 0;
	}

	public List<PokemonDTO> checkPokemon() {

		List<PokemonDTO> pokemonList = null;

		
		
		return pokemonList;
	}

	public int changePokemon() {

		return 0;
	}
	
	/**
	 * rankInfo 메소드
	 * @param first String 플레이어 ID
	 * @param second Integer 플레이어 HIGH_SCORE
	 * @ return rankList 랭킹 목록을 리턴값으로 반환
	 * */
	public Map<String, Integer> rankInfo() {

		Map<String, Integer> rankList = mainmenuService2.rankInfo();

		return rankList;
	}
}
