package com.greedy.pokemonrun.controller;

import com.greedy.pokemonrun.model.service.MainmenuService3;

/**
 * <pre>
 * Class : MainmenuController
 * Comment : 메인메뉴 뷰와 서비스를 이어주는 컨트롤러 클래스
 * History
 * 2021/12/12 (구훈모) 처음 작성
 * 2021/12/16 (구훈모) 완성
 * </pre>
 * @author 구훈모
 * */
public class MainmenuController3 {

	private MainmenuService3 mainmenuService3 = new MainmenuService3();
	
	/**
	 * <pre>
	 * Comment : 설정에서 계정삭제의 성공여부를 가져오는 메소드
	 * </pre>
	 * @author 구훈모
	 * @param acntNum : 로그인시 받아온 회원번호
	 * @param message : 계정탈퇴시 기입하는 메세지
	 * @return deleteResult : 계정탈퇴 성공여부를 정수로 리턴
	 * */
	public int deleteAcct(int acntNum, String message) {
		
		/* mainmenuService에서 update 결과 값을 받아 옴 */
		int deleteResult = mainmenuService3.deleteAcct(acntNum, message);
		
		if(deleteResult > 0) {
			System.out.println("계정 삭제 성공!");
		} else {
			System.out.println("실패다 이자식아!");
		}
		
		return deleteResult;
	}
	
	/**
	 * <pre>
	 * Comment : 설정에서 비밀번호 변경의 성공여부를 가져오는 메소드
	 * </pre>
	 * @author 구훈모
	 * @param acntNum : 로그인시 받아온 회원 번호
	 * @param acntPwd : 사용자가 입력하는 현재 비밀번호(회원번호를 이용해 현재 비밀번호와 비교 )
	 * @param newPwd : 사용자가 입력하는 새로운 비밀번호
	 * @param checkNewPwd : 사용자가 입력한 새로운 비밀번호와 일치하는지 비교하기 위한 비밀번호
	 * @return changeResult : 비밀변호 변경성공여부를 정수로 리턴
	 * */
	public int changePwd(int acntNum, String acntPwd, String newPwd, String checkNewPwd) {
		
		/* mainmenuService에서 update 결과 값을 받아 옴 */
		int changeResult = mainmenuService3.changPwd(acntNum, acntPwd, newPwd, checkNewPwd);
		
		if(changeResult > 0) {
			System.out.println("비밀번호 변경 성공!");
		} else {
			System.out.println("비밀번호 변경 실패다 이자식아 ");
		}
		
		return changeResult;
	}
}
