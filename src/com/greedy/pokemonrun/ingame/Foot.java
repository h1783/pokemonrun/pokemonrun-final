package com.greedy.pokemonrun.ingame;

import java.awt.Image;

/**
 * <pre>
 * Class : Foot
 * Comment : 발판의 이미지, X좌표 , Y좌표, 넓이, 높이를 담는다.
 * </pre>
 * @author 구훈모
 * */
public class Foot {

	private Image image;
	private int x;
	private int y;
	int width;
	int height;
	
	/**
	 * <pre>
	 * Comment : 기본 생성자 호출을 이용해서 변수들 초기화
	 * </pre>
	 * @author 구훈모
	 * @param image : 발판에 쓸 이미지
	 * @param x : 발판의 X좌표
	 * @param y : 발판의 Y좌표
	 * @param width : 발판의 넓이
	 * @param height : 발판의 높이
	 * */
	public Foot(Image image, int x, int y, int width, int height) {
		super();
		this.image = image;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public Image getImage() {
		return image;
	}
	
	public void setImage(Image image) {
		this.image = image;
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getWidth() {
		return width;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	
}
