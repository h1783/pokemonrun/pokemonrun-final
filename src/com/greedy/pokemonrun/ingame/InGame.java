package com.greedy.pokemonrun.ingame;

import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.greedy.pokemonrun.swingview.MainmenuPanel;
import com.greedy.pokemonrun.swingview.PanelSwitch;
import com.greedy.pokemonrun.swingview.ScorePanel;

/**
 * <pre>
 * Class : InGame
 * Comment : 게임플레이에 관련한 모든 기능들이 있는 클래스
 * History
 * 21/12/14 (구훈모)처음 작성
 * 21/12/19 (구훈모)더 이상의 개발 불가로 끝.feat 홍성원
 * </pre>
 * @author 구훈모
 * */
public class InGame extends JPanel {
	private JPanel thisPanel = this;
	private JFrame jf;
	private JTextField scoreLabel;

	/* 인게임 배경 스레드 준비 ---------------------------------*/
	ImageIcon backIc = new ImageIcon("images/게임배경.png");
	Image backImg = backIc.getImage();

	int backX = 0;
	int back2X = backImg.getWidth(null);

	/* 인게임 몬스터볼 스레드 준비 -------------------------------------*/
	public static int ballPoint = 0;

	ImageIcon monterB = new ImageIcon("images/몬스터볼.png");
	Image monsterBall = monterB.getImage().getScaledInstance(30, 30, 0);

	List<Item> ballList = new ArrayList<>();

	/* 피카츄 스레드 준비-----------------------------------*/
	int field = 500;
	int pikaX = 100;
	int pikaY = 400;
	boolean fall = false;
	boolean jump = false;
	int doubleJump = 0;

	ImageIcon pika = new ImageIcon("images/pikapika.gif");
	Image img = pika.getImage().getScaledInstance(100, 100, 0);

	static long getTime() {
		
		return Timestamp.valueOf(LocalDateTime.now()).getTime();
	}
	
	/* 인게임 발판 스레드 준비 1이면 발판 생성, 0이면 X-----------------------------------*/
	String stepPoint = "111100111101011010111010111110011110101101011101011111001111"
			+ "0101101011101011111001111010110101110101111100111101011010111010111"
			+ "110011110101101011101011111001111010110101110101111100111101011010"
			+ "111010111110011110101101011101011111001111010110101110101"
			+ "1101011101101011011111010101101101011110111110110110101011101010011010"
			+ "1101011010111001101110111010111011101010111010101101010010111011010";
	int stepCount = 0;
	int nowStep = field;

	ImageIcon step = new ImageIcon("images/발판.png");
	Image stepImg = step.getImage().getScaledInstance(50, 50, 0);

	List<Foot> stepList = new ArrayList<>();

	/* ----------------------------------------------------------------------------------------------- */
	
	/**
	 * <pre>
	 * Comment : stepPoint의 문자열을 한개 씩 분리해서 int형으로 바꿔주는 메소드 
	 * </pre>
	 * @param ground : 발판 유무를 판별해줄 문자열
	 * @param index : 문자열의 인덱스 번호
	 * */
	static int getGround(String ground, int index) {
		
		return Integer.parseInt(ground.substring(index, index + 1));
	}
	
	/* 호출될 때 기본생성자가 호출되는 것을 이용해 재시작시 ballPoint를 0으로 초기화 */
	public InGame() {
		ballPoint = 0;
	}

	public InGame(JFrame mf, int acntNum) {
		jf = mf;
		thisPanel = this;
	}

	/**
	 * <pre>
	 * Comment : 스레드를 추가할 판넬 => 배경, 몬스터볼, 발판, 피카츄, 점수
	 * 				, 시간 스레드가 존재..
	 * </pre>
	 * @author 구훈모
	 * */
	public JPanel runGame(JFrame mf, int acntNum) {
		this.setBounds(0, 0, 850, 600);
		this.setLayout(null);

		InGame thisPanel = this;

		setFocusable(true);

		/* 몬스터볼 생성 위치 정해주는 for문
		 * i = x좌표
		 * random = y좌표
		 * */
		for(int i = 600; i < 20000; i += (int) (Math.random()*100 + 100)) {
			int random = (int) (Math.random()*100 + 100) + 250;
			ballList.add(new Item(monsterBall, i, random));
		}

		/* 발판 생성 위치를 정해주는 for문 */
		for(int i = 0; i < stepPoint.length(); i++) {
			int tempX = i * stepImg.getWidth(null);
			
			/* 인덱스 번호가 1이면 발판 생성 */
			if(getGround(stepPoint, i) == 1) {
				stepList.add(new Foot(stepImg, tempX, 500, stepImg.getWidth(null), stepImg.getHeight(null)));
			}
		}

		/* 배경과 몬스터볼이 흐르도록 만들어주는 스레드 */
		new Thread(new Runnable() {

			@Override
			public void run() {
				
				/* 배경 흐르는 것이 무한 반복 되도록 true로 while문 돌리기 */
				while(true) {
					backX--; 														/* 배경1의 X좌표를 1씩 감소 */
					back2X--;														/* 배경2의 X좌표를 1씩 감소 */

					if(backX < -(backImg.getWidth(null))) {							/* 배경1의 X좌표가 -배경너비 가 되면 화면 밖으로 간 것이므로 다시 원래 위치에 위치 시킨다. */
						backX = backImg.getWidth(null);
					}
					if(back2X < -(backImg.getWidth(null))) {						/* 위와 마찬가지로  이렇게 해서 둘이 이어주면 배경이 계속 흐르게 됨. */
						back2X = backImg.getWidth(null);
					}

					/* 몬스터볼 List의 길이만큼 반복하여 몬스터볼의 X좌표를 3만큼씩 움직여 주는 for문 */
					for(int i = 0; i < ballList.size(); i++) {
						ballList.get(i).setX(ballList.get(i).getX()-3);
					}

					/* 피카츄의 영역(x좌표의 범위와 y좌표의 범위) 안에 몬스터볼의 x, y 좌표가 닿게 되면 몬스터볼이 사라지고, ballPoint가 1씩 증가 */
					for(int i = 0; i < ballList.size(); i++) {
						if((pikaX + 20 < ballList.get(i).getX()  && ballList.get(i).getX() < pikaX + 50 )
								&& (ballList.get(i).getY()  > pikaY  && ballList.get(i).getY() < pikaY + 80)) {
							ballList.remove(i);
							ballPoint += 1;
							scoreLabel.setText("SCORE : " + ballPoint);
							scoreLabel.setBackground(Color.DARK_GRAY);
						}
					}

					repaint();
					try {
						Thread.sleep(13);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				}
			}
		}).start();

		/* 시간을 재는 스레드 생성 */
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				JTextField label = new JTextField();
				
				label.setBounds(600, 0, 150, 50);
				label.setFont(new Font("Sanscerif", Font.BOLD, 20));
				thisPanel.add(label);
				
				/* sleep을 1000단위로 지정해서 1초 단위로 흐르도록 했다. */
				for(int i = 30; i >= 0; i--){
					try {
						Thread.sleep(1000);												/* 1초 단위로 지나도록 */ 
						label.setText("남은시간 : " + i);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if( i == 0) {
						/* 시간이 0이 되면 게임결과 화면으로 회원번호와 ballPoint를 넘겨준다 */
						PanelSwitch.ChangePanel(mf, thisPanel, new ScorePanel(mf, acntNum, ballPoint));
					}
				} 
			}
		});
		t.start();

		/* 캐릭터가 낙하 하는 스레드 */ 
		new Thread(new Runnable() {

			@Override
			public void run() {
				while(true) {
					int foot = pikaY + img.getHeight(null);

					/* 점프 중이지 않고, 캐릭터의 y축 + 이미지 높이 가 필드의 Y축보다 낮고, 떨어지는 중이 아니면 */
					if(jump == false && foot < field && fall == false) {
						fall = true;
						long t1 = getTime();
						long t2;
						int set = 1;

						/* Y축 비교이기 때문에 값이 크면 아래에 있는 것임 */
						while(foot < field) {
							t2 = getTime() - t1;
							int jumpY = set + (int)((t2) / 60);						/* 점프를 시작한 시점과 떨어지는 시점의 시간 차이를 이용해 Y축을 세팅 */

							if(foot + jumpY >= field) {
								pikaY = field - img.getWidth(null);
								
								break;
								
							} else {
								pikaY = pikaY + jumpY;
							}
							
							foot = pikaY + img.getHeight(null);
							
							repaint();

							if(jump == true) {

								break;
							}

							try {
								Thread.sleep(10);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						
						fall = false;

						if(jump == false) {
							doubleJump = 0;
						}

					}
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();

		/* 위 방향키를 눌렀을 때 피카츄가 점프 하는 리스너(스레드) 추가 */
		thisPanel.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_UP && doubleJump < 3) {

					new Thread(new Runnable() {
						@Override
						public void run() {
							jump = true;
							doubleJump++;
							int nowJump = doubleJump;
							int foot = pikaY + img.getHeight(null);							/* 발바닥의 위치는 이미지의 Y좌표 + 이미지의 높이이다. Y축이니까. */
							long t1 = getTime();
							long t2;
							int set = 5;
							int jumpY = 8;

							while(jumpY > 0) {
								t2 = getTime() - t1;
								jumpY = set - (int)((t2) / 40);
								pikaY = pikaY - jumpY;										/* 점프를 하면 점프높이만큼 이미지의 Y좌표가 감소. */ 
								foot = pikaY + img.getHeight(null);
							
								repaint();

								if(nowJump != doubleJump) {
								
									break;
								}

								try {
									Thread.sleep(10);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}
							if(nowJump == doubleJump) {
								jump = false;
							}

						}
					}).start();

				}
			}
		});

		new Thread(new Runnable() {

			@Override
			public void run() {
				while(true) {
					
					repaint();
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();

		/* 발판이 흐르도록 해주는 스레드 */
		new Thread(new Runnable() {
			@Override
			public void run() {
				while(true) {
					for(int i = 0; i < stepList.size(); i++) {
						stepList.get(i).setX(stepList.get(i).getX() - 4);						/* 발판의 X좌표를 4씩 감소시켜준다. */
					}
					int range = (int)(stepImg.getWidth(null) * 1.2);

					for(int i = 0; i < stepList.size(); i++) {
						if(stepList.get(i).getX() >= 0 && stepList.get(i).getX() < range) {
							stepCount = 1;
							break;
						} else if(i == stepList.size() - 1) {
							stepCount = 0;
						}
					}

					try {
						Thread.sleep(15);														/* 15 미리세컨즈 마다.. */
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();

		new Thread(new Runnable() {

			@Override
			public void run() {
				while(true) {
					int foot =  pikaY+ img.getHeight(null);

					if(stepCount == 0) {
						nowStep = 200;
					} else if(stepCount == 1 && foot > field) {
						nowStep = 2000;
					} else if(stepCount == 1 && foot < field) { 
						nowStep = field; 
					}

				}
			}
		}).start();

		scoreLabel = new JTextField("SCORE : " + ballPoint);
		scoreLabel.setBackground(Color.DARK_GRAY);
		scoreLabel.setFont(new Font("", Font.BOLD, 20));
		scoreLabel.setBounds(20, 0, 150, 50);
		this.add(scoreLabel);

		mf.add(thisPanel);

		return thisPanel;
	}

	/* 그림그려주는 메소드 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.drawImage(backImg, backX, 0, this);
		g.drawImage(backImg, back2X, 0, this);
		g.drawImage(img, pikaX, pikaY, this);

		/* 몬스터볼 그려주는 for문 */
		for(int i = 0; i < ballList.size(); i++) {
			g.drawImage(ballList.get(i).getImage(), ballList.get(i).getX(), ballList.get(i).getY(), this);
		}
		
		/* 발판 그려주는 for문 */
		for(int i = 0; i < stepList.size(); i++) {
			Image tempImg = stepList.get(i).getImage();
			int tempX = stepList.get(i).getX();
			int tempY = stepList.get(i).getY();
			int tempWidth = stepList.get(i).getWidth();
			int tempHeight = stepList.get(i).getHeight();
			g.drawImage(tempImg, tempX, tempY, tempWidth, tempHeight, null);
		}


	}
}
