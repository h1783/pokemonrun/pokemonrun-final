package com.greedy.pokemonrun.ingame;

import java.awt.Image;

/**
 * <pre>
 * Class : Item
 * Comment : 몬스터볼의 이미지, X좌표 , Y좌표, 넓이, 높이를 담는다.
 * </pre>
 * @author 구훈모
 * */
public class Item {
	
	private Image image;
	private int x;
	private int y;
	
	/**
	 * Comment : 기본생성자 호출을 이용한 초기화
	 * @author 구훈모
	 * @param image : 몬스터볼의 이미지
	 * @param x : 몬스터볼 이미지의 X좌표
	 * @param y : 몬스터볼 이미지의 Y좌표
	 * */
	public Item(Image image, int x, int y) {
		super();
		this.image = image;
		this.x = x;
		this.y = y;
	}
	
	public Image getImage() {
		return image;
	}
	
	public void setImage(Image image) {
		this.image = image;
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}

	
}
