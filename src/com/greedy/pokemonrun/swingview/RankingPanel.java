package com.greedy.pokemonrun.swingview;

import java.awt.Font;
import java.awt.Image;
import java.awt.TextArea;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.greedy.pokemonrun.controller.MainmenuController2;
import com.greedy.pokemonrun.swingview.MainmenuPanel;
import com.greedy.pokemonrun.swingview.PanelSwitch;

/*
 * <pre>
 * Class : RankingPanel
 * Comment : 랭킹 패널 출력 클래스
 * History
 * 2021/12/15 (차화응) 처음 작성
 * 2021/12/18 (구훈모) 구문 완료
 * </pre>
 * @version 1
 * @author 차화응
 * */
public class RankingPanel extends JPanel {
	
	MainmenuController2 mainmenuController2 = new MainmenuController2();
	
	Image rankingView = new ImageIcon("images/랭킹화면.png").getImage().getScaledInstance(850, 600, 0);
	Image backButton = new ImageIcon("images/button/뒤로가기버튼.png").getImage().getScaledInstance(200, 80, 0);
	
	public RankingPanel(JFrame mf, int acntNum) {
		
		this.setSize(850, 600);
		this.setLayout(null);
		
		RankingPanel thisPanel = this;
		
		JLabel rankingLabel = new JLabel(new ImageIcon(rankingView));
		rankingLabel.setBounds(0, 0, 850, 600);
		JButton backBtn = new JButton(new ImageIcon(backButton));
		backBtn.setBounds(590, 450, 200, 80);
		
		backBtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new MainmenuPanel(mf, acntNum));
			}
		});
		
		Font font1 = new Font("",0,35);
        TextArea ta1 = new TextArea();
        ta1.setBounds(150, 120, 500, 300);
        ta1.setFont(font1);
        ta1.setEditable(false);

        int rank = 1;
        Map<String, Integer> rankList = mainmenuController2.rankInfo();
        for(String key : rankList.keySet()) {
        	int value = rankList.get(key);
        	ta1.append(rank + " 위  ID :  " + key + " 점수 : " + value + "\n");
        	rank++;
        }
		
		rankingLabel.add(backBtn);
		rankingLabel.add(ta1);
		this.add(rankingLabel);
		mf.add(this);
		
	}
	
}
