package com.greedy.pokemonrun.swingview;

import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class DrawFailPanel extends JPanel {

	Image failView = new ImageIcon("images/shop/뽑기실패.png").getImage().getScaledInstance(850, 600, 0);
	Image okBt = new ImageIcon("images/button/확인.png").getImage().getScaledInstance(80, 50, 0);

	public DrawFailPanel(JFrame mf , int acntNum) {
		
		this.setSize(850, 600);
		this.setLayout(null);
		
		DrawFailPanel thisPanel = this;
		
		JLabel failLabel = new JLabel(new ImageIcon(failView));
		failLabel.setBounds(0, 0, 850, 600);
		
		JButton button = new JButton(new ImageIcon(okBt));
		button.setBounds(425, 500, 80, 50);
		
		button.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new DrawPanel(mf, acntNum));
			}
		});

		failLabel.add(button);
		this.add(failLabel);
		this.repaint();
		this.revalidate();
		
		mf.add(this);
		mf.repaint();
		mf.revalidate();
		
	}
}
