package com.greedy.pokemonrun.swingview;

import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class DrawSucessPanel extends JPanel {
	
	Image sucessView = new ImageIcon("images/shop/뽑기성공내역추가.png").getImage().getScaledInstance(850, 600, 0);
	Image okBt = new ImageIcon("images/button/확인.png").getImage().getScaledInstance(80, 50, 0);
	
	public DrawSucessPanel(JFrame mf, int acntNum) {
		
		this.setSize(850, 600);
		this.setLayout(null);
		
		DrawSucessPanel thisPanel = this;
		
		JLabel sucessLabel = new JLabel(new ImageIcon(sucessView));
		sucessLabel.setBounds(0, 0, 850, 600);
		
		JButton button = new JButton(new ImageIcon(okBt));
		button.setBounds(425, 500, 80, 50);
		
		button.addMouseListener(new MouseAdapter() { //상점이동
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new DrawPanel(mf, acntNum));	
			}	
		});
		
		this.add(sucessLabel);
		this.add(button);
		this.repaint();
		this.revalidate();
		
		mf.add(this);
		mf.repaint();
		mf.revalidate();
		
	}
	
	
	
}
