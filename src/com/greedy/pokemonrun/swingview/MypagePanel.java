package com.greedy.pokemonrun.swingview;

import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.greedy.pokemonrun.controller.LoginController;


public class MypagePanel extends JPanel {
	
	// 피카츄 - 거북왕 - 이상해씨 - 고오스 - 리자몽 - 피존투 - 뮤 - 미뇽 - 푸린 - 팬텀

	private Image mypageView;
	private Image changePokemonBt;
	private Image changeCostumeBt;
	private Image goBackBt;
	private Image scarf;
	private Image cap;
	private JPanel thisPanel;

//	private Image poke1;
//	private Image poke2;
//	private Image poke3;
//	private Image poke4;
//	private Image poke5;
//	private Image poke6;
//	private Image poke7;
//	private Image poke8;
//	private Image poke9;
//	private Image poke10;
//	private Image[] pokeArr;
	
	
	public MypagePanel(JFrame mf, int acntNum) {
		
		setUp();
		this.setSize(850, 600);
		this.setLayout(null);
		thisPanel = this;
	}
	
	
	
	public JPanel myPageView(JFrame mf, int acntNum) {
		JLabel mypageLabel = new JLabel(new ImageIcon(mypageView));
		mypageLabel.setBounds(0, 0, 850, 600);
		
		
		JButton button1 = new JButton(new ImageIcon(changePokemonBt));
		JButton button2 = new JButton(new ImageIcon(changeCostumeBt));
		JButton button3 = new JButton(new ImageIcon(goBackBt));
		button1.setBounds(550, 100, 180, 80);
		button2.setBounds(550, 220, 180, 70);
		button3.setBounds(600, 400, 100, 50);
		
		button1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new ChangePokemonPanel(mf).changePokemon(mf, acntNum));
			}
		});
		
		button2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new ChangeCostumePanel(mf).changeCostume(mf, acntNum));
			}
		});
		
		button3.addMouseListener(new MouseAdapter() {
			@Override 
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new MainmenuPanel(mf, acntNum)); 
			} 
		});
		 
		
		JLabel pokeLabel = setUpCurPokemon(acntNum);
		JLabel cosLabel = setupCurCos(acntNum);
//		int pokeNum = 0;
//		int cosNum = 0;
//		switch(cosNum) {
//			case 1 : 
//		}
		
		pokeLabel.setBounds(300, 200, 200, 200);
		cosLabel.setBounds(350, 170, 50, 50);
		mypageLabel.add(pokeLabel);
		mypageLabel.add(cosLabel);
		
		mypageLabel.add(button1);
		mypageLabel.add(button2);
		mypageLabel.add(button3);
		thisPanel.add(mypageLabel);
		mf.add(thisPanel);
		mf.repaint();
		mf.revalidate();	
		
		
		
		return thisPanel;
		
	}
		

	private JLabel setUpCurPokemon(int acntNum) {
		setUp();
		LoginController lc = new LoginController();
		int curPoke = lc.getCurPokemonNumByAcntNum(acntNum);
//		int curCos = lc.selectCurCos(acntNum);
//		
//		JLabel scarfCostume = new JLabel(new ImageIcon(new ImageIcon("images/costume/스카프.png").getImage()));
//		scarfCostume.setBounds(300, 400, 100, 100);
//		scarfCostume.setVisible(true);

//		JLabel capCostume = new JLabel(new ImageIcon(new ImageIcon("images/costume/모자.png").getImage()));
//		capCostume.setBounds(200, 400, 100, 100);
//		capCostume.setVisible(true);		
		
		JLabel curPokemonLabel = new JLabel(new ImageIcon(new ImageIcon("images/pokemon/피카츄1.png").getImage())); 
		curPokemonLabel.setBounds(0, 0, 440, 500);
//		curPokemonLabel.add(scarfCostume);
//		curPokemonLabel.add(capCostume);
		
		switch(curPoke) {
			case 1: curPokemonLabel = new JLabel(new ImageIcon(new ImageIcon("images/pokemon/피카츄1.png").getImage())); break;
			case 2: curPokemonLabel = new JLabel(new ImageIcon(new ImageIcon("images/pokemon/거북왕1.png").getImage()));break;
			case 3: curPokemonLabel = new JLabel(new ImageIcon(new ImageIcon("images/pokemon/이상해씨1.png").getImage()));break;
			case 4: curPokemonLabel = new JLabel(new ImageIcon(new ImageIcon("images/pokemon/고오스1.png").getImage()));break;
			case 5: curPokemonLabel = new JLabel(new ImageIcon(new ImageIcon("images/pokemon/리자몽1.png").getImage()));break;
			case 6: curPokemonLabel = new JLabel(new ImageIcon(new ImageIcon("images/pokemon/피존투1.png").getImage()));break;
			case 7: curPokemonLabel = new JLabel(new ImageIcon(new ImageIcon("images/pokemon/뮤1.png").getImage()));break;
			case 8: curPokemonLabel = new JLabel(new ImageIcon(new ImageIcon("images/pokemon/미뇽1.png").getImage()));break;
			case 9: curPokemonLabel = new JLabel(new ImageIcon(new ImageIcon("images/pokemon/푸린1.png").getImage()));break;
			case 10: curPokemonLabel = new JLabel(new ImageIcon(new ImageIcon("images/pokemon/팬텀1.png").getImage()));break;
			default : break;
		}
		
//		switch(curCos) {
//			case 1: scarfCostume.setVisible(true); capCostume.setVisible(true);
//			case 2: scarfCostume.setVisible(true); capCostume.setVisible(true);
//			
//		}

		
		return curPokemonLabel;
	}
	
	private JLabel setupCurCos(int acntNum) {
		setUp();
		LoginController lc = new LoginController();
		int curCos = lc.selectCurCos(acntNum);
		
		JLabel curCosLabel = new JLabel(new ImageIcon(new ImageIcon("").getImage().getScaledInstance(50, 50, 0)));
//		curCosLabel.setBounds(400, 20, 50, 50);

		switch(curCos) {
			case 1: curCosLabel = new JLabel(new ImageIcon(new ImageIcon("images/costume/스카프.png").getImage().getScaledInstance(50, 50, 0))); break;
			case 2: curCosLabel = new JLabel(new ImageIcon(new ImageIcon("images/costume/모자.png").getImage().getScaledInstance(50, 50, 0))); break;
			default : break;
		}
		
		return curCosLabel;
		
	}
	
	
	// 피카츄 - 거북왕 - 이상해씨 - 고오스 - 리자몽 - 피존투 - 뮤 - 미뇽 - 푸린 - 팬텀
	private void setUp() {

		mypageView = new ImageIcon("images/시작화면2.png").getImage().getScaledInstance(850, 600, 0);
		changePokemonBt = new ImageIcon("images/button/changepokbt.png").getImage().getScaledInstance(180, 80, 0);
		changeCostumeBt = new ImageIcon("images/button/costumebt.png").getImage().getScaledInstance(180, 70, 0);
		goBackBt = new ImageIcon("images/button/뒤로가기버튼.png").getImage().getScaledInstance(100, 50, 0);
		
//		poke1 = new ImageIcon("images/pokemon/피카츄1.png").getImage();
//		poke2 = new ImageIcon("images/pokemon/거북왕1.png").getImage();
//		poke3 = new ImageIcon("images/pokemon/이상해씨1.png").getImage();
//		poke4 = new ImageIcon("images/pokemon/고오스1.png").getImage();
//		poke5 = new ImageIcon("images/pokemon/리자몽1.png").getImage();
//		poke6 = new ImageIcon("images/pokemon/피존1.png").getImage();
//		poke7 = new ImageIcon("images/pokemon/뮤1.png").getImage();
//		poke8 = new ImageIcon("images/pokemon/미뇽1.png").getImage();
//		poke9 = new ImageIcon("images/pokemon/푸린1.png").getImage();
//		poke10 = new ImageIcon("images/pokemon/팬텀1.png").getImage();
//		
		
		
	}
}
