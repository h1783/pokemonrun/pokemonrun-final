package com.greedy.pokemonrun.swingview;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.greedy.pokemonrun.controller.MainmenuController3;

/**
 * <pre>
 * Class : SettingViewPanel
 * Comment : 설정화면에서 이루어지는 화면들을 구현 한 클래스이다.
 * History 
 * 21/12/14 (구훈모)시작
 * 21/12/19 (구훈모)완료
 * </pre>
 * @author 구훈모
 * */
public class SettingViewPanel extends JPanel {


	private SettingViewPanel thisPanel;
	private Image settingView;
	private Image deleteAccBt; 
	private Image pwdChangeBt;
	private Image goBackBt;
	private Image onBt;
	private Image offBt;
	private Image pwdChangeView;
	private Image okBt;
	private Image cancleBt;
	private Image changePwdFailedView;
	private Image changePwdSuccessView;
	private Image deleteAccView;
	private Image deleteAccSuccess;
	private Image deleteAccFailed;

	public SettingViewPanel() {

		setup();
		thisPanel = this;
	}

	/**
	 * <pre>
	 * Comment : 기본 생성자를 이용해 필드 값들을 초기화 해줌
	 * </pre>
	 * @author 구훈모
	 * */
	private void setup() {
		settingView = new ImageIcon("images/settingView2.png").getImage().getScaledInstance(850, 600, 0);
		deleteAccBt = new ImageIcon("images/button/설정deleteAccBt.png").getImage().getScaledInstance(150, 100, 0); 
		pwdChangeBt = new ImageIcon("images/button/설정pwdChangeBt.png").getImage().getScaledInstance(150, 100, 0);
		goBackBt = new ImageIcon("images/button/backBt.png").getImage().getScaledInstance(100, 80, 0);
		onBt = new ImageIcon("images/button/onBt.png").getImage().getScaledInstance(50, 50, 0);
		offBt = new ImageIcon("images/button/offBt.png").getImage().getScaledInstance(50, 50, 0);
		pwdChangeView = new ImageIcon("images/pwdChangeView.png").getImage().getScaledInstance(850, 600, 0);
		okBt = new ImageIcon("images/button/확인Bt.png").getImage().getScaledInstance(100, 100, 0);
		cancleBt = new ImageIcon("images/button/취소Bt.png").getImage().getScaledInstance(100, 100, 0);
		changePwdFailedView = new ImageIcon("images/비밀번호변경실패화면.png").getImage().getScaledInstance(850, 600, 0);
		changePwdSuccessView = new ImageIcon("images/비밀번호변경성공화면.png").getImage().getScaledInstance(850, 600, 0);
		deleteAccView = new ImageIcon("images/deleteAccView.png").getImage().getScaledInstance(850, 600, 0);
		deleteAccSuccess = new ImageIcon("images/계정삭제성공화면.png").getImage().getScaledInstance(850, 600, 0);
		deleteAccFailed = new ImageIcon("images/계정삭제실패화면.png").getImage().getScaledInstance(850, 600, 0);
	}


	/**
	 * <pre>
	 * Comment : 설정의 메인 화면 구현
	 * </pre>
	 * @author 구훈모
	 * @param mf : 판넬을 얹을 프레임
	 * @param acntNum : 로그인시 넘겨받은 회원 번호
	 * @return this : 프레임에 올릴 판넬
	 * */
	public JPanel SettingMainView(JFrame mf, int acntNum) {

		this.setSize(850, 600);
		this.setLayout(null);

		JLabel viewLabel = new JLabel(new ImageIcon(settingView));
		viewLabel.setBounds(0, 0, 850, 600);

		JButton deleteAcc = new JButton(new ImageIcon(deleteAccBt));		/* 계정삭제 버튼 */
		JButton pwdChange = new JButton(new ImageIcon(pwdChangeBt));		/* 비밀번호 변경 버튼 */
		JButton goBack = new JButton(new ImageIcon(goBackBt));				/* 뒤로가기 버튼 */
		JButton on = new JButton(new ImageIcon(onBt));						/* 음향 On 버튼 */
		JButton off= new JButton(new ImageIcon(offBt));						/* 음량 Off 버튼 */
		deleteAcc.setBounds(200, 400, 150, 100);
		pwdChange.setBounds(500, 400, 150, 100);
		goBack.setBounds(600, 100, 100, 80);
		on.setBounds(300, 190, 50, 50);
		off.setBounds(300, 290, 50, 50);

		/* 계정탈퇴 버튼 클릭 시 계정탈퇴 화면으로 전환 */
		deleteAcc.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new SettingViewPanel().deleteAccView(mf, acntNum));
			}
		});

		/* 비밀번호 변경 버튼 클릭시 비밀번호 변경 화면 전환 */
		pwdChange.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new SettingViewPanel().pwdChangeView(mf, acntNum));
			}
		});

		/* 뒤로가기 버튼 클릭시 메인메뉴 화면으로 전환 */
		goBack.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new MainmenuPanel(mf, acntNum));
			}
		});

		this.add(viewLabel);
		this.add(on);
		this.add(off);
		this.add(deleteAcc);
		this.add(pwdChange);
		this.add(goBack);
		this.repaint();

		mf.add(this);

		return this;

	}

	/**
	 * <pre>
	 * Comment : 비밀번호 변경 화면
	 * </pre>
	 * @author 구훈모
	 * @param mf : 판넬을 얹을 프레임
	 * @param acntNum : 로그인시 넘겨받은 회원 번호
	 * @return this : 프레임에 올릴 판넬
	 * */
	public JPanel pwdChangeView(JFrame mf, int acntNum) {

		JPanel jp = new JPanel();
		thisPanel.setSize(850, 600);
		thisPanel.setLayout(null);

		JLabel viewLabel = new JLabel(new ImageIcon(pwdChangeView));			/* 비밀번호 변경화면 배경 이미지 */
		viewLabel.setBounds(0, 0, 850, 600);									
		
		Font font = new Font("",0,30);
		
		/* 현재 비밀번호를 입력받는다. */
		JPasswordField curPwd = new JPasswordField();							/* 현재 비밀번호를 입력 받는 텍스트 필드 */
		curPwd.setBounds(400, 160, 200, 50);
		curPwd.setFont(font);
		
		/* 변경할 새 비밀번호를 입력받는다. */
		JPasswordField newPwd1 = new JPasswordField();							/* 바꿀 비밀번호를 입력 받는 텍스트 필드 */
		newPwd1.setBounds(400, 240, 200, 50);
		newPwd1.setFont(font);
		
		/* 변경할 새 비밀번호를 한번더 확인한다 (일치여부확인용) */
		JPasswordField checkNewPwd1 = new JPasswordField();						/* 바꿀 비밀번호 일치 여부 확인을 위해 입력 받는 텍스트 필드 */
		checkNewPwd1.setBounds(400, 310, 200, 50);
		checkNewPwd1.setFont(font);

		JButton ok = new JButton(new ImageIcon(okBt));							/* 변경하기 버튼 */
		ok.setBounds(520, 400, 100, 100);
		JButton cancle = new JButton(new ImageIcon(cancleBt));					/* 취소 버튼 */
		cancle.setBounds(220, 400, 100, 100);

		ok.addMouseListener(new MouseAdapter() {
			
			/* 비밀번호 변경 성공 여부를 확인하는 Controller 호출하여 결과 값을 받아 결과에 따라 다른 view 호출 */
			@Override
			public void mouseClicked(MouseEvent e) {
				MainmenuController3 mainmenuController3 = new MainmenuController3();
				String acntPwd = curPwd.getText();
				String newPwd = newPwd1.getText();
				String checkNewPwd = checkNewPwd1.getText();
				int changeYN = mainmenuController3.changePwd(acntNum, acntPwd, newPwd, checkNewPwd);
				if(changeYN > 0) {
					/* 비밀번호 변경 성공 시 화면 */
					PanelSwitch.ChangePanel(mf, thisPanel, new SettingViewPanel().pwdChangeSuccess(mf, acntNum));
				} else {
					/* 비밀번호 변경 실패 시 화면 */
					PanelSwitch.ChangePanel(mf, thisPanel, new SettingViewPanel().pwdChangeFailed(mf, acntNum));
				}
			}

		});

		cancle.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new SettingViewPanel().SettingMainView(mf, acntNum));
			}
		});


		viewLabel.add(curPwd);
		viewLabel.add(newPwd1);
		viewLabel.add(checkNewPwd1);
		viewLabel.add(ok);
		viewLabel.add(cancle);
		thisPanel.add(viewLabel);

		mf.add(thisPanel);

		return thisPanel;

	}
	
	/**
	 * <pre>
	 * Comment : 비밀번호 실패했을 때 화면
	 * </pre>
	 * @author 구훈모
	 * @param mf : 판넬을 얹을 프레임
	 * @param acntNum : 로그인시 넘겨받은 회원 번호
	 * @return this : 프레임에 올릴 판넬
	 * */
	public JPanel pwdChangeFailed(JFrame mf, int acntNum) {

		thisPanel.setSize(850, 600);
		thisPanel.setLayout(null);		

		JLabel changeFailedView = new JLabel(new ImageIcon(changePwdFailedView));
		changeFailedView.setBounds(0, 0, 850, 600);

		JButton ok = new JButton(new ImageIcon(okBt));
		ok.setBounds(380, 450, 100, 100);

		ok.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new SettingViewPanel().pwdChangeView(mf, acntNum));
			}
		});

		changeFailedView.add(ok);
		thisPanel.add(changeFailedView);
		mf.add(thisPanel);

		return thisPanel;
	}

	/**
	 * <pre>
	 * Comment : 비밀번호 변경 성공 화면
	 * </pre>
	 * @author 구훈모
	 * @param mf : 판넬을 얹을 프레임
	 * @param acntNum : 로그인시 넘겨받은 회원 번호
	 * @return this : 프레임에 올릴 판넬
	 * */
	public JPanel pwdChangeSuccess(JFrame mf, int acntNum) {

		thisPanel.setSize(850, 600);
		thisPanel.setLayout(null);

		JLabel changeSuccessView = new JLabel(new ImageIcon(changePwdSuccessView));
		changeSuccessView.setBounds(0, 0, 850, 600);

		JButton ok = new JButton(new ImageIcon(okBt));
		ok.setBounds(380, 450, 100, 100);

		ok.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new SettingViewPanel().SettingMainView(mf, acntNum));
			}
		});

		changeSuccessView.add(ok);
		thisPanel.add(changeSuccessView);
		mf.add(thisPanel);

		return thisPanel;
	}

	/**
	 * <pre>
	 * Comment : 계정삭제 메인 화면
	 * </pre>
	 * @author 구훈모
	 * @param mf : 판넬을 얹을 프레임
	 * @param acntNum : 로그인시 넘겨받은 회원 번호
	 * @return this : 프레임에 올릴 판넬
	 * */
	public JPanel deleteAccView(JFrame mf, int acntNum) {

		this.setSize(850, 600);
		this.setLayout(null);

		JLabel deleteView = new JLabel(new ImageIcon(deleteAccView));
		deleteView.setBounds(0, 0, 850, 600);

		Font font = new Font("",0,30);
		
		/* 계정 탈퇴시 필요한 "계정탈퇴"를 입력 받아 맞게 입력했는지 비교한다 */
		JTextField inputMessage = new JTextField();
		inputMessage.setBounds(350, 250, 200, 50);
		inputMessage.setFont(font);

		JButton ok = new JButton(new ImageIcon(okBt));
		ok.setBounds(530, 400, 100, 100);
		JButton cancle = new JButton(new ImageIcon(cancleBt));
		cancle.setBounds(220, 400, 100, 100);

		/* "계정탈퇴" 라는 단어를 제대로 입력했는지 확인해서 계정 탈퇴를 진행하는 Controller 호출하여 결과값에 따른 뷰 호출 */
		ok.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				MainmenuController3 mainmenuController3 = new MainmenuController3();
				String message = inputMessage.getText();
				int result = mainmenuController3.deleteAcct(acntNum, message);

				if(result > 0) {
					PanelSwitch.ChangePanel(mf, thisPanel, new SettingViewPanel().deleteSuccessView(mf, acntNum));
				} else {
					PanelSwitch.ChangePanel(mf, thisPanel, new SettingViewPanel().deleteFailedView(mf, acntNum));
				}

			}
		});
		
		cancle.addMouseListener(new MouseAdapter() {
			@Override 
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new SettingViewPanel().SettingMainView(mf, acntNum));
			}
		});

		deleteView.add(inputMessage);
		deleteView.add(ok);
		deleteView.add(cancle);
		this.add(deleteView);
		mf.add(this);

		return thisPanel;				
	}

	/**
	 * <pre>
	 * Comment : 계정 탈퇴 실패 화면
	 * </pre>
	 * @author 구훈모
	 * @param mf : 판넬을 얹을 프레임
	 * @param acntNum : 로그인시 넘겨받은 회원 번호
	 * @return this : 프레임에 올릴 판넬
	 * */
	public JPanel deleteFailedView(JFrame mf, int acntNum) {
		this.setSize(850, 600);
		this.setLayout(null);

		JLabel failedView = new JLabel(new ImageIcon(deleteAccFailed));
		failedView.setBounds(0, 0, 850, 600);

		JButton ok = new JButton(new ImageIcon(okBt));
		ok.setBounds(400, 400, 100, 100);
		
		ok.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new SettingViewPanel().deleteAccView(mf, acntNum));
			}
		});
		
		failedView.add(ok);
		this.add(failedView);
		mf.add(thisPanel);

		return thisPanel;
	}
	
	/**
	 * <pre>
	 * Comment : 계정탈퇴 성공 화면
	 * </pre>
	 * @author 구훈모
	 * @param mf : 판넬을 얹을 프레임
	 * @param acntNum : 로그인시 넘겨받은 회원 번호
	 * @return this : 프레임에 올릴 판넬
	 * */
	public JPanel deleteSuccessView(JFrame mf, int acntNum) {
		this.setSize(850, 600);
		this.setLayout(null);

		JLabel successView = new JLabel(new ImageIcon(deleteAccSuccess));
		successView.setBounds(0, 0, 850, 600);

		JButton ok = new JButton(new ImageIcon(okBt));
		ok.setBounds(400, 400, 100, 100);
		
		ok.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new PokerunFirstView(mf));
			}
		});
		
		successView.add(ok);
		this.add(successView);
		mf.add(thisPanel);

		return thisPanel;
	}
}














