package com.greedy.pokemonrun.swingview;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.greedy.pokemonrun.controller.LoginController;

/** 
 * <pre>
 * Class : SignUpPanel
 * Comment : 회원가입에 관련된 패널을 생성하는 메소드들을 모아둔 회원가입전용 Panel 클래스
 * History
 * 2021/12/18 (홍성원) 작성
 * </pre>
 * @version 1
 * @author 홍성원
 * */
public class SignUpPanel extends JPanel {
	private Image gaipView;
	private Image goBackBt;
	private Image submit;
	private Image signUpPwdView;
	private Image pwd;
	private Image signbt;
	private Image signUpFail;
	private SignUpPanel thisPanel;
	
	/* 프레임을 전달인자로 넣어 생성자를 호출하면 회원가입 출력 View들이 사용하는 이미지들을 생성해준다. */
	public SignUpPanel(JFrame mf) {
		
		setup();
		thisPanel = this;
	}
	
	/** 
	 * signUpFail : 회원가입중 잘못된 값을 입력받았을 경우 예외처리를 담당하는 패널 
	 *
	 * @param mf : 패널을 더해줄 프레임을 전달받는다
	 * @author 홍성원
	 */
	public SignUpPanel signUpFail(JFrame mf) {
		
		thisPanel.setBounds(0, 0, 850, 600);
		
		JLabel signLabel = new JLabel(new ImageIcon(signUpFail));
		signLabel.setBounds(0, 0, 850, 600);
		
		JButton submitButton = new JButton(new ImageIcon(submit));
		submitButton.setBounds(490, 430, 160, 50);
		signLabel.add(submitButton);
		submitButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new SignUpPanel(mf).signUpMain(mf));
				}
		});
		
		thisPanel.add(signLabel);
		mf.add(thisPanel);
		
		return thisPanel;
	}

	/**signUpFail : 회원가입중 잘못된 값을 입력받았을 경우 예외처리를 담당하는 패널 
	 * 
	 * @param mf : 패널을 더해줄 프레임을 전달받는다
	 * @author 홍성원
	 */
	private SignUpPanel signUpFail(JFrame mf, Map<String, String> signInfo) {
		
		thisPanel.setBounds(0, 0, 850, 600);
		
		JLabel signLabel = new JLabel(new ImageIcon(signUpFail));
		signLabel.setBounds(0, 0, 850, 600);
		
		JButton submitButton = new JButton(new ImageIcon(submit));
		submitButton.setBounds(490, 430, 160, 50);
		signLabel.add(submitButton);
		
		/* 재입력을 요구한 다음 확인버튼을 클릭하면 이전 화면으로 돌아가게 이벤트를 부여해준다*/
		submitButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new SignUpPanel(mf).signUpPwd(mf, signInfo));
				}
		});

		thisPanel.add(signLabel);
		mf.add(thisPanel);

		return thisPanel;
	}
	
	/** 
	 * signUpPwd : 회원가입때 입력한 이름 이메일 아이디의 중복이 없을시, 비밀번호 입력을 출력해주는 패널 
	 *
	 * @param mf : 패널을 더해줄 프레임을 전달받는다
	 * @author 홍성원
	 */
	public JPanel signUpPwd(JFrame mf,Map<String, String> signInfo) {
		
		this.setBounds(0, 0, 850, 600);;
		this.setLayout(null);
		Font font = new Font("",0,30);
		
		JLabel signUpPwd = new JLabel(new ImageIcon(signUpPwdView));
		signUpPwd.setBounds(0, 0, 850, 600);

		JLabel pwd1Label = new JLabel(new ImageIcon(pwd));
		pwd1Label.setBounds(150, 250, 200, 50);
		JPasswordField pwdText = new JPasswordField();
		pwdText.setBounds(370, 250, 240, 50);
		pwdText.setFont(font);

		JLabel pwd2Label = new JLabel(new ImageIcon(pwd));
		pwd2Label.setBounds(150, 320, 200, 50);
		JPasswordField pwd2Text = new JPasswordField();
		pwd2Text.setBounds(370, 320, 240, 50);
		pwd2Text.setFont(font);

		JButton goBackButton = new JButton(new ImageIcon(goBackBt));
		JButton submitButton = new JButton(new ImageIcon(submit));
		
		goBackButton.setBounds(200, 430, 130, 40);
		submitButton.setBounds(490, 430, 130, 40);

		signUpPwd.add(pwd1Label);
		signUpPwd.add(pwd2Label);
		signUpPwd.add(pwdText);
		signUpPwd.add(pwd2Text);
		signUpPwd.add(goBackButton);
		signUpPwd.add(submitButton);
		
		
		submitButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				LoginController loginController = new LoginController();
				String pwd1 = pwdText.getText();
				String pwd2 = pwd2Text.getText();
				int result = 0;
				
				/* 회원가입 과정중 비밀번호 입력의 예외처리부분 */
				/* 1. 빈칸을 입력한경우 회원가입 실패창으로 보낸다.
				 * 2. 8~20글자 사이의 길이가 아닌경우 실패창으로 보낸다.
				 * 3. 처음 입력한 비밀번호와 두번째 입력한 확인용 비밀번호가 다른경우 회원가입 실패창으로 보낸다
				 *   */
				/* 만약 위의 예외처리조건에 걸리지 않았다면 입력한 비밀번호로 설정 후 메인페이지로 바로 로그인한다. */
				if(pwd1 != null && pwd2 != null) { 
					if(pwd1.length() > 7 && pwd1.length() < 21) {
						if(pwd1.equals(pwd2)) {
							
							signInfo.put("acntPwd", pwd1);
							LoginController lc = new LoginController();
							
							lc.registAccountCheckInput2(mf, thisPanel, signInfo);
							
						}else{
							PanelSwitch.ChangePanel(mf, thisPanel, new SignUpPanel(mf).signUpFail(mf, signInfo));
						}
					}else{
						PanelSwitch.ChangePanel(mf, thisPanel, new SignUpPanel(mf).signUpFail(mf, signInfo));
					}
				}
				else{
					PanelSwitch.ChangePanel(mf, thisPanel, new SignUpPanel(mf).signUpFail(mf, signInfo));
				}
			}
			
		});
		
		/* 뒤로가기 버튼을 누른경우 초기화면으로 돌아가는 이벤트를 추가한다. */
		goBackButton.addMouseListener(new MouseAdapter() {
			@Override 
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new PokerunFirstView(mf));
			}
		});
		thisPanel.add(signUpPwd);
		mf.add(thisPanel);

		return thisPanel;
	}
	
	 
	 /** 
	 * signUpMain : 회원가입기능중 이름과 아이디 이메일을 입력받는 화면을 출력하는 뷰 
	 *
	 * @param mf : 패널을 더해줄 프레임을 전달받는다
	 * @author 홍성원
	 */
	public SignUpPanel signUpMain(JFrame mf) {
			
			JLabel loginLabel = new JLabel(new ImageIcon(gaipView));
			loginLabel.setLayout(null);
			loginLabel.setBounds(0, 0, 850, 600);
			
			JButton goBackButton = new JButton(new ImageIcon(goBackBt));
			JButton submitButton = new JButton(new ImageIcon(submit));
			
			goBackButton.setBounds(200, 430, 160, 50);
			submitButton.setBounds(490, 430, 160, 50);
			loginLabel.add(goBackButton);
			loginLabel.add(submitButton);
			
			Font font1 = new Font("",0,35);
			JTextField tf1 = new JTextField();
			tf1.setLocation(330, 90);
			tf1.setSize(250, 50);
			tf1.setFont(font1);
	
			JTextField tf2 = new JTextField();
			tf2.setLocation(330, 190);
			tf2.setSize(250, 50);
			tf2.setFont(font1);
			
			
			Font font3 = new Font("",0,35);
			JTextField tf3 = new JTextField();
			tf3.setLocation(330, 290);
			tf3.setSize(250, 50);
			tf3.setFont(font3);
			JLabel textLabel = new JLabel();
			thisPanel.add(tf1);
			thisPanel.add(tf2);
			thisPanel.add(tf3);
			this.add(loginLabel);
			
			submitButton.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					String memName = tf1.getText();
					String memEmail = tf2.getText();
					String acntId = tf3.getText();
					int result = 0;
					Map<String, String> inputData = new HashMap<>();
					LoginController loginController = new LoginController();
					
					/* 입력칸에서 입력받은 이름과 아이디, 이메일을 map에 각각 "memName", "acntId", "memEmail" 키 값으로 저장한다. */
					inputData.put("memName", memName);
					inputData.put("acntId", acntId);
					inputData.put("memEmail", memEmail);
					
					/* 만약 이름과, 이메일, 아이디 중 하나라도 입력이 안돼있다면  회원가입 실패창으로 이동한다. */
					if(!memName.isEmpty() || !memEmail.isEmpty() || !acntId.isEmpty()) {
						loginController.registAccountCheckInput(mf, thisPanel, inputData);
					}else {
						PanelSwitch.ChangePanel(mf, thisPanel, new SignUpPanel(mf).signUpFail(mf));
					}
				}
			});
			
			/* 뒤로가기 버튼을 누르면 게임의 초기화면으로 이동한다. */
			goBackButton.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
						PanelSwitch.ChangePanel(mf, thisPanel, new PokerunFirstView(mf));
					
				}
			});
			return thisPanel;
	}
	private void setup() {
		gaipView = new ImageIcon("images/회원가입화면.png").getImage().getScaledInstance(850, 600, 0);
		signUpFail = new ImageIcon("images/입력정보확인창(2).png").getImage().getScaledInstance(850, 600, 0);
		goBackBt = new ImageIcon("images/button/뒤로가기3.png").getImage().getScaledInstance(170, 50, 0);
		submit = new ImageIcon("images/button/확인버튼.png").getImage().getScaledInstance(160, 50, 0);
		this.setBounds(0, 0, 850, 600);
		this.setLayout(null);
		signUpPwdView = new ImageIcon("images/회원가입비밀번호입력.png").getImage().getScaledInstance(850, 600, 0);
		goBackBt = new ImageIcon("images/button/뒤로가기3.png").getImage().getScaledInstance(160, 50, 0);
		signbt = new ImageIcon("images/button/회원가입bt.png").getImage().getScaledInstance(130, 50, 0);
		pwd = new ImageIcon("images/비밀번호.png").getImage().getScaledInstance(150, 50, 0);
		
		
	}
}
