package com.greedy.pokemonrun.swingview;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class DisPlayOdoctorPanel extends JPanel{
	
	Image oDoctorView = new ImageIcon("images/shop/오박사 영상.gif").getImage().getScaledInstance(850, 600, 0); 
	
	public DisPlayOdoctorPanel(JFrame mf, int acntNum, int random) {
		
		this.setSize(850, 600);
		this.setLayout(null);
		
		DisPlayOdoctorPanel thisPanel = this;
		
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				JLabel oDoctorLabel = new JLabel(new ImageIcon(oDoctorView));
				oDoctorLabel.setBounds(0, 0, 850, 600);
				thisPanel.add(oDoctorLabel);
				thisPanel.repaint();
				thisPanel.revalidate();
				
				for(int i = 4; i >= 0; i--) {
					try {
						System.out.println(i);
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if(i == 0) {
						   PanelSwitch.ChangePanel(mf, thisPanel, new DrawResultPanel(mf, acntNum, random));
					}
				}
			}
		});
		
		t.start();

		mf.add(thisPanel);
		mf.repaint();
		mf.revalidate();
	}
}
