package com.greedy.pokemonrun.swingview;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.greedy.pokemonrun.controller.MainmenuController;

public class DrawPanel extends JPanel {

	// 피카츄 - 거북왕 - 이상해씨 - 고오스 - 리자몽 - 피존투 - 뮤 - 미뇽 - 푸린 - 팬텀

	Image drawView = new ImageIcon("images/shop/포켓몬뽑기메뉴화면.png").getImage().getScaledInstance(850, 600, 0);
	Image drawBt = new ImageIcon("images/shop/포켓몬 뽑기.png").getImage().getScaledInstance(100, 50, 0);
	Image goBackBt = new ImageIcon("images/button/뒤로가기버튼.png").getImage().getScaledInstance(100, 50, 0);
	Image curBall = new ImageIcon("images/shop/ct/현재 몬스터볼 그림.png").getImage().getScaledInstance(120, 50, 0);

	private MainmenuController mainmenuController;

	public DrawPanel(JFrame mf, int acntNum) {

		mainmenuController = new MainmenuController(acntNum);

		this.setSize(850, 600);
		this.setLayout(null);

		DrawPanel thisPanel = this;

		JLabel drawLabel = new JLabel(new ImageIcon(drawView));
		drawLabel.setBounds(0, 0, 850, 600);

		JLabel curBallLabel = new JLabel(new ImageIcon(curBall)); //
		curBallLabel.setBounds(650, 480, 120, 50); //

		String result = mainmenuController.selectCurBall(acntNum) + ""; //

		JTextField curBallText = new JTextField(result); //
		curBallText.setBounds(710, 490, 50, 30); //
		curBallText.setBackground(Color.orange); //
		curBallText.setFont(new Font("", Font.BOLD, 20)); //
		curBallText.setEditable(false);

		JButton button1 = new JButton(new ImageIcon(drawBt));
		JButton button2 = new JButton(new ImageIcon(goBackBt));
		button1.setBounds(362, 450, 100, 50);
		button2.setBounds(100, 500, 100, 50);

		button1.addMouseListener(new MouseAdapter() { // 뽑기

			@Override
			public void mouseClicked(MouseEvent e) {
				if (mainmenuController.selectCurBall(acntNum) >= 100) { // 돈이 충분한가
					int random = mainmenuController.randomDrawPokemon(acntNum); // 랜덤 포켓몬 뽑기
					PanelSwitch.ChangePanel(mf, thisPanel, new DisPlayOdoctorPanel(mf, acntNum, random)); // 뽑은 포켓몬 화면에 보여줌
				} else {
					PanelSwitch.ChangePanel(mf, thisPanel, new DrawFailPanel(mf, acntNum)); // 돈없다고 나옴
				}
			}
		});

		button2.addMouseListener(new MouseAdapter() { // 뒤로가기

			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new ShopPanel(mf, acntNum));
			}
		});
		drawLabel.add(curBallText);	
		drawLabel.add(button1);
		drawLabel.add(button2);
		drawLabel.add(curBallLabel);
		this.add(drawLabel);
		this.repaint();
		this.revalidate();

		mf.add(this);
		mf.repaint();
		mf.revalidate();
	}
}
