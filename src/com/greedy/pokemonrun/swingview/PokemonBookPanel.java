package com.greedy.pokemonrun.swingview;

import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.greedy.pokemonrun.controller.LoginController;
/**
 * <pre>
 * Class : PokemonBookPanel
 * Comment : 마이페이지에서 pokemon버튼을 눌렀을 때 볼 수 있는 포켓몬 도감 
 * History
 * 2021/12/19 (박휘림) 화면구현
 * 2021/12/19 (홍성원) DB연동
 * </pre>
 * @version 1
 * @author 박휘림 
 * @author 홍성원 
 * */
public class PokemonBookPanel extends JPanel{
	
	private Image book;
	private Image pikachu;
	private Image turtleKing;
	private Image leesanghaessi;
	private Image goos;
	private Image lizamong;
	private Image pigeon;
	private Image mew;
	private Image minyong;
	private Image purin;
	private Image phantom;
	private Image back;
	private PokemonBookPanel thisPanel;
	private String pikachus;
	private String turtleKings;
	private String leesanghaessis;
	private String gooss;
	private String lizamongs;
	private String pigeons;
	private String mews;
	private String minyongs;
	private String purins;
	private String phantoms;
	
	public PokemonBookPanel(JFrame mf) {
		setup();
		thisPanel = this;
	}

	/**
	 * 메소드 pokemonBook에 관한 문서화 주석
	 * Comment : 마이페이지에서 pokemon버튼을 눌렀을 때 볼 수 있는 포켓몬 도감이다. 
	 * 로그인한 회원이 보유한 포켓몬은 선명하게 움직이는 이미지로 출력되고, 미보유 포켓몬은 흐릿하게 출력된다.
	 * @param 로그인한 회원 계정 넘버
	 * @author 박휘림
	 * @author 홍성원
	 * */
	public JPanel pokemonBook(JFrame mf, int acntNum) {
		this.setBounds(0, 0, 850, 600);
		this.setLayout(null);
		
		
		JLabel pokemonBook = new JLabel(new ImageIcon(book));
		pokemonBook.setBounds(0, 0, 850, 600);
		/* pokemonCheckByNum메소드에서 로그인한 계정의 계정 넘버를 받아와 계정넘버와 지정한 포켓몬 넘버를 매개변수에 담아
		 * 해당 회원이 지정한 포켓몬을 가지고 있는지 결과값을 result1에 담아준다. */
		LoginController lc = new LoginController();
		int result1 = lc.pokemonCheckByNum(acntNum, 1);
		/* 보유하고 있는 포켓몬일 경우 선명하게 움직이는 이미지가 출력되고 미보유 포켓몬일 경우 흐릿한 이미지가 출력 */
		if(result1 > 0) {
			pikachus = "images/pikapika.gif"; /* 보유 */
		} else {
			pikachus = "images/pokemon/피카츄2.png"; /* 미보유 */		}
		setup();
		JLabel pikachupic = new JLabel(new ImageIcon(pikachu));
		pikachupic.setBounds(20, 90, 145, 145);
		
		/* 거북왕 */
		int result2 = lc.pokemonCheckByNum(acntNum, 2);
		/* 보유하고 있는 포켓몬일 경우 선명하게 움직이는 이미지가 출력되고 미보유 포켓몬일 경우 흐릿한 이미지가 출력 */
		if(result2 > 0) {
			turtleKings = "images/pokemon/거북왕.gif"; /* 보유 */
		} else {
			turtleKings = "images/pokemon/거북왕2.png"; /* 미보유 */
		}
		setup();
		JLabel turtleKingpic = new JLabel(new ImageIcon(turtleKing));
		turtleKingpic.setBounds(185, 90, 145, 145);
		
		/* 이상해씨 */
		int result3 = lc.pokemonCheckByNum(acntNum, 3);
		/* 보유하고 있는 포켓몬일 경우 선명하게 움직이는 이미지가 출력되고 미보유 포켓몬일 경우 흐릿한 이미지가 출력 */
		if(result3 > 0) {
			leesanghaessis = "images/pokemon/이상해씨.gif";
		} else {
			leesanghaessis = "images/pokemon/이상해씨2.png";
		}
		setup();
		JLabel leesanghaessipic = new JLabel(new ImageIcon(leesanghaessi));
		leesanghaessipic.setBounds(345, 90, 145, 145);
		
		
		/* 고오스 */
		int result4 = lc.pokemonCheckByNum(acntNum, 4);
		/* 보유하고 있는 포켓몬일 경우 선명하게 움직이는 이미지가 출력되고 미보유 포켓몬일 경우 흐릿한 이미지가 출력 */
		if(result4 > 0) {
			gooss = "images/pokemon/고오스.gif";
		} else {
			gooss = "images/pokemon/고오스2.png";
		}
		setup();
		JLabel goospic = new JLabel(new ImageIcon(goos));
		goospic.setBounds(505, 90, 145, 145);
		
		/* 리자몽 */
		int result5 = lc.pokemonCheckByNum(acntNum, 5);
		/* 보유하고 있는 포켓몬일 경우 선명하게 움직이는 이미지가 출력되고 미보유 포켓몬일 경우 흐릿한 이미지가 출력 */
		if(result5 > 0) {
			lizamongs = "images/pokemon/리자몽.gif";
		} else {
			lizamongs = "images/pokemon/리자몽2.png";
		}
		setup();
		JLabel lizamongpic = new JLabel(new ImageIcon(lizamong));
		lizamongpic.setBounds(665, 90, 145, 145);
		
		/* 피존투 */
		int result6 = lc.pokemonCheckByNum(acntNum, 6);
		/* 보유하고 있는 포켓몬일 경우 선명하게 움직이는 이미지가 출력되고 미보유 포켓몬일 경우 흐릿한 이미지가 출력 */
		if(result6 > 0) {
			pigeons = "images/pokemon/피존투.gif";
		} else {
			pigeons = "images/pokemon/피존투2.png";
		}
		setup();
		JLabel pigeonpic = new JLabel(new ImageIcon(pigeon));
		pigeonpic.setBounds(25, 300, 145, 145);
		
		/* 뮤 */
		int result7 = lc.pokemonCheckByNum(acntNum, 7);
		/* 보유하고 있는 포켓몬일 경우 선명하게 움직이는 이미지가 출력되고 미보유 포켓몬일 경우 흐릿한 이미지가 출력 */
		if(result7 > 0) {
			mews = "images/pokemon/뮤.gif";
		} else {
			mews = "images/pokemon/뮤2.png";
		}
		setup();
		JLabel mewpic = new JLabel(new ImageIcon(mew));
		mewpic.setBounds(185, 300, 145, 145);
		
		/* 미뇽 */
		int result8 = lc.pokemonCheckByNum(acntNum, 8);
		/* 보유하고 있는 포켓몬일 경우 선명하게 움직이는 이미지가 출력되고 미보유 포켓몬일 경우 흐릿한 이미지가 출력 */
		if(result8 > 0) {
			minyongs = "images/pokemon/미뇽2.gif";
		} else {
			minyongs = "images/pokemon/미뇽2.png";
		}
		setup();
		JLabel minyongpic = new JLabel(new ImageIcon(minyong));
		minyongpic.setBounds(345, 300, 145, 145);
		
		/* 푸린 */
		int result9 = lc.pokemonCheckByNum(acntNum, 9);
		/* 보유하고 있는 포켓몬일 경우 선명하게 움직이는 이미지가 출력되고 미보유 포켓몬일 경우 흐릿한 이미지가 출력 */
		if(result9 > 0) {
			purins = "images/pokemon/푸린.gif";
		} else {
			purins = "images/pokemon/푸린2.png";
		}
		setup();
		JLabel purinpic = new JLabel(new ImageIcon(purin));
		purinpic.setBounds(505, 300, 145, 145);
		
		/* 팬텀 */
		int result10 = lc.pokemonCheckByNum(acntNum, 10);
		/* 보유하고 있는 포켓몬일 경우 선명하게 움직이는 이미지가 출력되고 미보유 포켓몬일 경우 흐릿한 이미지가 출력 */
		if(result10 > 0) {
			phantoms = "images/pokemon/팬텀.gif";
		} else {
			phantoms = "images/pokemon/팬텀2.png";
		}
		setup();
		JLabel phantompic = new JLabel(new ImageIcon(phantom));
		phantompic.setBounds(665, 300, 145, 145);
		
		
		JButton goBack = new JButton(new ImageIcon(back));
		goBack.setBounds(340, 500, 150, 50);
		/* 뒤로가기 버튼을 누르면 다시 메인 메뉴로 이동 */
		goBack.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new MainmenuPanel(mf, acntNum));
			}
		});
		pokemonBook.add(pikachupic);
		pokemonBook.add(turtleKingpic);
		pokemonBook.add(leesanghaessipic);
		pokemonBook.add(goospic);
		pokemonBook.add(lizamongpic);
		pokemonBook.add(pigeonpic);
		pokemonBook.add(mewpic);
		pokemonBook.add(minyongpic);
		pokemonBook.add(purinpic);
		pokemonBook.add(phantompic);
		pokemonBook.add(goBack);
		thisPanel.add(pokemonBook);
		mf.add(thisPanel);
		return thisPanel;
	}
	
	private void setup() {
		book = new ImageIcon("images/book/도감뷰.png").getImage().getScaledInstance(850, 600, 0);
		pikachu = new ImageIcon(pikachus).getImage().getScaledInstance(145, 145, 0);
		turtleKing = new ImageIcon(turtleKings).getImage().getScaledInstance(145, 145, 0);
		leesanghaessi = new ImageIcon(leesanghaessis).getImage().getScaledInstance(145, 145, 0);
		goos = new ImageIcon(gooss).getImage().getScaledInstance(145, 145, 0);
		lizamong = new ImageIcon(lizamongs).getImage().getScaledInstance(145, 145, 0);
		pigeon = new ImageIcon(pigeons).getImage().getScaledInstance(145, 145, 0);
		mew = new ImageIcon(mews).getImage().getScaledInstance(145, 145, 0);
		minyong = new ImageIcon(minyongs).getImage().getScaledInstance(145, 145, 0);
		purin = new ImageIcon(purins).getImage().getScaledInstance(145, 145, 0);
		phantom = new ImageIcon(phantoms).getImage().getScaledInstance(145, 145, 0);
		back = new ImageIcon("images/button/backbt.png").getImage().getScaledInstance(150, 50, 0);
	}
}








