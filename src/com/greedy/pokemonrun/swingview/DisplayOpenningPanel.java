package com.greedy.pokemonrun.swingview;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class DisplayOpenningPanel extends JPanel{

	Image openningView = new ImageIcon("images/button/멋쟁이피카츄.gif").getImage().getScaledInstance(850, 600, 0); 
	
	public DisplayOpenningPanel(JFrame mf, int acntNum) {
		
		this.setSize(850, 600);
		this.setLayout(null);
		
		DisplayOpenningPanel thisPanel = this;
		
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				JLabel openningLabel = new JLabel(new ImageIcon(openningView));
				openningLabel.setBounds(0, 0, 850, 600);
				thisPanel.add(openningLabel);
				thisPanel.repaint();
				thisPanel.revalidate();
				
				for(int i = 6; i >= 0; i--) {
					try {
						System.out.println(i);
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if(i == 0) {
						   PanelSwitch.ChangePanel(mf, thisPanel, new MainmenuPanel(mf, acntNum));
					}
				}
			}
		});
		
		t.start();

		mf.add(thisPanel);
		mf.repaint();
		mf.revalidate();
	}
}
