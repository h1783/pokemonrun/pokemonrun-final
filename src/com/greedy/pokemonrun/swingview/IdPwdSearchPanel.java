package com.greedy.pokemonrun.swingview;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.greedy.pokemonrun.controller.LoginController;

/**
 * <pre>
 * Class : IdPwdSearchPanel
 * Comment : 시작화면에서 id/pwd찾기 기능을 출력하는 화면
 * History
 * 2021/12/16 (박휘림) 
 * 2021/12/19 (홍성원) DB 연동
 * </pre>
 * @version 1
 * @author 박휘림
 * @author 홍성원
 * */
public class IdPwdSearchPanel extends JPanel{
	
	private Image idpwdWhere;
	private Image idWhere;
	private Image pwdWhere;
	private Image backStartView;
	private IdPwdSearchPanel thisPanel;
	private Image idSearch;
	private Image back;
	private Image ok;
	private Image popUp;
	private Image error;
	private Image idResult;
	private Image pwdSearch;
	private Image emailCode;
	
	
	/* id찾기, pwd찾기, 뒤로가기 버튼 출력하는 곳*/
	
	
	public IdPwdSearchPanel(JFrame mf) {
		setup();
		thisPanel = this;
	}


	
	/**
	 * 메소드 idPwdSearchPanel에 관한 문서 주석
	 * Comment : 시작화면에서 id/pwd찾기 버튼을 눌렀을 때 출력되는 화면 
	 * @version 1
	 * @author 박휘림
	 * */
	/* 아이디 찾기, 비밀번호 찾기, 뒤로가기 버튼이 출력되는 화면 */
	public JPanel idPwdSearchPanel(JFrame mf) {
		JPanel jp = new JPanel();
		this.setBounds(0, 0, 850, 600);
		this.setLayout(null);
		
		JLabel idPwdWhere = new JLabel(new ImageIcon(idpwdWhere));
		idPwdWhere.setBounds(0, 0, 850, 600);
		/* 아이디 찾기, 비밀번호 찾기, 뒤로가기 버튼 */
		JButton whatId = new JButton(new ImageIcon(idWhere));
		JButton whatPwd = new JButton(new ImageIcon(pwdWhere));
		JButton backStart = new JButton(new ImageIcon(backStartView));
		whatId.setBounds(280, 150, 250, 60);
		whatPwd.setBounds(280, 270, 250, 60);
		backStart.setBounds(290, 390, 230, 60);
		
		/* 아이디 찾기를 눌렀을 때 아이디를 찾기 위해 회원 정보를 입력하는 창으로 이동 */
		whatId.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new IdPwdSearchPanel(mf).idSearchMainView(mf));
			}
		});
		
		/* 비밀번호 찾기를 눌렀을 때 비밀번호를 찾기 위해 회원 정보를 입력하는 창으로 이동 */
		whatPwd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new IdPwdSearchPanel(mf).pwdSearchMainView(mf));
			}
		});
		
		/* 뒤로가기 버튼을 누르면 처음 게임시작화면으로 이동 */
		backStart.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new PokerunFirstView(mf));
			}
		});
		
		idPwdWhere.add(whatId);
		idPwdWhere.add(whatPwd);
		idPwdWhere.add(backStart);
		this.add(idPwdWhere);
		
		mf.add(this);
		return this;
	}

	
	/**
	 * 메소드 idSearchMainView에 관한 문서화 주석
	 * Comment : 아이디 찾기를 위해 이름과 이메일을 입력하는 화면
	 * @author 박휘림
	 * @author 홍성원
	 * */
	public JPanel idSearchMainView(JFrame mf) {
		
		this.setSize(850,600);
		this.setLayout(null);
		
		JLabel idSearchLabel = new JLabel(new ImageIcon(idSearch));
		idSearchLabel.setBounds(0,0,850,600);
		/* 아이디를 찾기 위해 이름을 입력 */
		JTextField name = new JTextField();
		name.setBounds(300, 105, 250, 60);
		Font font1 = new Font("",0,30);
		name.setFont(font1);
		idSearchLabel.add(name);
		/* 아이디를 찾기 위해 이메일 입력 */
		JTextField email = new JTextField();
		email.setBounds(300 ,260, 250, 60);
		Font font2 = new Font("",0,30);
		email.setFont(font1);
		idSearchLabel.add(email);
		
		
		JButton okok = new JButton(new ImageIcon(ok));
		okok.setBounds(500,400,200,60);
		okok.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				/* 버튼을 누르면 입력창에 기입한 이름과 이메일을 컨트롤러에 보내, 회원존재여부를 판별한다. */
				LoginController loginController = new LoginController();
				String memName = name.getText();
				String memEmail = email.getText();
				Map<String, String> memInfo = new HashMap<String, String>();
				memInfo.put("memName", memName);
				memInfo.put("memEmail", memEmail);//
				loginController.findMemberId(mf, thisPanel, memInfo);
			}
		});
		
		JButton goBack = new JButton(new ImageIcon(back));
		goBack.setBounds(180,400,200,60);
		goBack.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new PokerunFirstView(mf));
			}
		});
		
		idSearchLabel.add(goBack);
		idSearchLabel.add(okok);
		this.add(idSearchLabel);
		mf.add(this);
		
		return thisPanel;
	}
	
	/**
	 * 메소드 idSearchSuccess에 관한 문서화 주석
	 * Comment : 아이디 찾기에서 회원 정보를 맞게 입력하면 이메일과 이름이 일치하는 회원의 아이디가 출력되는 화면
	 * @param 회원 아이디
	 * @author 박휘림
	 * @author 홍성원
	 * */
	public JPanel idSearchSuccess(JFrame mf, String memId) {
		this.setBounds(0, 0, 850, 600);
		this.setLayout(null);

		JLabel resultLabel = new JLabel(new ImageIcon(idResult));
		resultLabel.setBounds(0, 0, 850, 600);
		Font font1 = new Font("",2,40);
		JLabel text = new JLabel("ID는 [" + memId + "] 입니다!");
		text.setFont(font1);
		
		
		text.setBounds(200, 200, 400, 200);
		JButton okok = new JButton(new ImageIcon(ok));
		okok.setBounds(350 ,400 ,200 ,60);
		/* 확인버튼을 누르면 다시 로그인창으로 back*/
		okok.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new PokerunFirstView(mf));
			}
		});
		resultLabel.add(okok);
		resultLabel.add(text);
		this.add(resultLabel);
		mf.add(this);
		return thisPanel;
	}
	
	
	
	/**
	 * 메소드 idSearchFailed에 관한 문서화 주석
	 * Comment : 아이디 찾기에서 회원 정보를 잘못 입력시 출력되는 화면
	 * @author 박휘림
	 * */
	/* 아이디 찾기 실패시 출력될 화면*/
	public JPanel idSearchFailed(JFrame mf) {
		/* id찾기 실패 판넬 -> 실패하면 아이디/비밀번호찾기 메뉴로 돌아감*/
		this.setBounds(0, 0, 850, 600);
		this.setLayout(null);
		
		JLabel popLabel = new JLabel(new ImageIcon(popUp));
		popLabel.setBounds(0, 0, 850, 600);
		
		JButton errButton = new JButton(new ImageIcon(error));
		errButton.setBounds(130, 150, 600, 120);
		errButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new IdPwdSearchPanel(mf).idSearchMainView(mf));
			}
		});
		
		popLabel.add(errButton);
		this.add(popLabel);
		mf.add(this);
		
		return thisPanel;
	}
	
	
	/**
	 * 메소드 pwdSearchMainView에 관한 문서화 주석
	 * Comment : 비밀번호를 찾기 위해 회원정보(이름, 이메일, 아이디)를 입력하는 창
	 * @author 박휘림
	 * @author 홍성원
	 * */
	/* 비번을 찾기 위한 정보 입력창 */
	public JPanel pwdSearchMainView(JFrame mf) {
	      
		this.setSize(850, 600);
		this.setLayout(null);
		Font font1 = new Font("",0,30);
	      
	    JLabel backLabel = new JLabel(new ImageIcon(pwdSearch));
	    backLabel.setBounds(0, 0, 850, 600);
	      
	    JLabel nameLabel = new JLabel();
	    nameLabel.setBounds(300, 100, 250, 60);
	    backLabel.add(nameLabel);
	    JTextField name = new JTextField();
	    name.setBounds(300, 100, 250, 60);

	    backLabel.add(name);
	    
	    JLabel emailLabel = new JLabel();
	    emailLabel.setBounds(300, 200, 250, 60);
	    backLabel.add(emailLabel);
	    JTextField email = new JTextField();
	    email.setBounds(300, 220, 250, 60);
	    backLabel.add(email);
	     
	    JLabel idLabel = new JLabel();
	    idLabel.setBounds(300, 300, 250, 60);
	    backLabel.add(idLabel);
	    JTextField id = new JTextField();
	    id.setBounds(300, 340, 250, 60);
	    backLabel.add(id);

	    name.setFont(font1);
	    email.setFont(font1);
	    id.setFont(font1);
	    
	    JButton goback2 = new JButton(new ImageIcon(back));
	    JButton okok2 = new JButton(new ImageIcon(ok));
	    goback2.setBounds(180 ,430 ,200 ,60);
	    okok2.setBounds(500 ,430 ,200 ,60);
	    
	    /* 정보를 입력한 후 ok 버튼을 누르면 */
	    okok2.addMouseListener(new MouseAdapter() {
	    	@Override
	        public void mouseClicked(MouseEvent e) {
	    		LoginController loginController = new LoginController();
	            String memName = name.getText();
	            String acntId = id.getText();
	            String memEmail = email.getText();
	            Map<String, String> memInfo = new HashMap<String, String>();
	            memInfo.put("memName", memName);
	            memInfo.put("memEmail", memEmail);
	            memInfo.put("acntId", acntId);
	            loginController.findMemberPwd(mf, thisPanel, memInfo);
	    	}
	    });
	      
	    backLabel.add(goback2);
	    backLabel.add(okok2);
	    this.add(backLabel);
	    mf.add(this);

	    return thisPanel;
	}
	
	/**
	 * 메소드 pwdSearchSuccess에 관한 문서화 주석
	 * Comment : 비밀번호 찾기 성공 시 출력되는 창
	 * @param 회원 비밀번호
	 * @author 박휘림
	 * */   
	public JPanel pwdSearchSuccess(JFrame mf, String pwd) {
	      
		this.setSize(850, 600);
	    this.setLayout(null);

	    JLabel backLabel = new JLabel(new ImageIcon(emailCode));
	    backLabel.setBounds(0, 0, 850, 600);
	    JLabel text = new JLabel("비밀번호 : " + pwd);
	    text.setBounds(200,200,400,300);
	    Font f = new Font("", 2 , 40);
	    text.setFont(f);
	      
	    JButton okok = new JButton(new ImageIcon(ok));
	      
	    okok.setBounds(400, 400, 100, 50);
	      
	    okok.addMouseListener(new MouseAdapter() {
	    	@Override
	        public void mouseClicked(MouseEvent e) {
	    		PanelSwitch.ChangePanel(mf, thisPanel, new PokerunFirstView(mf));
	        }//비번찾고 다시 초기화면으로 넘어가서 로그인하면 됨
	    });
	      
	    backLabel.add(text);
	    backLabel.add(okok);
	    this.add(backLabel);
	    mf.add(this);
	      
	    return thisPanel;
	}
	
	/**
	 * 메소드 pwdSearchFailed에 관한 문서화 주석
	 * Comment : 비밀번호 찾기 실패 시 출력되는 화면
	 * @author 박휘림
	 * */
	/* 비번 찾기 실패 시 출력될 화면 */
	public JPanel pwdSearchFailed(JFrame mf) {
		this.setBounds(0, 0, 850, 600);
	    this.setLayout(null);
	      
	    JLabel popLabel = new JLabel(new ImageIcon(popUp));
	    popLabel.setBounds(0, 0, 850, 600);
	    this.add(popLabel);
	    
	    JButton errButton = new JButton(new ImageIcon(error));
	    errButton.setBounds(130, 150, 600, 120);
	    this.add(errButton);
	    errButton.addMouseListener(new MouseAdapter() {
	    	@Override
	        public void mouseClicked(MouseEvent e) {
	    		PanelSwitch.ChangePanel(mf, thisPanel, new IdPwdSearchPanel(mf).pwdSearchMainView(mf));
	    	}// 다시 시작화면으로 이동
	    });
	      
	    mf.add(this);
	      
	    return thisPanel;
	}
	
	private void setup() {
		idpwdWhere = new ImageIcon("images/로그인창.png").getImage().getScaledInstance(850, 600, 0);
		idWhere = new ImageIcon("images/button/ID찾기.png").getImage().getScaledInstance(250, 60, 0);
		idSearch = new ImageIcon("images/shop/아이디찾기.png").getImage().getScaledInstance(850,600,0);
		idResult = new ImageIcon("images/로그인창.png").getImage().getScaledInstance(850, 600, 0);
		backStartView = new ImageIcon("images/button/뒤로가기.png").getImage().getScaledInstance(250, 60, 0);
		back = new ImageIcon("images/button/뒤로가기.png").getImage().getScaledInstance(200, 60, 0);
		error = new ImageIcon("images/shop/입력정보를 확인하세요.png").getImage().getScaledInstance(400, 100, 0);
		pwdWhere = new ImageIcon("images/button/비밀번호찾기.png").getImage().getScaledInstance(250, 60, 0);
		pwdSearch = new ImageIcon("images/shop/비번찾기정보입력.png").getImage().getScaledInstance(850, 600, 0);
	    popUp = new ImageIcon("images/shop/입력정보확인에러.png").getImage().getScaledInstance(850, 600, 0);
	    back = new ImageIcon("images/button/뒤로가기.png").getImage().getScaledInstance(200, 60, 0);
	    emailCode = new ImageIcon("images/로그인창.png").getImage().getScaledInstance(850, 600, 0);
	    ok = new ImageIcon("images/button/확인.png").getImage().getScaledInstance(200, 60, 0);
	}
}
