package com.greedy.pokemonrun.swingview;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.greedy.pokemonrun.controller.InGameController;
import com.greedy.pokemonrun.ingame.InGame;

/**
 * <pre>
 * Class : ScorePanel
 * Comment : Player들의 iD와 점수를 불러와 5등까지 순위를 뽑아준다
 * History 
 * 21/12/14 (구훈모)시작
 * 21/12/19 (구훈모)완료
 * </pre>
 * @author 구훈모
 * */
public class ScorePanel extends JPanel {
	
	InGameController inGameController = null;
	Map<String, Integer> map = null;
	Image resultView = new ImageIcon("images/게임결과화면.png").getImage().getScaledInstance(850, 600, 0);
	Image restartBt = new ImageIcon("images/button/재시작.png").getImage().getScaledInstance(150, 50, 0);
	Image goMenuBt = new ImageIcon("images/button/메뉴로.png").getImage().getScaledInstance(150, 50, 0);
	
	/**
	 * Comment : 게임 종료시 회원 아이디와 점수, 얻은 몬스터볼을 넘겨 게임내역에 업데이트 하는 메소드
	 * @author 구훈모
	 * @param mf : 이 판넬을 얹을 프레임
	 * @param acntNum : 플레이한 회원 번호
	 * @param ballPoint : 게임 진행 동안 얻은 몬스터볼 갯수
	 * */
	public ScorePanel(JFrame mf, int acntNum, int ballPoint) {
		
		this.setSize(850, 600);
		this.setLayout(null);
		
		JLabel viewLabel = new JLabel(new ImageIcon(resultView));
		viewLabel.setBounds(0, 0, 850, 600);

		JPanel thisPanel = this;
		
		/* 게임 종료후 재시작 버튼을 누르면 게임 내역에 점수 등록 */
		JButton restart = new JButton(new ImageIcon(restartBt));
		restart.setBounds(150, 450, 150, 50);
		
		/* 매개변수 값들을 가지고 게임내역에 정보를 추가해주는 매소드를 호출. */
		restart.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				inGameController = new InGameController();
				map = new HashMap<>();
				map.put("acntNum", acntNum);
				map.put("ballPoint", ballPoint);
				inGameController.insertGameHistory(map);
				PanelSwitch.ChangePanel(mf, thisPanel, new InGame().runGame(mf, acntNum));
				
			}
		});
		
		/* 게임 종료 후 메인메뉴로 가기 누르면 게임 내역에 점수 등록 */
		JButton goMenu = new JButton(new ImageIcon(goMenuBt));
		goMenu.setBounds(550, 450, 150, 50);
		goMenu.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				inGameController = new InGameController();
				map = new HashMap<>();
				map.put("acntNum", acntNum);
				map.put("ballPoint", ballPoint);
				inGameController.insertGameHistory(map);
				PanelSwitch.ChangePanel(mf, thisPanel, new MainmenuPanel((JFrame) mf, acntNum));
			}
		});
		
		/* 최종 스코어를 알 수 있는 텍스트필드 */
		JTextField score = new JTextField(":" + (ballPoint * 100));
		score.setBackground(Color.DARK_GRAY);
	    score.setFont(new Font("", Font.BOLD, 20));
	    score.setBounds(250, 360, 120, 50);
	    
	    /* 얻은 몬스터볼을 알 수 있는 텍스트필드 */
	    JTextField ball = new JTextField(":" + ballPoint);
		ball.setBackground(Color.DARK_GRAY);
	    ball.setFont(new Font("", Font.BOLD, 20));
	    ball.setBounds(600, 360, 120, 50);
		
	    viewLabel.add(ball);
	    viewLabel.add(score);
		viewLabel.add(restart);
		viewLabel.add(goMenu);
		
		this.add(viewLabel);
		mf.add(thisPanel);
		System.out.println("회원번호 : " + acntNum + "점수 : " + ballPoint);
		
	}
}
