package com.greedy.pokemonrun.swingview;

import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class DuplDrawPokemonPanel extends JPanel { // 중복된 포켓몬을 뽑알을 때 패널

	Image resultFailView = new ImageIcon("images/shop/중복된포켓몬뽑기결과.png").getImage().getScaledInstance(850, 600, 0);
	Image okBt = new ImageIcon("images/button/확인.png").getImage().getScaledInstance(80, 50, 0);

	public DuplDrawPokemonPanel(JFrame mf, int acntNum) {

		this.setSize(850, 600);
		this.setLayout(null);

		DuplDrawPokemonPanel thisPanel = this;

		JLabel resultLabel = new JLabel(new ImageIcon(resultFailView));
		resultLabel.setBounds(0, 0, 850, 600);

		JButton button = new JButton(new ImageIcon(okBt));
		button.setBounds(425, 500, 80, 50);

		button.addMouseListener(new MouseAdapter() { // 뽑기

			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new DrawPanel(mf, acntNum));
			}
		});

		this.add(resultLabel);
		this.add(button);
		this.repaint();
		this.revalidate();

		mf.add(this);
		mf.repaint();
		mf.revalidate();
	}
}
