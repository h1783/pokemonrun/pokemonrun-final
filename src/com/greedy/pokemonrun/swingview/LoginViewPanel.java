package com.greedy.pokemonrun.swingview;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.greedy.pokemonrun.controller.LoginController;

/** 
 * <pre>
 * Class : LoginViewPanel
 * Comment : 초기화면에서 로그인 버튼을 누르면 출력되는 로그인 기능의 화면
 * History
 * 2021/12/16 (홍성원) 작성
 * </pre>
 * @version 1
 * @author 홍성원
 * */
public class LoginViewPanel extends JPanel {

   private Image loginView;
   private Image loginFailView;
   private Image goBackBt;
   private Image loginBt;
   private Image id;
   private Image pwd;
   private LoginViewPanel thisPanel;

   /* 로그인뷰에 프레임을 담아 생성자를 호출하면 이 클래스에서 쓸 데이터들을 생성해주는 기능을 수행한다 */
   public LoginViewPanel(JFrame mf) {
	   setup();
	   thisPanel = this;
   }

   /** loginMainView : 로그인 메인화면 출력을 담당하는 메소드
	 * 
	 * @param mf : 패널이 담길 메인 프레임을 전달받는다
	 * @return : 패널교체를 할때 삭제할 자기자신의 주소를 반환한다.
	 * @author 홍성원
	 * */
   public JPanel loginMainView(JFrame mf) {

	   this.setBounds(0, 0, 850, 600);;
	   this.setLayout(null);
	   /* 로그인화면에 아이디 비밀번호입력칸 글자의 크기를 조절하기위한 Font클래스를 생성 */
	   Font font = new Font("",0,30);
	   
	   /* 로그인화면의 배경을 생성 */
	   JLabel loginLabel = new JLabel(new ImageIcon(loginView));
	   loginLabel.setBounds(0, 0, 850, 600);
	
	   /* "아이디"글자를 출력하는 라밸 생성 */
	   JLabel idLabel = new JLabel(new ImageIcon(id));
	   idLabel.setBounds(200, 100, 130, 50);
	   /* 아이디를 입력받을 입력칸 생성 */
	   JTextField idText = new JTextField();
	   idText.setBounds(350, 100, 200, 50);					//아이디입력칸의 크기와 위치 설정
	   idText.setFont(font);								//아이디입력칸의 글자크기 설정
	
	   /* "비밀번호" 글자를 출력하는 라벨 생성*/
	   JLabel pwdLabel = new JLabel(new ImageIcon(pwd));		
	   pwdLabel.setBounds(180, 230, 150, 50);							//"비밀번호"글자의 위치를 지정
	   
	   /* 비밀번호 입력을 받기위한 입력창 생성 
	    * 비밀번호 입력시 별표(*)로 출력되게 JPasswordField 클래스로 생성 */
	   JPasswordField pwdText = new JPasswordField();		
	   pwdText.setBounds(350, 230, 200, 50);							//비밀번호 입력칸 크기와 위치 설정
	   pwdText.setFont(font);											//비밀번호 입력칸 글자크기 설정
	  
	   /* 아이디, 비밀번호입력칸을 다 작성했을때 , 로그인을 하기위해 누르는 "로그인" 버튼 생성 */
	   JButton loginButton = new JButton(new ImageIcon(loginBt));	
	   loginButton.setBounds(500, 400, 120, 50);						//로그인버튼 크기와 위치 설정
	   
	   /* 로그인버튼을 클릭했을때 기능을 수행하기위한 이벤트 처리 */
	   loginButton.addMouseListener(new MouseAdapter() {
		   @Override
		   public void mouseClicked(MouseEvent e) {
			   
			   
			   LoginController loginController = new LoginController();			//로그인 컨트롤러를 호출하기위해 선언 후 생성한다.
			   Map<String, String> loginInfo = new HashMap<String, String>();	//컨트롤러에 ID와 PWD를 전달하기위해 Map을 선언 후 생성한다.
		       String acntId = idText.getText();								//아이디입력칸에 기입된 문자열을 저장한다
		       String acntPwd = pwdText.getText();								//비밀번호입력칸에 기입된 문자열을 저장한다
		       loginInfo.put("acntId", acntId);									//위에 생성한 Map에 ID를 저장한다
		       loginInfo.put("acntPwd", acntPwd);								//위에 생성한 Map에 PWD를 저장한다.
		       
		       /* 아이디와 비밀번호를 담은 Map자료형 loginInfo를 컨트롤러의 login메소드로 전달인자로 주며 호출한다. */
		       loginController.login(mf, thisPanel, loginInfo);					
	     }
	  });
      
      /* 뒤로가기버튼. 이벤트처리를 줘서, 클릭을 하면 초기화면으로 이동한다. */
      JButton goBackButton = new JButton(new ImageIcon(goBackBt));
      goBackButton.setBounds(200, 400, 130, 50);
      goBackButton.addMouseListener(new MouseAdapter() {
         @Override
         public void mouseClicked(MouseEvent e) {
            PanelSwitch.ChangePanel(mf, thisPanel, new PokerunFirstView(mf));
         }
      });

      /* 로그인배경과 버튼, 입력부분을 라벨에 담은 뒤, 패널에 담고, 그 패널을 전달받은 프레임에 담는 부분 */
      loginLabel.add(pwdLabel);
      loginLabel.add(pwdText);
      loginLabel.add(idLabel);
      loginLabel.add(idText);
      loginLabel.add(goBackButton);
      loginLabel.add(loginButton);
      this.add(loginLabel);
      mf.add(this);
      
      return this;
   }

   /** loginFailed : 로그인중 실패화면 출력을 담당하는 메소드
	 * 
	 * @param mf : 실패화면 패널이 담길 메인 프레임을 전달받는다
	 * @return : 패널교체를 할때 삭제할 자기자신의 주소를 반환한다.
	 * @author 홍성원
	 * */
   public JPanel loginFailed(JFrame mf) {
      thisPanel.setBounds(0, 0, 850, 600);;
      thisPanel.setLayout(null);
      
      /* 로그인 실패화면의 배경과,문구를 패널에 올리는 부분  */
      JLabel loginLabel = new JLabel(new ImageIcon(loginView));
      loginLabel.setBounds(0, 0, 850, 600);
      Font font1 = new Font("",2,40);
      JLabel text = new JLabel("입력정보를 확인해주세요~!");
      text.setFont(font1);
      text.setBounds(200, 200, 400, 200);
      loginLabel.add(text);
      thisPanel.add(loginLabel);
      
      /* 뒤로가기버튼. 이벤트처리를 설정해, 버튼을 클릭하면 다시 로그인정보 입력 창으로 이동한다. */
      JButton goBackButton = new JButton(new ImageIcon(goBackBt));
      goBackButton.setBounds(350, 400, 150, 50);
      goBackButton.addMouseListener(new MouseAdapter() {
         @Override
         public void mouseClicked(MouseEvent e) {
            PanelSwitch.ChangePanel(mf, thisPanel, new LoginViewPanel(mf).loginMainView(mf));
         }
      });
      
      thisPanel.add(goBackButton);
      mf.add(this);
      return thisPanel;
   }
      
   /** setup : 로그인기능의 메소드들이 사용하는 이미지를 설정해주는 메소드
	 * 
	 * @author 홍성원
	 * */
   private void setup() {
	      loginView = new ImageIcon("images/로그인창.png").getImage().getScaledInstance(850, 600, 0);
	      goBackBt = new ImageIcon("images/button/뒤로가기3.png").getImage().getScaledInstance(160, 50, 0);
	      loginBt = new ImageIcon("images/button/로그인.png").getImage().getScaledInstance(130, 50, 0);
	      id = new ImageIcon("images/아이디.png").getImage().getScaledInstance(130, 50, 0);
	      pwd = new ImageIcon("images/비밀번호.png").getImage().getScaledInstance(150, 50, 0);
	      loginFailView = new ImageIcon("images/로그인(실패).png").getImage().getScaledInstance(700, 500, 0);      
	   }      
}
