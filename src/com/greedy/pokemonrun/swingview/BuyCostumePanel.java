package com.greedy.pokemonrun.swingview;

import java.awt.Color;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.greedy.pokemonrun.controller.MainmenuController;

public class BuyCostumePanel extends JPanel {

	Image buyCostumeView = new ImageIcon("images/shop/ct/코스튬배경.png").getImage().getScaledInstance(850, 600, 0);
	Image buyBt = new ImageIcon("images/shop/ct/구매버튼.png").getImage().getScaledInstance(100, 50, 0);
	Image okBuyBt = new ImageIcon("images/shop/ct/구매완료.png").getImage().getScaledInstance(100, 50, 0);
	Image goBackBt = new ImageIcon("images/button/뒤로가기버튼.png").getImage().getScaledInstance(100, 50, 0);
	Image onScarf = new ImageIcon("images/costume/스카프.png").getImage().getScaledInstance(200, 200, 0);
	Image offScarf = new ImageIcon("images/shop/ct/스카프구매완료.png").getImage().getScaledInstance(200, 200, 0);
	Image onCap = new ImageIcon("images/costume/모자.png").getImage().getScaledInstance(200, 200, 0);
	Image offCap = new ImageIcon("images/shop/ct/모자구매완료.png").getImage().getScaledInstance(200, 200, 0);
	Image scarfPrice = new ImageIcon("images/shop/ct/100볼.png").getImage().getScaledInstance(120, 50, 0);
	Image capPrice = new ImageIcon("images/shop/ct/120볼.png").getImage().getScaledInstance(120, 50, 0);
	Image curBall = new ImageIcon("images/shop/ct/현재 몬스터볼 그림.png").getImage().getScaledInstance(120, 50, 0);

	private MainmenuController mainmenuController;

	public BuyCostumePanel(JFrame mf, int acntNum) {

		mainmenuController = new MainmenuController(acntNum);

		this.setSize(850, 600);
		this.setLayout(null);

		BuyCostumePanel thisPanel = this;

		JLabel buyCostumeLabel = new JLabel(new ImageIcon(buyCostumeView));
		buyCostumeLabel.setBounds(0, 0, 850, 600);

		JLabel curBallLabel = new JLabel(new ImageIcon(curBall)); //
		curBallLabel.setBounds(650, 480, 120, 50); //

		String result = mainmenuController.selectCurBall(acntNum) + ""; //

		JTextField curBallText = new JTextField(result); //
		curBallText.setBounds(710, 490, 50, 30); //
		curBallText.setBackground(Color.orange); //
		curBallText.setFont(new Font("", Font.BOLD, 20)); //
		curBallText.setEditable(false); //

		JLabel costume1 = costumeImage(acntNum, 1); // 스카프
		JLabel price1 = new JLabel(new ImageIcon(scarfPrice));
		costume1.setBounds(50, 50, 280, 280);
		price1.setBounds(120, 300, 120, 50);

		JLabel costume2 = costumeImage(acntNum, 2); // 모자
		JLabel price2 = new JLabel(new ImageIcon(capPrice));
		costume2.setBounds(260, 60, 300, 260);
		price2.setBounds(350, 300, 120, 50);

		JButton button1 = buttonImage(mf, thisPanel, acntNum, 1);
		JButton button2 = buttonImage(mf, thisPanel, acntNum, 2);
		JButton button3 = new JButton(new ImageIcon(goBackBt));
		button1.setBounds(135, 380, 100, 50);
		button2.setBounds(360, 380, 100, 50);
		button3.setBounds(100, 500, 100, 50);

		button3.addMouseListener(new MouseAdapter() { // 뒤로가기

			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new ShopPanel(mf, acntNum));
			}
		});
		buyCostumeLabel.add(curBallText);
		buyCostumeLabel.add(button1);
		buyCostumeLabel.add(button2);
		buyCostumeLabel.add(button3);
		buyCostumeLabel.add(costume1);
		buyCostumeLabel.add(price1);
		buyCostumeLabel.add(costume2);
		buyCostumeLabel.add(price2);
		buyCostumeLabel.add(curBallLabel);	
		thisPanel.add(buyCostumeLabel);
		this.repaint();
		this.revalidate();
		mf.add(this);
		mf.repaint();
		mf.revalidate();
	}

	private JLabel costumeImage(int acntNum, int costumeNum) {

		mainmenuController = new MainmenuController(acntNum);

		JLabel costumeLabel = new JLabel(new ImageIcon());

		if (costumeNum == 1) {

			int result = mainmenuController.selectGetCostume(acntNum, costumeNum); // 1이면 가지고 있음
			if (result != 1) {
				costumeLabel = new JLabel(new ImageIcon(onScarf));
			} else {
				costumeLabel = new JLabel(new ImageIcon(offScarf));
			}
		} else if (costumeNum == 2) {

			int result = mainmenuController.selectGetCostume(acntNum, costumeNum);

			if (result != 1) {
				costumeLabel = new JLabel(new ImageIcon(onCap));
			} else {
				costumeLabel = new JLabel(new ImageIcon(offCap));
			}
		}
		return costumeLabel;
	}

	private JButton buttonImage(JFrame mf, JPanel thisPanel, int acntNum, int costumeNum) {

		mainmenuController = new MainmenuController(acntNum);

		JButton buyButton = new JButton(new ImageIcon());

		Dialog dl = new Dialog(mf, "구매완료");
		dl.setBounds(425, 300, 300, 300);

		if (costumeNum == 1) {

			int result = mainmenuController.selectGetCostume(acntNum, costumeNum); // 1이면 가지고 있음
			if (result != 1) {
				buyButton = new JButton(new ImageIcon(buyBt));
				buyButton.addMouseListener(new MouseAdapter() { // 스카프 구매

					@Override
					public void mouseClicked(MouseEvent e) {
						int costumeNum = 1; // 코스튬넘버
						int curBall = mainmenuController.selectCurBall(acntNum); // 현재 몬스터볼 수
						int price = mainmenuController.selectCostumePrice(costumeNum); // 선택한 코스튬 가격

						if (curBall >= price) {// 구매가능 여부
							mainmenuController.buyCostume(acntNum, costumeNum); // 구매
							PanelSwitch.ChangePanel(mf, thisPanel, new BuyCostumeSucessPanel(mf, acntNum)); // 구매완료
						} else {
							PanelSwitch.ChangePanel(mf, thisPanel, new BuyCostumeFailPanel(mf, acntNum)); // 몬스터볼이 부족합니다.
						}

					}
				});
			} else {
				buyButton = new JButton(new ImageIcon(okBuyBt));
			}
		} else if (costumeNum == 2) {

			int result = mainmenuController.selectGetCostume(acntNum, costumeNum);

			if (result != 1) {
				buyButton = new JButton(new ImageIcon(buyBt));
				buyButton.addMouseListener(new MouseAdapter() { // 스카프 구매

					@Override
					public void mouseClicked(MouseEvent e) {
						int costumeNum = 2; // 코스튬넘버 ////////////////////////
						int curBall = mainmenuController.selectCurBall(acntNum); // 현재 몬스터볼 수
						int price = mainmenuController.selectCostumePrice(costumeNum); // 선택한 코스튬 가격

						if (curBall >= price) {// 구매가능 여부
							mainmenuController.buyCostume(acntNum, costumeNum);
							PanelSwitch.ChangePanel(mf, thisPanel, new BuyCostumeSucessPanel(mf, acntNum)); // 구매완료
						} else {
							PanelSwitch.ChangePanel(mf, thisPanel, new BuyCostumeFailPanel(mf, acntNum)); // 몬스터볼이
																											// 부족합니다.
						}

					}
				});
			} else {
				buyButton = new JButton(new ImageIcon(okBuyBt));
			}
		}
		return buyButton;
	}
}
