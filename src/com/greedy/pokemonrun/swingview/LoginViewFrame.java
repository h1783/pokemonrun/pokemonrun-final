package com.greedy.pokemonrun.swingview;

import javax.swing.JFrame;

/** 
 * <pre>
 * Class : LoginViewFrame
 * Comment : 포켓몬런 게임의 화면을 프로그램 종료때 까지 담고이을 메인 프레임
 * History
 * 2021/12/15 (홍성원) 작성
 * </pre>
 * @version 1
 * @author 홍성원
 * */
public class LoginViewFrame extends JFrame{

	public LoginViewFrame() {
		
		this.setSize(850, 600);
		this.setTitle("PokemonRun");
		this.setLayout(null);
		setResizable(false);
		setLocationRelativeTo(null);
		
		new PokerunFirstView(this);
		
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	}
}