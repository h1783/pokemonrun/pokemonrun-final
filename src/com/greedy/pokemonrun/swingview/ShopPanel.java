package com.greedy.pokemonrun.swingview;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.w3c.dom.css.RGBColor;

import com.greedy.pokemonrun.controller.MainmenuController;
import com.greedy.pokemonrun.swingview.LoginViewFrame;
import com.greedy.pokemonrun.swingview.PanelSwitch;

public class ShopPanel extends JPanel{
	
	Image shopView = new ImageIcon("images/shop/엘리니아.png").getImage().getScaledInstance(1000, 600, 0);
	Image drawPokemonBt = new ImageIcon("images/shop/포켓몬 뽑기.png").getImage().getScaledInstance(100, 40, 0);
	Image buyCostumeBt = new ImageIcon("images/shop/코스튬 구매.png").getImage().getScaledInstance(100, 40, 0);
	Image goBackBt = new ImageIcon("images/button/뒤로가기버튼.png").getImage().getScaledInstance(100, 50, 0);		
	Image logo = new ImageIcon("images/shop/로고.png").getImage().getScaledInstance(350, 180, 0);
	Image curBall = new ImageIcon("images/shop/ct/현재 몬스터볼 그림.png").getImage().getScaledInstance(120, 50, 0);
	Image pokemon1 = new ImageIcon("images/shop/피카츄풍선.gif").getImage().getScaledInstance(100, 100, 0);
	Image pokemon2 = new ImageIcon("images/pokemon/움직이는포켓몬1.gif").getImage().getScaledInstance(100, 100, 0);
	Image pokemon3 = new ImageIcon("images/shop/이상해꽃.gif").getImage().getScaledInstance(150, 150, 0);
	Image pokemon4 = new ImageIcon("images/shop/독침붕.gif").getImage().getScaledInstance(100, 100, 0);
	Image pokemon5 = new ImageIcon("images/shop/뿔충이.gif").getImage().getScaledInstance(60, 60, 0);
	Image pokemon6 = new ImageIcon("images/shop/버터풀.gif").getImage().getScaledInstance(100, 100, 0);
	
	
	
	
	
	public ShopPanel(JFrame mf, int acntNum) {
		
		MainmenuController mainmenuController = new MainmenuController(acntNum);
		
		this.setSize(850,600);
		this.setLayout(null);
		
		ShopPanel thisPanel = this;
		
		JLabel shopLabel = new JLabel(new ImageIcon(shopView));
		JLabel logoLabel = new JLabel(new ImageIcon(logo));
		JLabel pokemonLabel1 = new JLabel(new ImageIcon(pokemon1));
		JLabel pokemonLabel2 = new JLabel(new ImageIcon(pokemon2));
		JLabel pokemonLabel3 = new JLabel(new ImageIcon(pokemon3));
		JLabel pokemonLabel4 = new JLabel(new ImageIcon(pokemon4));
		JLabel pokemonLabel5 = new JLabel(new ImageIcon(pokemon5));
		JLabel pokemonLabel6 = new JLabel(new ImageIcon(pokemon6));
		JLabel curBallLabel = new JLabel(new ImageIcon(curBall));
		
		shopLabel.setBounds(0, 0, 850, 600);
		logoLabel.setBounds(30, 30, 350, 180);
		pokemonLabel1.setBounds(250, 300, 100, 100);
		pokemonLabel2.setBounds(450, 130, 100, 100);
		pokemonLabel3.setBounds(45, 315, 150, 150);
		pokemonLabel4.setBounds(300, 250, 100, 100);
		pokemonLabel5.setBounds(410, 500, 60, 60);
		pokemonLabel6.setBounds(550, 300, 100, 100);
		curBallLabel.setBounds(650, 480, 120, 50);
		
		String result = mainmenuController.selectCurBall(acntNum) +"";
		
		JTextField curBallText = new JTextField(result);
		curBallText.setBounds(710, 490, 50, 30);
		curBallText.setBackground(Color.orange);
		curBallText.setFont(new Font("", Font.BOLD, 20));
		curBallText.setEditable(false);
		
		JButton button1 = new JButton(new ImageIcon(drawPokemonBt));
		JButton button2 = new JButton(new ImageIcon(buyCostumeBt));
		JButton button3 = new JButton(new ImageIcon(goBackBt));
		button1.setBounds(650, 160, 100, 40);
		button2.setBounds(650, 230, 100, 40);
		button3.setBounds(650, 300, 100, 50);
		
		button1.addMouseListener(new MouseAdapter() {	//뻡기
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new DisplayRandomPokemonPanel(mf, acntNum));
			}
		});
		
		button2.addMouseListener(new MouseAdapter() {	//커스텸 구매
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new BuyCostumePanel(mf, acntNum));
			}
		});
		
		button3.addMouseListener(new MouseAdapter() {	//빽
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new MainmenuPanel(mf, acntNum));
			}
		});
		shopLabel.add(curBallText);
		shopLabel.add(logoLabel);
		shopLabel.add(pokemonLabel4);
		shopLabel.add(pokemonLabel1);
		shopLabel.add(pokemonLabel2);
		shopLabel.add(pokemonLabel3);
		shopLabel.add(pokemonLabel5);
		shopLabel.add(pokemonLabel6);
		shopLabel.add(curBallLabel);
		shopLabel.add(button1);
		shopLabel.add(button2);
		shopLabel.add(button3);
		this.add(shopLabel);
		this.repaint();
		this.revalidate();
		
		mf.add(this);
		mf.repaint();
		mf.revalidate();
	}
}
