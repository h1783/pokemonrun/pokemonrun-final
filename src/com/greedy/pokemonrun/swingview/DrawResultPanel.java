package com.greedy.pokemonrun.swingview;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.greedy.pokemonrun.controller.MainmenuController;

public class DrawResultPanel extends JPanel {

	private Image pokemonView;
	private Image okBt = new ImageIcon("images/button/확인.png").getImage().getScaledInstance(80, 50, 0);
	private MainmenuController mainmenuController;

	public DrawResultPanel(JFrame mf, int acntNum, int random) {

		mainmenuController = new MainmenuController();

		this.setSize(850, 600);
		this.setLayout(null);

		DrawResultPanel thisPanel = this;

		switch (random) {
		case 2:
			pokemonView = new ImageIcon("images/shop/2.거북왕2.png").getImage().getScaledInstance(850, 600, 0);
			break;
		case 3:
			pokemonView = new ImageIcon("images/shop/3.이상해씨2.png").getImage().getScaledInstance(850, 600, 0);
			break;
		case 4:
			pokemonView = new ImageIcon("images/shop/4.고오스2.png").getImage().getScaledInstance(850, 600, 0);
			break;
		case 5:
			pokemonView = new ImageIcon("images/shop/5.리자몽2.png").getImage().getScaledInstance(850, 600, 0);
			break;
		case 6:
			pokemonView = new ImageIcon("images/shop/6.피죤투2.png").getImage().getScaledInstance(850, 600, 0);
			break;
		case 7:
			pokemonView = new ImageIcon("images/shop/7.뮹2.png").getImage().getScaledInstance(850, 600, 0);
			break;
		case 8:
			pokemonView = new ImageIcon("images/shop/8.미뇽2.png").getImage().getScaledInstance(850, 600, 0);
			break;
		case 9:
			pokemonView = new ImageIcon("images/shop/9.푸린2.png").getImage().getScaledInstance(850, 600, 0);
			break;
		case 10:
			pokemonView = new ImageIcon("images/shop/10.팬텀2.png").getImage().getScaledInstance(850, 600, 0);
			break;
		}

		JLabel pokemonLabel = new JLabel(new ImageIcon(pokemonView));
		pokemonLabel.setSize(850, 600);

		JButton button = new JButton(new ImageIcon(okBt));
		button.setBounds(700, 500, 80, 50);

		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int result = 0;
				result = mainmenuController.checkPokemon(acntNum, random); // 중복됬는지 안됬는지 확인결과를 반환
				if (result > 0) {
					mainmenuController.insertGetPokemon(acntNum, random);
					PanelSwitch.ChangePanel(mf, thisPanel, new DrawSucessPanel(mf, acntNum)); // 중복안됬으면 내역에 추가
				} else {
					mainmenuController.plus30CurBall(acntNum);
					PanelSwitch.ChangePanel(mf, thisPanel, new DuplDrawPokemonPanel(mf, acntNum)); // 중복됬으면 +30돌려줌
				}
			}
		});
		
		pokemonLabel.add(button);
		this.add(pokemonLabel);
		this.repaint();
		this.revalidate();

		mf.add(this);
		mf.repaint();
		mf.revalidate();
	}

}
