package com.greedy.pokemonrun.swingview;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.greedy.pokemonrun.controller.AdminController;
import com.greedy.pokemonrun.model.dto.AccountDTO;
/**
 * <pre>
 * Class : AdminManagementPanel
 * Comment : 관리자 계정으로 접속해 회원 계정을 관리
 * History
 * 2021/12/19 (박휘림) 화면 구현
 * 2021/12/19 (홍성원) DB 연동
 * </pre>
 * @version 1
 * @author 박휘림
 * @author 홍성원
 * */
public class AdminManagementPanel extends JPanel {
   
   private Image admin;
   private Image savegui;
   private Image save;
   private Image back;
   private Image ok;
   private Image deletedacnt;
   private AdminManagementPanel thisPanel;
   
   public AdminManagementPanel(JFrame mf) {
      setup();
      thisPanel = this;
   }
   
   private void setup() {
      admin = new ImageIcon("images/admin/관리자화면.png").getImage().getScaledInstance(850, 600, 100);
      savegui = new ImageIcon("images/admin/저장되었습니다..png").getImage().getScaledInstance(850, 600, 0);
      save = new ImageIcon("images/button/저장.png").getImage().getScaledInstance(120, 50, 0);
      back = new ImageIcon("images/button/뒤로가기admin.png").getImage().getScaledInstance(150, 50, 0);
      ok = new ImageIcon("images/button/확인버튼.png").getImage().getScaledInstance(150, 50, 0);
      deletedacnt = new ImageIcon("images/admin/이미삭제계정.png").getImage().getScaledInstance(850, 600, 0);
   }
   
   /**
	 * 메소드 adminManagement에 관한 문서화 주석
	 * Comment : 전체 회원 계정을 볼 수 있는 화면
	 * @return 
	 * @author 박휘림
	 * @author 홍성원
	 * */
   public JPanel adminManagement(JFrame mf) {
      this.setBounds(0, 0, 850, 600);
      this.setLayout(null);
      
      JLabel accountListLabel = new JLabel(new ImageIcon(admin));
      accountListLabel.setBounds(0, 0, 850, 600);

      	
        AdminController adminService = new AdminController();
        List<AccountDTO> memList = adminService.selectAllAccounts();
        String[] colName = new String[] {"회원번호", "아이디", "비밀번호", "이름", "이메일", "탈퇴여부"};
        String[][] value = new String[][] {};
        DefaultTableModel table = new DefaultTableModel(value,colName);
        JTable jtable = new JTable(table);
        RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(table);
        jtable.setRowSorter(sorter);

        JScrollPane scrollPane = new JScrollPane(jtable);
        scrollPane.setBounds(70, 70, 700, 250);

        Font labelFont = new Font("",5,30);
        JLabel label = new JLabel("삭제할 ID 입력");
        label.setBounds(70, 380, 200, 50);
        label.setFont(labelFont);
        JTextField tf = new JTextField();
        tf.setFont(labelFont);
        tf.setBounds(300, 380, 250, 50);
        
      for(AccountDTO mem : memList) {
         String[] values = new String[colName.length];
         values[0] = String.format("%d",mem.getAcntNum());
         values[1] = mem.getAcntId();
         values[2] = mem.getAcntPwd();
         values[3] = mem.getMemName();
         values[4] = mem.getMemEmail();
         values[5] = mem.getDelYn();
         table.addRow(values);
      }
      
      JButton submit = new JButton(new ImageIcon(save));
      submit.setBounds(630, 380, 100, 50);
      accountListLabel.add(submit);
      /* 계정 삭제 후 저장 버튼을 눌렀을 때 성공하면 '저장되었습니다'화면 출력. 실패하면 '이미 삭제된 계정이거나 존재하지 않는 계정입니다 출력' */
      submit.addMouseListener(new MouseAdapter() {
         @Override
         public void mouseClicked(MouseEvent e) {
            AdminController adminService = new AdminController();
            String acntId = tf.getText();
            int result = adminService.deleteAccount(acntId);
            if(result > 0) {
               PanelSwitch.ChangePanel(mf, thisPanel, new AdminManagementPanel(mf).saveList(mf));
            } else {
            	PanelSwitch.ChangePanel(mf, thisPanel, new AdminManagementPanel(mf).deletedAccount(mf));
            }
            
         }
      });
      
      JButton backButton = new JButton(new ImageIcon(back));
      backButton.setBounds(630, 480, 140, 50);
      
      backButton.addMouseListener(new MouseAdapter() {
         @Override
         public void mouseClicked(MouseEvent e) {
            PanelSwitch.ChangePanel(mf, thisPanel, new PokerunFirstView(mf));
         }
      });
      
      
//      Font font1 = new Font("",0,15);
//        TextArea ta1 = new TextArea();
//        ta1.setBounds(70, 70, 700, 350);
//        ta1.setFont(font1);
//        ta1.setEditable(false);
      accountListLabel.add(label);
      accountListLabel.add(tf);
      
      
//      accountListLabel.add(ta1);
//      accountListLabel.add(table);
      accountListLabel.add(scrollPane);
      accountListLabel.add(backButton);
      thisPanel.add(accountListLabel);
      mf.add(thisPanel);
      
      return thisPanel;
      
   }
   
   /**
	 * 메소드 saveList에 관한 문서화 주석
	 * Comment : 회원계정을 삭제한 후 저장 버튼을 눌렀을 때 저장되었습니다 팝업창으로 이동
	 * @param first 
	 * @return 
	 * @author 박휘림
	 * */
   public JPanel saveList(JFrame mf) {
      this.setBounds(0, 0, 850, 600);
      this.setLayout(null);
      
      JLabel saveguiLabel = new JLabel(new ImageIcon(savegui));
      saveguiLabel.setBounds(0, 0, 850, 600);
      
      JButton okButton = new JButton(new ImageIcon(ok));
      okButton.setBounds(370, 430, 120, 50);
      
      okButton.addMouseListener(new MouseAdapter() {
         @Override
         public void mouseClicked(MouseEvent e) {
            PanelSwitch.ChangePanel(mf, thisPanel, new AdminManagementPanel(mf).adminManagement(mf));
         }
      });
      
      saveguiLabel.add(okButton);
      thisPanel.add(saveguiLabel);
      mf.add(thisPanel);
      
      return thisPanel;
   }
   
   /**
  	 * 메소드 deletedAccount에 관한 문서화 주석
  	 * Comment : 회원계정을 삭제 시도 했는데 이미 삭제되었거나 존재하지 않는 경우 출력되는 팝업창
  	 * @param first 
  	 * @return 
  	 * @author 박휘림
  	 * */
   public JPanel deletedAccount(JFrame mf) {
		this.setBounds(0, 0, 850, 600);
		this.setLayout(null);
		
		JLabel deletedLabel = new JLabel(new ImageIcon(deletedacnt));
		deletedLabel.setBounds(0, 0, 850, 600);
		
		JButton okButton = new JButton(new ImageIcon(ok));
		okButton.setBounds(370, 430, 120, 50);
		
		okButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new AdminManagementPanel(mf).adminManagement(mf));
			}
		});
		
		deletedLabel.add(okButton);
		thisPanel.add(deletedLabel);
		mf.add(thisPanel);
		
		return thisPanel;
	}
}