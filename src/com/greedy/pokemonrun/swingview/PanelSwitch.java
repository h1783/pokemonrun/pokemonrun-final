package com.greedy.pokemonrun.swingview;

import java.awt.Frame;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * <pre>
 * Class : PanelSwitch
 * Comment : 판넬 전환 해주는 클래스
 * </pre>
 * @author 홍성원
 * */
public class PanelSwitch {

	/**
	 * Comment : 로그인 Frame에 기존 Panel을 제거하고 새로운 Panel을 올리는 메소드
	 * @author 홍성원
	 * @param mf : 기준이 되는 Frame 
	 * @param op : 제거할 Panel
	 * @param np : 추가할 Panel
	 * */
	public static void ChangePanel(LoginViewFrame mf, JPanel op, JPanel np) {
		mf.remove(op);		//올드패널 없애
		mf.add(np);			//새패널 추가해
		mf.repaint();		//새로고침
		mf.revalidate();	//더 쎈 새로고침
	}

	/**
	 * Comment : 기본 Frame에 기존 Panel을 제거하고 새로운 Panel을 올리는 메소드
	 * @author 구훈모
	 * @param mf : 기준이 되는 Frame 
	 * @param op : 제거할 Panel
	 * @param np : 추가할 Panel
	 * */
	public static void ChangePanel(Frame mf, JPanel op, JPanel np) {
		mf.remove(op);		//올드패널 없애
		mf.add(np);			//새패널 추가해
		mf.repaint();		//새로고침
		mf.revalidate();	//더 쎈 새로고침
	}

}
