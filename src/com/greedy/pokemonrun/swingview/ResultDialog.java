package com.greedy.pokemonrun.swingview;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ResultDialog extends JDialog {

	public ResultDialog(JFrame mf,JPanel thisPanel,int acntNum, int result) {
		
		this.setSize(300, 200);
		this.setLayout(null);
		
		
		
		JLabel message;
		JButton button;
		
		if(result == 0) {
			message = new JLabel("구매 성공!");
			button = new JButton("확인"); 
			
			button.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					PanelSwitch.ChangePanel(mf, thisPanel, new BuyCostumePanel(mf, acntNum));
				}
			});
			
		}else {
			message = new JLabel("몬스터볼이 부족합니다.");
			button = new JButton("확인");
			
			button.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					PanelSwitch.ChangePanel(mf, thisPanel, new BuyCostumePanel(mf, acntNum));
				}
			});

				
		}
		
		this.add(message);
		this.add(button);
		this.repaint();
		this.revalidate();
		mf.add(this);
		mf.repaint();
		mf.revalidate();
	}

}
