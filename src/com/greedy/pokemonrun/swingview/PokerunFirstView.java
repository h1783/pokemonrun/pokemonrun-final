package com.greedy.pokemonrun.swingview;

import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/** 
 * <pre>
 * Class : PokerunFirstView
 * Comment : 게임의 첫 화면 출력을 담당하는 패널
 * History
 * 2021/12/15 (홍성원) 작성
 * </pre>
 * @version 1
 * @author 홍성원
 * */
public class PokerunFirstView extends JPanel {
	
	private Image startView;
	private Image gaipBt;
	private Image logInBt;
	private Image findIdPwdBt;
	
	/* 게임이 실행됐을때 출력되는 첫 패널에 이미지를 설정하는 생성자  */
	/* 프레임을 넣어 생성하면 패널에 시작화면의 버튼과 이미지를 추가한 뒤 그 패널을 프레임에 담아준다.*/
	public PokerunFirstView(JFrame mf) {
		this.setSize(850, 600);
		this.setLayout(null);
		setUp();
		PokerunFirstView thisPanel = this;

		JLabel backLabel = new JLabel(new ImageIcon(startView));
		backLabel.setBounds(0, 0, 850, 600);

		JButton button1 = new JButton(new ImageIcon(logInBt));
		JButton button2 = new JButton(new ImageIcon(gaipBt));
		JButton button3 = new JButton(new ImageIcon(findIdPwdBt));
		button1.setBounds(650, 100, 120, 50);
		button2.setBounds(650, 170, 120, 50);
		button3.setBounds(650, 240, 120, 50);
		
		/* 버튼을 누르면 각각 지정된 패널로 이동하는 이벤트를 추가해 준다 */
		/* 로그인 버튼을 누르면 로그인 화면으로 넘어간다. */
		button1.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new LoginViewPanel(mf).loginMainView(mf) );
			}
		});
		/* 회원가입 버튼을 누르면 회원가입 화면으로 넘어간다. */
		button2.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new SignUpPanel(mf).signUpMain(mf));
			}
		});
		
		/* 아이디, 비밀번호찾기 버튼을 누르면 아이디 비밀번호찾기 화면으로 이동한다 */
		button3.addMouseListener(new MouseAdapter() {
			@Override 
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new IdPwdSearchPanel(mf).idPwdSearchPanel(mf));
			}
		});
		
		this.add(button1);
		this.add(button2);
		this.add(button3);
		this.add(backLabel);
		
		this.repaint();
		this.revalidate();

		mf.add(this);
		mf.repaint();
		mf.revalidate();
		
	}
	

	private void setUp() {
		startView = new ImageIcon("images/startgameview.png").getImage().getScaledInstance(850, 600, 0);
		gaipBt = new ImageIcon("images/button/회원가입bt.png").getImage().getScaledInstance(120, 50, 0);
		logInBt = new ImageIcon("images/button/로그인bt.png").getImage().getScaledInstance(120, 50, 0);
		findIdPwdBt = new ImageIcon("images/button/idpwd찾기.png").getImage().getScaledInstance(120, 50, 0);
	}

}
