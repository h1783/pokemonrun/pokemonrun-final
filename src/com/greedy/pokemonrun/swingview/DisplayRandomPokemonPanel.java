package com.greedy.pokemonrun.swingview;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class DisplayRandomPokemonPanel extends JPanel {
	
	Image img = new ImageIcon("images/shop/뽑기영상.gif").getImage().getScaledInstance(850, 600, 0);
	
	public DisplayRandomPokemonPanel(JFrame mf, int acntNum) {
	
		this.setSize(850, 600);
		this.setLayout(null);
		
		DisplayRandomPokemonPanel thisPanel = this;
		
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				JLabel ballLabel = new JLabel(new ImageIcon(img));
				ballLabel.setBounds(0, 0, 850, 600);
				thisPanel.add(ballLabel);
				thisPanel.repaint();
				thisPanel.revalidate();
				
				for(int i = 2; i >= 0; i--) {
					try {
						System.out.println(i);
						Thread.sleep(300);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if(i == 0) {
						   PanelSwitch.ChangePanel(mf, thisPanel, new DrawPanel(mf, acntNum));
					}
				}
			}
		});
		
		t.start();
		
		mf.add(this);
		mf.repaint();
		mf.revalidate();
	}
}
