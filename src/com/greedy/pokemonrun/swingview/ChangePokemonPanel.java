package com.greedy.pokemonrun.swingview;

import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.greedy.pokemonrun.controller.LoginController;
import com.greedy.pokemonrun.controller.MainmenuController;
/**
 * <pre>
 * Class : ChangePokemonPanel
 * Comment : 마이페이지에서 포켓몬을 교체하는 화면 
 * History
 * 2021/12/19 (박휘림) 화면 구현
 * 2021/12/19 (홍성원) DB 연동
 * </pre>
 * @version 1
 * @author 박휘림
 * @author 홍성원
 * */
public class ChangePokemonPanel extends JPanel{
	
	private Image changePokemon;
	private Image turtleKing;
	private Image goos;
	private Image lizamong;
	private Image mew;
	private Image minyong;
	private Image leesanghaessi;
	private Image phantom;
	private Image purin;
	private Image pigeontwo;
	private Image pikachu;
	private Image noPokemon;
	private Image ok;
	private Image back;
	private ChangePokemonPanel thisPanel;
	
	public ChangePokemonPanel(JFrame mf) {
		setup();
		thisPanel = this;
	}

	private void setup() {
		changePokemon = new ImageIcon("images/pokemon/체인지포켓몬화면.png").getImage().getScaledInstance(850, 600, 0);
		turtleKing = new ImageIcon("images/pokemon/거북왕1.png").getImage().getScaledInstance(145, 145, 0);
		goos = new ImageIcon("images/pokemon/고오스1.png").getImage().getScaledInstance(145, 145, 0);
		lizamong = new ImageIcon("images/pokemon/리자몽1.png").getImage().getScaledInstance(145, 145, 0);
		mew = new ImageIcon("images/pokemon/뮤1.png").getImage().getScaledInstance(145, 145, 0);
		minyong = new ImageIcon("images/pokemon/미뇽1.png").getImage().getScaledInstance(145, 145, 0);
		leesanghaessi = new ImageIcon("images/pokemon/이상해씨1.png").getImage().getScaledInstance(145, 145, 0);
		phantom = new ImageIcon("images/pokemon/팬텀1.png").getImage().getScaledInstance(145, 145, 0);
		purin = new ImageIcon("images/pokemon/푸린1.png").getImage().getScaledInstance(145, 145, 0);
		pigeontwo = new ImageIcon("images/pokemon/피존투1.png").getImage().getScaledInstance(145, 145, 0);
		pikachu = new ImageIcon("images/pokemon/피카츄1.png").getImage().getScaledInstance(145, 145, 0);
		noPokemon = new ImageIcon("images/보유하지않은포켓몬.png").getImage().getScaledInstance(850, 600, 0);
		ok = new ImageIcon("images/button/확인버튼.png").getImage().getScaledInstance(200, 60, 0);
		back = new ImageIcon("images/button/backbt.png").getImage().getScaledInstance(150, 50, 0);
	}
	
	/**
	 * 메소드 changePokemon에 관한 문서화 주석
	 * Comment : 마이페이지 뷰에서 changePokemon을 클릭해서 원하는 포켓몬을 착용하는 화면
	 * @param first 
	 * @param second 로그인한 회원 계정 넘버
	 * @return 
	 * @author 박휘림
	 * @author 홍성원
	 * */
	public JPanel changePokemon(JFrame mf, int acntNum) {
		this.setBounds(0, 0, 850, 600);
		this.setLayout(null);
		
		JLabel changePokemonLabel = new JLabel(new ImageIcon(changePokemon));
		changePokemonLabel.setBounds(0, 0, 850, 600);
		
		JButton pikachuButton = new JButton(new ImageIcon(pikachu));
		pikachuButton.setBounds(25, 110, 145, 145);
		pikachuButton.addMouseListener(new MouseAdapter() {
			@Override
			/* 회원계정 넘버와 포켓몬 넘버를 받아와서 해당 계정이 해당 포켓몬을 보유하고 있는지 if문으로 판별 */
			public void mouseClicked(MouseEvent e) {
				LoginController lc = new LoginController();
				int result = lc.changePokemon(acntNum, 1);
				
				if(result > 0) {
					PanelSwitch.ChangePanel(mf, thisPanel, new MypagePanel(mf, acntNum).myPageView(mf, acntNum));
				} else {
					PanelSwitch.ChangePanel(mf, thisPanel, new ChangePokemonPanel(mf).noPokemon(mf, acntNum));
				}
			}
		});
		
		JButton turtleKingButton = new JButton(new ImageIcon(turtleKing));
		turtleKingButton.setBounds(185, 110, 145, 145);
		turtleKingButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				LoginController lc = new LoginController();
				/* 회원계정 넘버와 포켓몬 넘버를 받아와서 해당 계정이 해당 포켓몬을 보유하고 있는지 if문으로 판별 */
				int result = lc.changePokemon(acntNum, 2);
				
				
				if(result > 0) {
					PanelSwitch.ChangePanel(mf, thisPanel, new MypagePanel(mf, acntNum).myPageView(mf, acntNum));
				} else {
					PanelSwitch.ChangePanel(mf, thisPanel, new ChangePokemonPanel(mf).noPokemon(mf, acntNum));
				}
			}
		});
		
		JButton leesanghaessiButton = new JButton(new ImageIcon(leesanghaessi));
		leesanghaessiButton.setBounds(345, 110, 145, 145);
		leesanghaessiButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				LoginController lc = new LoginController();
				/* 회원계정 넘버와 포켓몬 넘버를 받아와서 해당 계정이 해당 포켓몬을 보유하고 있는지 if문으로 판별 */
				int result = lc.changePokemon(acntNum, 3);
				
				
				if(result > 0) {
					PanelSwitch.ChangePanel(mf, thisPanel, new MypagePanel(mf, acntNum).myPageView(mf, acntNum));
				} else {
					PanelSwitch.ChangePanel(mf, thisPanel, new ChangePokemonPanel(mf).noPokemon(mf, acntNum));
				}
			}
		});
		
		JButton goosButton = new JButton(new ImageIcon(goos));
		goosButton.setBounds(505, 110, 145, 145);
		goosButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				LoginController lc = new LoginController();
				/* 회원계정 넘버와 포켓몬 넘버를 받아와서 해당 계정이 해당 포켓몬을 보유하고 있는지 if문으로 판별 */
				int result = lc.changePokemon(acntNum, 4);
				
				
				if(result > 0) {
					PanelSwitch.ChangePanel(mf, thisPanel, new MypagePanel(mf, acntNum).myPageView(mf, acntNum));
				} else {
					PanelSwitch.ChangePanel(mf, thisPanel, new ChangePokemonPanel(mf).noPokemon(mf, acntNum));
				}
			}
		});
		
		JButton lizamongButton = new JButton(new ImageIcon(lizamong));
		lizamongButton.setBounds(665, 110, 145, 145);
		lizamongButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				LoginController lc = new LoginController();
				/* 회원계정 넘버와 포켓몬 넘버를 받아와서 해당 계정이 해당 포켓몬을 보유하고 있는지 if문으로 판별 */
				int result = lc.changePokemon(acntNum, 5);
				
				if(result > 0) {
					PanelSwitch.ChangePanel(mf, thisPanel, new MypagePanel(mf, acntNum).myPageView(mf, acntNum));
				} else {
					PanelSwitch.ChangePanel(mf, thisPanel, new ChangePokemonPanel(mf).noPokemon(mf, acntNum));
				}
			}
		});
		
		JButton pigeontwoButton = new JButton(new ImageIcon(pigeontwo));
		pigeontwoButton.setBounds(25, 300, 145, 145);
		pigeontwoButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				LoginController lc = new LoginController();
				/* 회원계정 넘버와 포켓몬 넘버를 받아와서 해당 계정이 해당 포켓몬을 보유하고 있는지 if문으로 판별 */
				int result = lc.changePokemon(acntNum, 6);
				
				
				if(result > 0) {
					PanelSwitch.ChangePanel(mf, thisPanel, new MypagePanel(mf, acntNum).myPageView(mf, acntNum));
				} else {
					PanelSwitch.ChangePanel(mf, thisPanel, new ChangePokemonPanel(mf).noPokemon(mf, acntNum));
				}
			}
		});
		
		JButton mewButton = new JButton(new ImageIcon(mew));
		mewButton.setBounds(185, 300, 145, 145);
		mewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				LoginController lc = new LoginController();
				/* 회원계정 넘버와 포켓몬 넘버를 받아와서 해당 계정이 해당 포켓몬을 보유하고 있는지 if문으로 판별 */
				int result = lc.changePokemon(acntNum, 7);
				
				
				if(result > 0) {
					PanelSwitch.ChangePanel(mf, thisPanel, new MypagePanel(mf, acntNum).myPageView(mf, acntNum));
				} else {
					PanelSwitch.ChangePanel(mf, thisPanel, new ChangePokemonPanel(mf).noPokemon(mf, acntNum));
				}
			}
		});
		
		JButton minyongButton = new JButton(new ImageIcon(minyong));
		minyongButton.setBounds(345, 300, 145, 145);
		minyongButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				LoginController lc = new LoginController();
				/* 회원계정 넘버와 포켓몬 넘버를 받아와서 해당 계정이 해당 포켓몬을 보유하고 있는지 if문으로 판별 */
				int result = lc.changePokemon(acntNum, 8);
				
				
				if(result > 0) {
					PanelSwitch.ChangePanel(mf, thisPanel, new MypagePanel(mf, acntNum).myPageView(mf, acntNum));
				} else {
					PanelSwitch.ChangePanel(mf, thisPanel, new ChangePokemonPanel(mf).noPokemon(mf, acntNum));
				}
			}
		});
		
		JButton purinButton = new JButton(new ImageIcon(purin));
		purinButton.setBounds(505, 300, 145, 145);
		purinButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				LoginController lc = new LoginController();
				/* 회원계정 넘버와 포켓몬 넘버를 받아와서 해당 계정이 해당 포켓몬을 보유하고 있는지 if문으로 판별 */
				int result = lc.changePokemon(acntNum, 9);
				
				
				if(result > 0) {
					PanelSwitch.ChangePanel(mf, thisPanel, new MypagePanel(mf, acntNum).myPageView(mf, acntNum));
				} else {
					PanelSwitch.ChangePanel(mf, thisPanel, new ChangePokemonPanel(mf).noPokemon(mf, acntNum));
				}
			}
		});
		
		JButton phantomButton = new JButton(new ImageIcon(phantom));
		phantomButton.setBounds(665, 300, 145, 145);
		phantomButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				LoginController lc = new LoginController();
				/* 회원계정 넘버와 포켓몬 넘버를 받아와서 해당 계정이 해당 포켓몬을 보유하고 있는지 if문으로 판별 */
				int result = lc.changePokemon(acntNum, 10);
				
				
				if(result > 0) {
					PanelSwitch.ChangePanel(mf, thisPanel, new MypagePanel(mf, acntNum).myPageView(mf, acntNum));
				} else {
					PanelSwitch.ChangePanel(mf, thisPanel, new ChangePokemonPanel(mf).noPokemon(mf, acntNum));
				}
			}
		});
		
		JButton goBack = new JButton(new ImageIcon(back));
		goBack.setBounds(340, 460, 150, 50);
		/* 뒤로 가기 버튼을 누르면 마이페이지 뷰로 이동한다. */
		goBack.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new MypagePanel(mf, acntNum).myPageView(mf, acntNum));
			}
		});
		
		changePokemonLabel.add(turtleKingButton);
		changePokemonLabel.add(goosButton);
		changePokemonLabel.add(lizamongButton);
		changePokemonLabel.add(mewButton);
		changePokemonLabel.add(minyongButton);
		changePokemonLabel.add(leesanghaessiButton);
		changePokemonLabel.add(phantomButton);
		changePokemonLabel.add(purinButton);
		changePokemonLabel.add(pigeontwoButton);
		changePokemonLabel.add(pikachuButton);
		changePokemonLabel.add(goBack);
		thisPanel.add(changePokemonLabel);
		mf.add(thisPanel);
		return thisPanel;
	}
	
	/**
	 * 메소드 noPokemon에 관한 문서화 주석
	 * Comment : 마이페이지 뷰에서 changePokemon을 클릭해서 원하는 포켓몬을 선택했을 때 미보유 포켓몬일 경우 출력되는 화면
	 * @param first 
	 * @param second 로그인한 회원 계정 넘버
	 * @return 
	 * @author 박휘림
	 * */
	/* 변경하려고 선택한 포켓몬이 없다면 보유하지 않은 포켓몬입니다 출력*/
	public JPanel noPokemon(JFrame mf, int acntNum) {
			this.setSize(850, 600);
			this.setLayout(null);
			
			JLabel backLabel = new JLabel(new ImageIcon(noPokemon));
			backLabel.setBounds(0, 0, 850, 600);
			/* 확인 버튼을 누르면 포켓몬 변경하는 화면으로 이동 */
			JButton okok = new JButton(new ImageIcon(ok));
			okok.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					PanelSwitch.ChangePanel(mf, thisPanel, new ChangePokemonPanel(mf).changePokemon(mf, acntNum));
				}
			});
			
			
			okok.setBounds(350, 400, 200, 60);
			backLabel.add(okok);
			this.add(backLabel);
			this.add(okok);
			mf.add(this);
			
		return thisPanel;
	}
	
}









