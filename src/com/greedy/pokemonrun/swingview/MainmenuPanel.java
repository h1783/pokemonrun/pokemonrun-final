package com.greedy.pokemonrun.swingview;

import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.greedy.pokemonrun.ingame.InGame;

public class MainmenuPanel extends JPanel {

	
	Image mainmenuView = new ImageIcon("images/메인메뉴.png").getImage().getScaledInstance(850, 600, 0);
	Image mypageBt = new ImageIcon("images/button/mtpagebt.png").getImage().getScaledInstance(110, 50, 0);
	Image shopBt = new ImageIcon("images/button/shopbt.png").getImage().getScaledInstance(100, 50, 0);
	Image dicBt = new ImageIcon("images/button/pokemonbt.png").getImage().getScaledInstance(170, 50, 0);
	Image rankingBt = new ImageIcon("images/button/rankbt.png").getImage().getScaledInstance(120, 50, 0);
	Image settingBt = new ImageIcon("images/button/settingbt.png").getImage().getScaledInstance(150, 55, 0);
	Image gameStartBt = new ImageIcon("images/button/startbt.png").getImage().getScaledInstance(150, 60, 0);
	Image gameCloseBt = new ImageIcon("images/button/quitbt.png").getImage().getScaledInstance(150, 60, 0);
	
	public MainmenuPanel() {
		
	}
	
	
	public MainmenuPanel(JFrame mf,int acntNum) { 
		
		this.setSize(850, 600);
		this.setLayout(null);
		
		MainmenuPanel thisPanel = this;
		
		JLabel menuLabel = new JLabel(new ImageIcon(mainmenuView));
		menuLabel.setBounds(0, 0, 850, 600);
		
		JButton button1 = new JButton(new ImageIcon(mypageBt));
		JButton button2 = new JButton(new ImageIcon(shopBt));
		JButton button3 = new JButton(new ImageIcon(dicBt));
		JButton button4 = new JButton(new ImageIcon(rankingBt));
		JButton button5 = new JButton(new ImageIcon(settingBt));
		JButton button6 = new JButton(new ImageIcon(gameStartBt));
		JButton button7 = new JButton(new ImageIcon(gameCloseBt));
		button1.setBounds(600, 60, 110, 45);
		button2.setBounds(600, 140, 100, 50);
		button3.setBounds(600, 220, 170, 50);
		button4.setBounds(600, 300, 100, 55);
		button5.setBounds(600, 380, 150, 55);
		button6.setBounds(50, 450, 150, 60);
		button7.setBounds(250, 450, 130, 60);
		
		button1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new MypagePanel(mf, acntNum).myPageView(mf, acntNum));
			}
		});
		
		button2.addMouseListener(new MouseAdapter() { //상점이동
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new ShopPanel(mf, acntNum));
			}
		});
		
		button3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new PokemonBookPanel(mf).pokemonBook(mf, acntNum));
			}
		});
		
		button4.addMouseListener(new MouseAdapter() { //랭킹
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new RankingPanel(mf, acntNum));
			}
		});
		
		button5.addMouseListener(new MouseAdapter() { // 설정
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new SettingViewPanel().SettingMainView(mf, acntNum));
			}
			
		});
		
		button6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				/*게임 연결*/
				PanelSwitch.ChangePanel(mf, thisPanel, new InGame().runGame(mf, acntNum));
			}
		});
		
		button7.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				PanelSwitch.ChangePanel(mf, thisPanel, new PokerunFirstView(mf));
			}
		});
		
		menuLabel.add(button1);
		menuLabel.add(button2);
		menuLabel.add(button3);
		menuLabel.add(button4);
		menuLabel.add(button5);
		menuLabel.add(button6);
		menuLabel.add(button7);
		this.add(menuLabel);
		mf.add(this);
		mf.repaint();
		mf.revalidate();
	}

}
