package com.greedy.pokemonrun.swingview;

import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.greedy.pokemonrun.controller.LoginController;
import com.greedy.pokemonrun.controller.MainmenuController;
/**
 * <pre>
 * Class : ChangeCostumePanel
 * Comment : 마이페이지에서 코스튬을 교체하는 화면 
 * History
 * 2021/12/19 (박휘림) 화면 구현
 * 2021/12/19 (홍성원) DB 연동
 * </pre>
 * @version 1
 * @author 박휘림
 * @author 홍성원
 * */
public class ChangeCostumePanel extends JPanel{
	
	private Image cap;
	private Image scarf;
	private Image costumeChange;
	private Image noCostume;
	private Image ok;
	private Image back;
	private ChangeCostumePanel thisPanel;
	
	public ChangeCostumePanel(JFrame mf) {
		setup();
		thisPanel = this;
	}

	private void setup() {
		costumeChange = new ImageIcon("images/costume/코스튬변경화면.png").getImage().getScaledInstance(850, 600, 0);
		cap = new ImageIcon("images/costume/모자.png").getImage().getScaledInstance(330, 330, 0);
		scarf = new ImageIcon("images/costume/스카프.png").getImage().getScaledInstance(330, 330, 0);
		noCostume = new ImageIcon("images/보유하지않은코스튬.png").getImage().getScaledInstance(850, 600, 0);
		ok = new ImageIcon("images/button/확인버튼.png").getImage().getScaledInstance(200, 60, 0);
		back = new ImageIcon("images/button/backbt.png").getImage().getScaledInstance(150, 50, 0);
	}
	
	/**
	 * 메소드 changeCostume에 관한 문서화 주석
	 * Comment : 마이페이지 뷰에서 Costume을 클릭해서 원하는 코스튬을 착용하는 화면
	 * @param first 
	 * @param second 로그인한 회원 계정 넘버
	 * @return 
	 * @author 박휘림
	 * @author 홍성원
	 * */
	/* 코스튬 바꾸기 */
	public JPanel changeCostume(JFrame mf, int acntNum) {
		this.setBounds(0, 0, 850, 600);
		this.setLayout(null);
		
		JLabel changeCostumeLabel = new JLabel(new ImageIcon(costumeChange));
		changeCostumeLabel.setBounds(0, 0, 850, 600);
		
		JButton scarfButton = new JButton(new ImageIcon(scarf));
		scarfButton.setBounds(450, 120, 330, 330);
		/* 회원 계정번호를 받아와서 해당 코스튬을 보유하고 있는지 if문으로 판별해서 보유 코스튬일 경우  마이페이지로 넘어가고
		 * 미보유 코스튬일 경우 '보유하지 않은 코스튬입니다' 출력된다. */
		scarfButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				LoginController lc = new LoginController();
				/* 해당 회원 계정 넘버와 코스튬 넘버를 인자로 받아 로그인 상태의 회원이 해당 코스튬을 보유하고 있는지 판별한다. */
				int result = lc.changeCostume(acntNum, 1);
				
				if(result > 0) {
					PanelSwitch.ChangePanel(mf, thisPanel, new MypagePanel(mf, acntNum).myPageView(mf, acntNum));					
				} else {
					PanelSwitch.ChangePanel(mf, thisPanel, new ChangeCostumePanel(mf).noCostume(mf, acntNum));
				}
			} 
		});
		
		JButton capButton = new JButton(new ImageIcon(cap));
		capButton.setBounds(70, 120, 330, 330);
		/* 회원 계정번호를 받아와서 해당 코스튬을 보유하고 있는지 if문으로 판별해서 보유 코스튬일 경우  마이페이지로 넘어가고
		 * 미보유 코스튬일 경우 '보유하지 않은 코스튬입니다' 출력된다. */
		capButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				LoginController lc = new LoginController();
				int result = lc.changeCostume(acntNum, 2);
				
				if(result > 0) {
					PanelSwitch.ChangePanel(mf, thisPanel, new MypagePanel(mf, acntNum).myPageView(mf, acntNum));					
				} else {
					PanelSwitch.ChangePanel(mf, thisPanel, new ChangeCostumePanel(mf).noCostume(mf, acntNum));
				}
			}
		});
		
		JButton goBack = new JButton(new ImageIcon(back));
		goBack.setBounds(340, 460, 150, 50);
		/* 뒤로가기 버튼을 누르면 마이페이지 뷰로 이동한다.*/
		goBack.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PanelSwitch.ChangePanel(mf, thisPanel, new MypagePanel(mf, acntNum).myPageView(mf, acntNum));
			}
		});
		
		changeCostumeLabel.add(capButton);
		changeCostumeLabel.add(scarfButton);
		changeCostumeLabel.add(goBack);
		thisPanel.add(changeCostumeLabel);
		mf.add(thisPanel);
		
		return thisPanel;
	}
	
	/**
	 * 메소드 noCostume에 관한 문서화 주석
	 * Comment : 마이페이지 뷰에서 Costume을 클릭해서 원하는 코스튬을 클릭했는데 미보유 코스튬일 경우 출력되는 화면
	 * @param first 
	 * @param second 로그인한 회원 계정 넘버
	 * @return 
	 * @author 박휘림
	 * */
	public JPanel noCostume(JFrame mf, int acntNum) {
		this.setSize(850, 600);
		this.setLayout(null);

		JLabel noCosLabel = new JLabel(new ImageIcon(noCostume));
		noCosLabel.setBounds(0, 0, 850, 600);
		/* 확인 버튼을 누르면 다시 코스튬을 선택하는 창으로 돌아간다. */
		JButton okok = new JButton(new ImageIcon(ok));
		okok.setBounds(350, 400, 200, 60);
		okok.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
						PanelSwitch.ChangePanel(mf, thisPanel, new ChangeCostumePanel(mf).changeCostume(mf, acntNum));
					}
				});
		
		noCosLabel.add(okok);
		thisPanel.add(noCosLabel);
		mf.add(thisPanel);
		
		return thisPanel;
		
	}


}
