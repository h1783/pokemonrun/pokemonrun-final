
DROP SEQUENCE SEQ_ACNT_NUM;
DROP SEQUENCE SEQ_PLAY_DATE_NUM;

CREATE SEQUENCE SEQ_ACNT_NUM;
CREATE SEQUENCE SEQ_PLAY_DATE_NUM;

DROP TABLE TEST_TABLE CASCADE CONSTRAINTS;
DROP TABLE TBL_MAP_KIND CASCADE CONSTRAINTS;
DROP TABLE TBL_MAP CASCADE CONSTRAINTS;
DROP TABLE TBL_ELEMENT CASCADE CONSTRAINTS;
DROP TABLE TBL_GET_POKEMON CASCADE CONSTRAINTS;
DROP TABLE TBL_COSTUME CASCADE CONSTRAINTS;
DROP TABLE TBL_PLAY_HISTORY CASCADE CONSTRAINTS;
DROP TABLE TBL_PLAYER CASCADE CONSTRAINTS;
DROP TABLE TBL_BUY_COSTUME CASCADE CONSTRAINTS;
DROP TABLE TBL_ACCOUNT CASCADE CONSTRAINTS;
DROP TABLE TBL_POKEMON CASCADE CONSTRAINTS;


CREATE TABLE TBL_ACCOUNT(
  ACNT_NUM NUMBER PRIMARY KEY,
  ACNT_KIND VARCHAR2(20)  DEFAULT 'PLAYER' NOT NULL,
  ACNT_ID VARCHAR2(30) UNIQUE NOT NULL,
  ACNT_PW VARCHAR2(20) NOT NULL,
  DEL_YN VARCHAR2(10) DEFAULT 'N' NOT NULL,
  DEL_DATE DATE,
  MEM_NAME VARCHAR2(30) NOT NULL,
  MEM_EMAIL VARCHAR2(50) UNIQUE NOT NULL,
  ENT_DATE DATE NOT NULL
);

COMMENT ON COLUMN TBL_ACCOUNT.ACNT_NUM IS '계정번호';
COMMENT ON COLUMN TBL_ACCOUNT.ACNT_KIND IS '계정종류';
COMMENT ON COLUMN TBL_ACCOUNT.ACNT_ID IS '아이디';
COMMENT ON COLUMN TBL_ACCOUNT.ACNT_PW IS '비밀번호';
COMMENT ON COLUMN TBL_ACCOUNT.DEL_YN IS '삭제여부';
COMMENT ON COLUMN TBL_ACCOUNT.DEL_DATE IS '삭제날짜';
COMMENT ON COLUMN TBL_ACCOUNT.MEM_NAME IS '이름';
COMMENT ON COLUMN TBL_ACCOUNT.MEM_EMAIL IS '이메일';
COMMENT ON COLUMN TBL_ACCOUNT.ENT_DATE IS '가입일자';
COMMENT ON TABLE TBL_ACCOUNT IS '계정';


CREATE TABLE TBL_COSTUME(
  COSTUME_NUM NUMBER PRIMARY KEY,
  COSTUME_NAME VARCHAR2(30) UNIQUE NOT NULL,
  INCREASE_LIFE NUMBER NOT NULL,
  INCREASE_SCORE_RATE NUMBER NOT NULL,
  COSTUME_PRICE NUMBER NOT NULL
);
COMMENT ON COLUMN TBL_COSTUME.COSTUME_NUM IS '코스튬 번호';
COMMENT ON COLUMN TBL_COSTUME.COSTUME_NAME IS '코스튬 종류';
COMMENT ON COLUMN TBL_COSTUME.INCREASE_LIFE IS '목숨 수';
COMMENT ON COLUMN TBL_COSTUME.INCREASE_SCORE_RATE IS '점수 증가';
COMMENT ON COLUMN TBL_COSTUME.COSTUME_PRICE IS '코스튬 가격';
COMMENT ON TABLE TBL_COSTUME IS '코스튬';



CREATE TABLE TBL_POKEMON(
  POKEMON_NUM NUMBER PRIMARY KEY,
  POKEMON_NAME VARCHAR2(30) UNIQUE NOT NULL,
  INCREASE_BALL_RATE NUMBER NOT NULL,
  POKEMON_PRICE NUMBER NOT NULL
);

COMMENT ON COLUMN TBL_POKEMON.POKEMON_NUM IS '포켓몬 번호';
COMMENT ON COLUMN TBL_POKEMON.POKEMON_NAME IS '포켓몬 종류';
COMMENT ON COLUMN TBL_POKEMON.INCREASE_BALL_RATE IS '포켓몬 능력(몬스터볼증가)';
COMMENT ON COLUMN TBL_POKEMON.POKEMON_PRICE IS '포켓몬 가격';
COMMENT ON TABLE TBL_POKEMON IS '포켓몬';





CREATE TABLE TBL_PLAYER(
  ACNT_NUM NUMBER PRIMARY KEY REFERENCES TBL_ACCOUNT (ACNT_NUM),
  CUR_BALL NUMBER DEFAULT 100 NOT NULL,
  HIGH_SCORE NUMBER DEFAULT 0 NOT NULL,
  CUR_POKEMON_NUM NUMBER DEFAULT 1 REFERENCES TBL_POKEMON (POKEMON_NUM),
  COSTUME_NUM NUMBER REFERENCES TBL_COSTUME (COSTUME_NUM)
);
  ALTER 
  TABLE TBL_PLAYER
    ADD CHECK(CUR_BALL >= 0); 

COMMENT ON COLUMN TBL_PLAYER.ACNT_NUM IS '계정번호';
COMMENT ON COLUMN TBL_PLAYER.CUR_BALL IS '현재몬스터볼갯수';
COMMENT ON COLUMN TBL_PLAYER.HIGH_SCORE IS '최고점수';
COMMENT ON COLUMN TBL_PLAYER.CUR_POKEMON_NUM IS '현재포켓몬';
COMMENT ON COLUMN TBL_PLAYER.COSTUME_NUM IS '코스튬 번호';
COMMENT ON TABLE TBL_PLAYER IS '플레이어';


CREATE TABLE TBL_BUY_COSTUME(
  COSTUME_NUM NUMBER,
  ACNT_NUM NUMBER,
  BUY_DATE DATE NOT NULL,
  CONSTRAINT FK_COSTUME_NUM2 FOREIGN KEY (COSTUME_NUM) REFERENCES TBL_COSTUME (COSTUME_NUM),
  CONSTRAINT FK_ACNT_NUM2 FOREIGN KEY (ACNT_NUM) REFERENCES TBL_PLAYER (ACNT_NUM),
  CONSTRAINT BUY_COSTUME_NO2 PRIMARY KEY(COSTUME_NUM, ACNT_NUM)
);

COMMENT ON COLUMN TBL_BUY_COSTUME.COSTUME_NUM IS '코스튬 번호';
COMMENT ON COLUMN TBL_BUY_COSTUME.ACNT_NUM IS '계정번호';
COMMENT ON COLUMN TBL_BUY_COSTUME.BUY_DATE IS '획득일';
COMMENT ON TABLE TBL_BUY_COSTUME IS '코스튬 구매 내역';


CREATE TABLE TBL_ELEMENT(
  ELEMENT_NUM NUMBER PRIMARY KEY,
  ELEMENT_NAME VARCHAR2(30) UNIQUE NOT NULL,
  ELEMENT_LENGTH NUMBER NOT NULL,
  HURDLE_HEIGHT NUMBER NOT NULL,
  HURDLE_DAMAGE NUMBER NOT NULL
);

COMMENT ON COLUMN TBL_ELEMENT.ELEMENT_NUM IS '요소번호';
COMMENT ON COLUMN TBL_ELEMENT.ELEMENT_NAME IS '종류';
COMMENT ON COLUMN TBL_ELEMENT.ELEMENT_LENGTH IS '길이';
COMMENT ON COLUMN TBL_ELEMENT.HURDLE_HEIGHT IS '높이';
COMMENT ON COLUMN TBL_ELEMENT.HURDLE_DAMAGE IS '데미지';
COMMENT ON TABLE TBL_ELEMENT IS '요소';


CREATE TABLE TBL_MAP(
  MAP_NUM NUMBER PRIMARY KEY,
  MAP_NAME VARCHAR2(30) NOT NULL
);

COMMENT ON COLUMN TBL_MAP.MAP_NUM IS '맵 번호';
COMMENT ON COLUMN TBL_MAP.MAP_NAME IS '맵 종류';
COMMENT ON TABLE TBL_MAP IS '맵';



CREATE TABLE TBL_GET_POKEMON(
  POKEMON_NUM NUMBER,
  ACNT_NUM NUMBER,
  GET_DATE DATE NOT NULL,
  CONSTRAINT FK_POKEMON_NUM3 FOREIGN KEY (POKEMON_NUM) REFERENCES TBL_POKEMON (POKEMON_NUM),
  CONSTRAINT FK_ACNT_NUM3 FOREIGN KEY (ACNT_NUM) REFERENCES TBL_PLAYER (ACNT_NUM),
  CONSTRAINT GET_POKEMON_NO2 PRIMARY KEY(POKEMON_NUM, ACNT_NUM)
);

COMMENT ON COLUMN TBL_GET_POKEMON.POKEMON_NUM IS '포켓몬 번호';
COMMENT ON COLUMN TBL_GET_POKEMON.ACNT_NUM IS '계정번호';
COMMENT ON COLUMN TBL_GET_POKEMON.GET_DATE IS '획득일';
COMMENT ON TABLE TBL_GET_POKEMON IS '포켓몬획득내역';


CREATE TABLE TBL_PLAY_HISTORY(
  PLAY_DATE_NUM NUMBER PRIMARY KEY,
  PLAY_DATE DATE NOT NULL,
  GET_SCORE NUMBER NOT NULL,
  GET_BALL NUMBER NOT NULL,
  USE_POKEMON_NUM NUMBER NOT NULL,
  MAP_NUM NUMBER NOT NULL REFERENCES TBL_MAP (MAP_NUM),
  ACNT_NUM NUMBER NOT NULL REFERENCES TBL_PLAYER (ACNT_NUM),
  USE_COSTUME NUMBER
);

COMMENT ON COLUMN TBL_PLAY_HISTORY.PLAY_DATE_NUM IS '플레이내역번호';
COMMENT ON COLUMN TBL_PLAY_HISTORY.PLAY_DATE IS '날짜';
COMMENT ON COLUMN TBL_PLAY_HISTORY.GET_SCORE IS '획득점수';
COMMENT ON COLUMN TBL_PLAY_HISTORY.GET_BALL IS '획득몬스터볼';
COMMENT ON COLUMN TBL_PLAY_HISTORY.USE_POKEMON_NUM IS '사용한 포켓몬';
COMMENT ON COLUMN TBL_PLAY_HISTORY.MAP_NUM IS '맵 번호';
COMMENT ON COLUMN TBL_PLAY_HISTORY.ACNT_NUM IS '계정번호';
COMMENT ON COLUMN TBL_PLAY_HISTORY.USE_COSTUME IS '착용중인코스튬';
COMMENT ON TABLE TBL_PLAY_HISTORY IS '게임플레이내역';


CREATE TABLE TBL_MAP_KIND(
  ELEMENT_NUM NUMBER REFERENCES TBL_ELEMENT (ELEMENT_NUM),
  MAP_NUM NUMBER REFERENCES TBL_MAP (MAP_NUM),
  INGAME_NUM NUMBER,
  CONSTRAINT MAP_KIND_NO PRIMARY KEY(ELEMENT_NUM, MAP_NUM, INGAME_NUM)  
);

COMMENT ON COLUMN TBL_MAP_KIND.ELEMENT_NUM IS '요소번호';
COMMENT ON COLUMN TBL_MAP_KIND.MAP_NUM IS '맵 번호';
COMMENT ON COLUMN TBL_MAP_KIND.INGAME_NUM IS '순번';
COMMENT ON TABLE TBL_MAP_KIND IS '인게임';




INSERT
  INTO TBL_COSTUME C
(
  C.COSTUME_NUM, C.COSTUME_NAME, C.INCREASE_LIFE
, C.INCREASE_SCORE_RATE, C.COSTUME_PRICE
)
VALUES
(
  1, '스카프', 1
, 0.2, 120
);
INSERT
  INTO TBL_COSTUME C
(
  C.COSTUME_NUM, C.COSTUME_NAME, C.INCREASE_LIFE
, C.INCREASE_SCORE_RATE, C.COSTUME_PRICE
)
VALUES
(
  2, '모자', 2
, 0.1, 110
);



INSERT
  INTO TBL_POKEMON P
(
  P.POKEMON_NUM
, P.POKEMON_NAME
, P.INCREASE_BALL_RATE
, P.POKEMON_PRICE
)
VALUES
(
  1
, '피카츄'
, 0
, 100
);
INSERT
  INTO TBL_POKEMON P
(
  P.POKEMON_NUM
, P.POKEMON_NAME
, P.INCREASE_BALL_RATE
, P.POKEMON_PRICE
)
VALUES
(
  2
, '거북왕'
, 0.4
, 100
);
INSERT
  INTO TBL_POKEMON P
(
  P.POKEMON_NUM
, P.POKEMON_NAME
, P.INCREASE_BALL_RATE
, P.POKEMON_PRICE
)
VALUES
(
  3
, '이상해씨'
, 0.1
, 100
);
INSERT
  INTO TBL_POKEMON P
(
  P.POKEMON_NUM
, P.POKEMON_NAME
, P.INCREASE_BALL_RATE
, P.POKEMON_PRICE
)
VALUES
(
  4
, '고오스'
, 0.1
, 100
);
INSERT
  INTO TBL_POKEMON P
(
  P.POKEMON_NUM
, P.POKEMON_NAME
, P.INCREASE_BALL_RATE
, P.POKEMON_PRICE
)
VALUES
(
  5
, '리자몽'
, 0.3
, 100
);
INSERT
  INTO TBL_POKEMON P
(
  P.POKEMON_NUM
, P.POKEMON_NAME
, P.INCREASE_BALL_RATE
, P.POKEMON_PRICE
)
VALUES
(
  6
, '피존'
, 0
, 100
);
INSERT
  INTO TBL_POKEMON P
(
  P.POKEMON_NUM
, P.POKEMON_NAME
, P.INCREASE_BALL_RATE
, P.POKEMON_PRICE
)
VALUES
(
  7
, '뮤'
, 0.5
, 100
);
INSERT
  INTO TBL_POKEMON P
(
  P.POKEMON_NUM
, P.POKEMON_NAME
, P.INCREASE_BALL_RATE
, P.POKEMON_PRICE
)
VALUES
(
  8
, '미뇽'
, 0.2
, 100
);
INSERT
  INTO TBL_POKEMON P
(
  P.POKEMON_NUM
, P.POKEMON_NAME
, P.INCREASE_BALL_RATE
, P.POKEMON_PRICE
)
VALUES
(
  9
, '푸린'
, (-0.3)
, 100
);
INSERT
  INTO TBL_POKEMON P
(
  P.POKEMON_NUM
, P.POKEMON_NAME
, P.INCREASE_BALL_RATE
, P.POKEMON_PRICE
)
VALUES
(
  10
, '팬텀'
, 0.2
, 100
);



INSERT
  INTO TBL_MAP M
(
  M.MAP_NUM
, M.MAP_NAME
)
VALUES
(
  1
, '초원'
);
INSERT
  INTO TBL_MAP M
(
  M.MAP_NUM
, M.MAP_NAME
)
VALUES
(
  2
, '바다'
);
INSERT
  INTO TBL_MAP M
(
  M.MAP_NUM
, M.MAP_NAME
)
VALUES
(
  3
, '던전'
);

/*
DELETE FROM TBL_ELEMENT;

INSERT
  INTO TBL_ELEMENT E
(
  E.ELEMENT_NUM
, E.ELEMENT_NAME
, E.ELEMENT_LENGTH
, E.HURDLE_HEIGHT
, E.HURDLE_DAMAGE
)
VALUES
(

);
*/
INSERT
  INTO TBL_ACCOUNT A
(
  A.ACNT_NUM, A.ACNT_KIND, A.ACNT_ID
, A.ACNT_PW, A.DEL_YN, A.DEL_DATE
, A.MEM_NAME, A.MEM_EMAIL, A.ENT_DATE
)
VALUES
(
  999, 'ADMIN', 'admin'
, 'admin', 'N', NULL
, '관리자계정', 'h99www@gmail.com', '20211212'
);